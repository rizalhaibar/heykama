<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryStockProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_stock_products', function (Blueprint $table) {
            $table->id();
            $table->integer('variant_id');
            $table->integer('product_id');
            $table->integer('name');
            $table->integer('description');
            $table->integer('current_stock');
            $table->integer('stock');
            $table->integer('latest_stock');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_stock_products');
    }
}
