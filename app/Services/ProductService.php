<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class ProductService {

    function listproduct() {
		$hasil = DB::select(DB::raw("
                SELECT products.*, created_user.name as created_name, updated_user.name as updated_name
                FROM products
                LEFT JOIN users created_user on created_user.id = products.created_by
                LEFT JOIN users updated_user on updated_user.id = products.updated_by
		"));

		return $hasil;
	}


    function listproduct_type() {
		$hasil = DB::select(DB::raw("
                SELECT *
                FROM product_types
		"));

		return $hasil;
	}

    function listproductimage($id) {
        $hasil = DB::table('mapping_image_varian_products')
        ->where('product_id',  $id)->get();
		return $hasil;
	}

    function listproductimagebyvariant($id) {
        $hasil = DB::table('mapping_image_varian_products')
        ->where('variant_id',  $id)->get();
		return $hasil;
	}


    function product($id) {
        $hasil = DB::table('products')
        ->where('id',  $id)->first();

        $mapping_varian_products = DB::select(DB::raw("
                SELECT * FROM mapping_varian_products where product_id = :product_id
        "), array(
            'product_id' => $id,
        ));
        $arr = [];
        foreach($mapping_varian_products as $new1){

            $mapping_image_varian_products = DB::select(DB::raw("
                    SELECT * FROM mapping_image_varian_products where variant_id = :variant_id
            "), array(
                'variant_id' => $new1->id,
            ));
            if($new1->product_id == $hasil->id){
                foreach($mapping_image_varian_products as $new2){
                    if($new1->id == $new2->variant_id){
                        $new1->mapping_image_varian_products = $mapping_image_varian_products;
                    }
                }


            }
            array_push($arr, $new1);
        }
        $hasil->mapping_varian_products = $arr;
		return $hasil;
	}


    function mapping_varian_products($id) {
        $hasil = DB::table('mapping_varian_products')
        ->where('id',  $id)->first();
        $hasil->mapping_image_varian_products = [];
        $mapping_image_varian_products = DB::select(DB::raw("
                SELECT * FROM mapping_image_varian_products where variant_id = :variant_id
        "), array(
            'variant_id' => $hasil->id,
        ));
        foreach($mapping_image_varian_products as $new2){
            if($hasil->id == $new2->variant_id){
                $hasil->mapping_image_varian_products = $mapping_image_varian_products;
            }
        }

		return $hasil;
	}

    function allproduct() {
        $hasil = DB::select(DB::raw("
                SELECT products.*, created_user.name as created_name, updated_user.name as updated_name
                FROM products
                LEFT JOIN users created_user on created_user.id = products.created_by
                LEFT JOIN users updated_user on updated_user.id = products.updated_by
		"));

        $data = [];
        foreach($hasil as $produk){
            // dd($produk->id);
            $mapping_varian_products = DB::select(DB::raw("
                    SELECT * FROM mapping_varian_products where product_id = :product_id
            "), array(
                'product_id' => $produk->id,
            ));
            $arr = [];
            foreach($mapping_varian_products as $new1){

                $mapping_image_varian_products = DB::select(DB::raw("
                        SELECT * FROM mapping_image_varian_products where variant_id = :variant_id
                "), array(
                    'variant_id' => $new1->id,
                ));
                if($new1->product_id == $produk->id){
                    foreach($mapping_image_varian_products as $new2){
                        if($new1->id == $new2->variant_id){
                            $new1->url = $new2->url;
                            // $new1->mapping_image_varian_products = $mapping_image_varian_products;
                        }
                    }


                }

                array_push($arr, $new1);

            }

            $produk->mapping_varian_products = $arr;
            array_push($data, $produk);
        }
		return $data;
	}


    function listcategory() {
		$hasil = DB::select(DB::raw("
                SELECT *
                FROM categories
		"));

		return $hasil;
	}

    function save($request, $sessionUser) {
        // dd($request);
		DB::beginTransaction();
        try {
            $preorder = false;
            if (isset($request->preorder)){
                $preorder = true;
            }
            DB::table('products')
					->insert([
						'name' => $request->product_name,
						'description' => $request->product_description,
						'sku' => $request->sku,
						'product_type_id' => $request->product_type_id,
						'category_id' => $request->account_type,
						'url_thumnail' => $request->url_thumnail,
						'preorder' => $preorder,
                        'created_by' => $sessionUser->id,
						'created_at' => date('Y-m-d H:i:s')
					]);
			$id = DB::getPdo()->lastInsertId();
            $result = DB::commit();
		} catch(Exception $e){
			DB::rollback();

			return 0;
		}
		return $id;
	}

    function edit($request, $sessionUser) {
        $detail = DB::table('products')
        ->where('id',  $request->id)->first();
        $thumnail = $detail->url_thumnail;
        if(isset($request->url_thumnail)){
            $thumnail = $request->url_thumnail;
        }
		DB::beginTransaction();
        try {
            $preorder = false;
            if (isset($request->preorder)){
                $preorder = true;
            }
            DB::table('products')
                    ->where('id', $request->id)
					->update([
						'name' => $request->product_name,
						'description' => $request->product_description,
						'sku' => $request->sku,
						'product_type_id' => $request->product_type_id,
						'category_id' => $request->account_type,
						'url_thumnail' => $thumnail,
						'updated_by' => $sessionUser->id,
						'preorder' => $preorder,
						'updated_at' => date('Y-m-d H:i:s')
					]);
			$id = DB::getPdo()->lastInsertId();
            $result = DB::commit();
		} catch(Exception $e){
			DB::rollback();

			return 0;
		}
		return $id;
	}
    function saveVariant($name, $id, $current_price, $price, $qty) {
		DB::beginTransaction();
        try {
            DB::table('mapping_varian_products')
					->insert([
						'product_id' => $id,
						'name' => $name,
						'currrent_price' => $current_price,
						'price' => $price,
						'qty' => $qty,
						'created_at' => date('Y-m-d H:i:s')
					]);
            $variant_id = DB::getPdo()->lastInsertId();
            DB::table('history_stock_products')
					->insert([
						'variant_id' => $variant_id,
						'product_id' => $id,
						'name' => $name,
						'description' => "Tambah Item Baru",
						'stock' => $qty,
						'current_stock' => $qty,
						'latest_stock' => $qty,
						'status' => 'Stock In',
						'created_at' => date('Y-m-d H:i:s')
					]);

            DB::commit();
		} catch(Exception $e){
			DB::rollback();

			return 0;
		}
		return $variant_id;
	}
    function updateVariant($name, $id, $idvariant, $current_price, $price, $qty) {
		DB::beginTransaction();
        try {
            DB::table('mapping_varian_products')
                    ->where('id', $idvariant)
					->update([
						'name' => $name,
						'currrent_price' => $current_price,
						'price' => $price,
						'qty' => $qty,
						'created_at' => date('Y-m-d H:i:s')
					]);
            $detail = DB::table('mapping_varian_products')
                    ->where('id',  $idvariant)->first();
            if($detail->qty != $qty){
                DB::table('history_stock_products')
                ->insert([
                    'variant_id' => $idvariant,
                    'product_id' => $id,
                    'name' => $name,
                    'description' => "Update Item Baru",
                    'stock' => $qty,
                    'current_stock' => $detail->qty,
                    'latest_stock' => $qty,
                    'status' => 'Stock Update',
                    'created_at' => date('Y-m-d H:i:s')
                ]);

            }

			$id = DB::getPdo()->lastInsertId();
            $result = DB::commit();
		} catch(Exception $e){
			DB::rollback();

			return 0;
		}
		return $id;
	}
    function saveVariantImage($id, $variant_id, $url) {
		DB::beginTransaction();
        try {
            DB::table('mapping_image_varian_products')
					->insert([
						'product_id' => $id,
						'variant_id' => $variant_id,
						'url' => $url,
						'created_at' => date('Y-m-d H:i:s')
					]);
			$id = DB::getPdo()->lastInsertId();
            $result = DB::commit();
		} catch(Exception $e){
			DB::rollback();

			return 0;
		}
		return $id;
	}
    function updateVariantImage($id, $variant_id) {
		DB::beginTransaction();
        try {
            DB::table('mapping_image_varian_products')
                    ->where('id', $id)
					->update([
						'variant_id' => $variant_id
					]);
			$id = DB::getPdo()->lastInsertId();
            $result = DB::commit();
		} catch(Exception $e){
			DB::rollback();

			return 0;
		}
		return $id;
	}
    function updateVariantImageEdit($id, $variant_id) {
		DB::beginTransaction();
        try {
            DB::table('mapping_image_varian_products')
                    ->where('id', $id)
					->update([
						'variant_id' => $variant_id
					]);
			$id = DB::getPdo()->lastInsertId();
            $result = DB::commit();
		} catch(Exception $e){
			DB::rollback();

			return 0;
		}
		return $id;
	}
    function savePrice($request) {
		DB::beginTransaction();
        try {
            $id 		=DB::table('products')
						->where('id', $request->id)
						->update([
							'price' => $request->price,
                            'selling_price' => $request->price,
                            'diskon_type_id' => 1,
                            'diskon' => $request->vat,
                            'updated_at' => date('Y-m-d H:i:s')
						]);
            DB::commit();
		} catch(Exception $e){
			DB::rollback();

			return 0;
		}
		return $id;
	}

}
