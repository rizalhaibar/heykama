<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Auth;

class UserService {

    function detail_user($sessionUser) {
        $hasil = DB::table('users')
        ->where('id',  $sessionUser->id)->first();
		return $hasil;
	}

    function detail_user_by_id($id) {
        $hasil = DB::table('users')
        ->where('id',  $id)->first();
		return $hasil;
	}

    function mapping_address_users($sessionUser) {
        $hasil = DB::table('mapping_address_users')
        ->where('user_id',  $sessionUser->id)->get();
		return $hasil;
	}


}
