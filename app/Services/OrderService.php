<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class OrderService {

    function listorder() {
		$order = DB::select(DB::raw("
                SELECT *
                FROM orders o
		"));
        $data = [];
        foreach($order as $value) {

            $product = DB::select(DB::raw("
                    SELECT *
                    FROM products o
                    left join mapping_order_products mop on mop.product_id = o.id
                    where mop.order_id = :order_id
            "), array(
                'order_id' => $value->id,
            ));
            foreach($product as $new6){


                if($new6->order_id == $value->id){

                    $value->product = $product;
                }
            }
            array_push($data, $value);
        }
		return $data;
	}

    function detailorder($id) {
		$order = DB::table('orders')->where('id', $id)->first();

        $data = [];

        $product = DB::select(DB::raw("
                SELECT *
                FROM products o
                left join mapping_order_products mop on mop.product_id = o.id
                where mop.order_id = :order_id
        "), array(
            'order_id' => $order->id,
        ));
        foreach($product as $new6){


            if($new6->order_id == $order->id){

                $order->product = $product;
            }
        }
        array_push($data, $order);

		return $data;
	}

    function detailorderfirst($id) {
		$order = DB::table('orders')->where('id', $id)->first();

        $product = DB::select(DB::raw("
                SELECT *, mop.qty as order_qty
                FROM products o
                left join mapping_order_products mop on mop.product_id = o.id
                where mop.order_id = :order_id
        "), array(
            'order_id' => $order->id,
        ));
        foreach($product as $new6){


            if($new6->order_id == $order->id){

                $order->product = $product;
            }
        }
        // array_push($data, $order);

		return $order;
	}

}
