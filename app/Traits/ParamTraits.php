<?php 

namespace App\Traits;

trait ParamTraits
{

    protected function paramBy($codename)
    {
        return $this->paramRepo->search('codename', $codename)->get();
    }

}