<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FlashMessageTraits;
use DB;
class ProductTypeController extends Controller
{
    use FlashMessageTraits;
    function __construct(

    )
    {
        //No session access from constructor work arround
        // $this->middleware(function ($request, $next)
        // {
        //     $this->user = session('data')->user;
        //     return $next($request);
        // });
    }

    public function index()
    {
        $data['breadcrumb'] = [
            'parent' => 'Category',
            'child' => 'List Category'
        ];
        $data['product_types'] = $this->product_types();
        $data['listcategory'] = $this->listcategory();
        return view('backend.producttype.index', $data);
    }

    public function create(Request $request)
    {
        $input = $request->all();
		$data = DB::table('product_types')->insert(
			['name' => $input['name'], 'description' => $input['description'], 'category_id' => $input['category_id']]
		);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Product Type Created Success!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Product Type Created Failed!' );
			return response()->json($result);
		}
    }


	public function product_types(){
		$hasil = DB::select('SELECT *
								FROM product_types');
		return $hasil;
	}

	public function listcategory(){
		$hasil = DB::select('SELECT *
								FROM categories');
		return $hasil;
	}


}
