<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FlashMessageTraits;
use DB;

class CustomerController extends Controller
{
    use FlashMessageTraits;
    function __construct(

    )
    {
        //No session access from constructor work arround
        // $this->middleware(function ($request, $next)
        // {
        //     $this->user = session('data')->user;
        //     return $next($request);
        // });
    }

    public function index()
    {
        $data['breadcrumb'] = [
            'parent' => 'Category',
            'child' => 'List Category'
        ];
        $data['listcategory'] = $this->listcategory();
        return view('backend.customer.index', $data);
    }

    public function create(Request $request)
    {
        $input = $request->all();
		$data = DB::table('customers')->insert(
			[
                'name' => $input['name'],
                'address' => $input['address'],
                'fax' => $input['fax'],
                'phone_number' => $input['phone_number'],
                'email' => $input['email']
            ]
		);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Category Created Success!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Category Created Failed!' );
			return response()->json($result);
		}
    }


	public function listcategory(){
		$hasil = DB::table('users')->where('type_user', 'CUSTOMER')->get();
		return $hasil;
	}


}
