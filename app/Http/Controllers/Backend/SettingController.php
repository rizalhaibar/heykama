<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FlashMessageTraits;
use DB;
class SettingController extends Controller
{
    use FlashMessageTraits;
    function __construct(

    )
    {
        //No session access from constructor work arround
        // $this->middleware(function ($request, $next)
        // {
        //     $this->user = session('data')->user;
        //     return $next($request);
        // });
    }

    public function index()
    {
        $data['breadcrumb'] = [
            'parent' => 'Category',
            'child' => 'List Category'
        ];
        $data['list'] = $this->list();
        return view('backend.setting.index', $data);
    }

    public function create(Request $request)
    {
        $input = $request->all();
        $id = 0;
        if ($request->hasFile('image')) {
            $img       = $request->file('image');
            $destinationPath = 'setting/banner';
            $myimage = $img->getClientOriginalName();
            $path = $destinationPath.'/'.$myimage;
            $request->image->move(public_path($destinationPath), $myimage);

            $id = DB::table('settings')
            ->where('type_setting', $input['type_setting'])
            ->update(
                ['url' => $path]
            );

        }
        if($id){
            return redirect()->route('backend.setting.index');
        }

        return redirect()->back()->withErrors(['Upload Min Image 1']);


    }


	public function list(){
		$hasil = DB::select('SELECT *
								FROM settings');
		return $hasil;
	}


}
