<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FlashMessageTraits;
use DB;
class DashboardController extends Controller
{
    use FlashMessageTraits;
    function __construct(

    )
    {
        //No session access from constructor work arround
        // $this->middleware(function ($request, $next)
        // {
        //     $this->user = session('data')->user;
        //     return $next($request);
        // });
    }

    public function index()
    {
        $data['breadcrumb'] = [
            'parent' => 'Dashboard',
            'child' => ''
        ];
        return view('backend.dashboard.index', $data);
    }
}
