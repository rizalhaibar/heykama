<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FlashMessageTraits;
use DB;
class JournalController extends Controller
{
    use FlashMessageTraits;
    function __construct(

    )
    {
        //No session access from constructor work arround
        // $this->middleware(function ($request, $next)
        // {
        //     $this->user = session('data')->user;
        //     return $next($request);
        // });
    }

    public function index()
    {
        $data['breadcrumb'] = [
            'parent' => 'Category',
            'child' => 'List Category'
        ];
        $data['listcategory'] = $this->listcategory();
        return view('backend.journal.index', $data);
    }

    public function detail($id)
    {
        $data['id'] = $id;
        $data['detail'] = $this->detaildata($id);
        return view('backend.journal.detail', $data);
    }

    public function create(Request $request)
    {

        $sessionUser = $request->session()->get('session_user');
        $input = $request->all();
		$data = DB::table('journals')->insert(
			['created_by' => $sessionUser->id, 'title' => $input['title'], 'body' => $input['body'], 'status' => $input['status'], 'created_at' => date('Y-m-d H:i:s')]
		);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Journals Created Success!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Journals Created Failed!' );
			return response()->json($result);
		}
    }


	public function listcategory(){
		$hasil = DB::select('SELECT *
								FROM journals');
		return $hasil;
	}

	public function detaildata($id){
        $hasil = DB::table('journals')
        ->where('id',  $id)->first();
		return $hasil;
	}


}
