<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FlashMessageTraits;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Validator;
use DB;

class UserController extends Controller
{
    use FlashMessageTraits;
    function __construct(

    )
    {
        //No session access from constructor work arround
        // $this->middleware(function ($request, $next)
        // {
        //     $this->user = session('data')->user;
        //     return $next($request);
        // });
    }

    public function index()
    {
        $data['breadcrumb'] = [
            'parent' => 'User',
            'child' => 'List User'
        ];
        $data['listuser'] = $this->listuser();
        return view('backend.user.index', $data);
    }

    public function add()
    {
        $data['breadcrumb'] = [
            'parent' => 'User',
            'child' => 'Create User'
        ];
        return view('backend.user.create', $data);
    }

    public function edit($id)
    {
        $data['breadcrumb'] = [
            'parent' => 'User',
            'child' => 'Create User'
        ];
        $data['detail'] = $this->detailuser($id);
        return view('backend.user.edit', $data);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'password' => 'required',
            'gender' => 'required',
            'type_user' => 'required',
            'phone_number' => 'required',
            'avatar' => 'image|mimes:jpeg,png,jpg',
        ]);
        if ($validator->fails()) {
			$label = '';
			$errors = $validator->errors();
			foreach ($errors->all() as $message) {
				$label .= ' '.$message;
			}
            // dd($label);
			return redirect()->back()->withErrors([$label]);
        }

        $sessionUser = $request->session()->get('session_user');
        $id = 0;
        if ($request->hasFile('avatar')) {
            $img       = $request->file('avatar');
            $path = Storage::putFileAs('public/avatar', $img, $img->getClientOriginalName());
            $request->url_thumnail = $path;
            $id = DB::table('users')->insert(
                [
                    'name' => $request->name,
                    'address' => $request->address,
                    'password' => Hash::make($request->password),
                    'gender' => $request->gender,
                    'type_user' => $request->type_user,
                    'url' => $path,
                    'phone_number' => $request->phone_number,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $sessionUser->id,
                    'email' => $request->email
                ]
            );

        }


        if ($id > 0){
            return redirect()->route('backend.user.index');
        }


        return redirect()->back()->withErrors(['Create User Failed']);

    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'gender' => 'required',
            'type_user' => 'required',
            'phone_number' => 'required',
            'avatar' => 'image|mimes:jpeg,png,jpg',
        ]);
        if ($validator->fails()) {
			$label = '';
			$errors = $validator->errors();
			foreach ($errors->all() as $message) {
				$label .= ' '.$message;
			}
            // dd($label);
			return redirect()->back()->withErrors([$label]);
        }

        $sessionUser = $request->session()->get('session_user');
        if ($request->hasFile('avatar')) {
            $img       = $request->file('avatar');
            $destinationPath = 'glasses';
            $myimage = $img->getClientOriginalName();
            $path = $destinationPath.'/'.$myimage;
            $request->avatar->move(public_path($destinationPath), $myimage);


            // $path = Storage::putFileAs('public/avatar', $img, $img->getClientOriginalName());

            if($request->password){
                $id = DB::table('users')->where('id', $request->id)->update(
                    [
                        'name' => $request->name,
                        'address' => $request->address,
                        'password' => Hash::make($request->password),
                        'gender' => $request->gender,
                        'type_user' => $request->type_user,
                        'url' => $path,
                        'phone_number' => $request->phone_number,
                        'updated_at' => date('Y-m-d H:i:s'),
                        'updated_by' => $sessionUser->id,
                        'email' => $request->email
                    ]
                );
            }else{
                $id = DB::table('users')->where('id', $request->id)->update(
                    [
                        'name' => $request->name,
                        'address' => $request->address,
                        'gender' => $request->gender,
                        'type_user' => $request->type_user,
                        'url' =>$path,
                        'phone_number' => $request->phone_number,
                        'updated_at' => date('Y-m-d H:i:s'),
                        'updated_by' => $sessionUser->id,
                        'email' => $request->email
                    ]
                );
            }

        }



        if ($id > 0){
            return redirect()->route('backend.user.index');
        }


        return redirect()->back()->withErrors(['Update User Failed']);

    }


	public function listuser(){
		$hasil = DB::table('users')->where('type_user', '!=', 'CUSTOMER')->get();
		return $hasil;
	}

	public function detailuser($id){
		$hasil = DB::table('users')->where('id', $id)->first();
		return $hasil;
	}


}
