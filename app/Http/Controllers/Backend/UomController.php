<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FlashMessageTraits;
use DB;
class UomController extends Controller
{
    use FlashMessageTraits;
    function __construct(

    )
    {
        //No session access from constructor work arround
        // $this->middleware(function ($request, $next)
        // {
        //     $this->user = session('data')->user;
        //     return $next($request);
        // });
    }

    public function index()
    {
        $data['breadcrumb'] = [
            'parent' => 'Category',
            'child' => 'List Category'
        ];
        $data['listcategory'] = $this->listcategory();
        return view('backend.uom.index', $data);
    }

    public function create(Request $request)
    {
        $input = $request->all();
		$data = DB::table('unit_of_measures')->insert(
			['name' => $input['name'], 'description' => $input['description']]
		);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Category Created Success!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Category Created Failed!' );
			return response()->json($result);
		}
    }


	public function listcategory(){
		$hasil = DB::select('SELECT *
								FROM unit_of_measures');
		return $hasil;
	}


}
