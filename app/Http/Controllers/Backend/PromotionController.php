<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FlashMessageTraits;
use App\Services\ProductService;
use App\Services\UserService;
use Illuminate\Support\Facades\Storage;
use Validator;
use DB;
class PromotionController extends Controller
{
    use FlashMessageTraits;
    public function __construct() {
		$this->productService = app(ProductService::class);
		$this->userService = app(UserService::class);

    }

    public function index()
    {
        $data['breadcrumb'] = [
            'parent' => 'Dashboard',
            'child' => ''
        ];
        $data['listproduct'] = $this->productService->listproduct();
        return view('backend.promotion.index', $data);
    }


    function listdatatable(Request $request)
    {
        // $data = '{
        //     "data": [
        //       {
        //         "id": "1",
        //         "name": "Tiger Nixon",
        //         "position": "System Architect",
        //         "salary": "$320,800",
        //         "start_date": "2011/04/25",
        //         "office": "Edinburgh",
        //         "extn": "5421",
        //         "child":[
        //             {
        //                 "id": "1",
        //                 "name": "Child 1",
        //                 "position": "Child 1",
        //                 "salary": "$320,800"
        //             },
        //             {
        //                 "id": "2",
        //                 "name": "Child 2",
        //                 "position": "Child 1",
        //                 "salary": "$320,800"
        //             },
        //             {
        //                 "id": "2",
        //                 "name": "Child 2",
        //                 "position": "Child 1",
        //                 "salary": "$320,800"
        //             }
        //         ]
        //       }
        //     ]
        //   }';

        $data['data'] = $this->productService->allproduct();
        $datatable = json_encode($data, true);
        $datatable = json_decode($datatable, true);
        // $datatable = $this->historyCorporateService->getTopUpDatatable($request->all());
		return response()->json($datatable);
    }

}
