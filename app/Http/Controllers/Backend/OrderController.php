<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FlashMessageTraits;
use App\Services\OrderService;
use App\Services\UserService;
use Validator;
use DB;
class OrderController extends Controller
{
    use FlashMessageTraits;
    public function __construct() {
		$this->orderService = app(OrderService::class);
		$this->userService = app(UserService::class);

    }

    public function index()
    {
        $data['breadcrumb'] = [
            'parent' => 'Dashboard',
            'child' => ''
        ];
        $data['listorder'] = $this->orderService->listorder();
        // dd($data);
        return view('backend.order.index', $data);
    }

    public function detail($id)
    {
        $data['breadcrumb'] = [
            'parent' => 'Dashboard',
            'child' => ''
        ];
        $detail = $this->orderService->detailorderfirst($id);
        $data['listorder'] = $detail;
        if($detail){
            $data['userorder'] = $this->userService->detail_user_by_id($detail->user_id);
        }
        // dd($data);
        return view('backend.order.detail', $data);
    }
}
