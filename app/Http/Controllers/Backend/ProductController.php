<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FlashMessageTraits;
use App\Services\ProductService;
use App\Services\UserService;
use Illuminate\Support\Facades\Storage;
use Validator;
use DB;
class ProductController extends Controller
{
    use FlashMessageTraits;
    public function __construct() {
		$this->productService = app(ProductService::class);
		$this->userService = app(UserService::class);

    }

    public function index()
    {
        $data['breadcrumb'] = [
            'parent' => 'Dashboard',
            'child' => ''
        ];
        $data['listproduct'] = $this->productService->listproduct();
        return view('backend.product.index', $data);
    }

    public function addproduct()
    {
        $data['breadcrumb'] = [
            'parent' => 'Dashboard',
            'child' => ''
        ];

        $data['product_type'] = $this->productService->listproduct_type();
        $data['category'] = $this->productService->listcategory();
        return view('backend.product.addproduct', $data);
    }

    public function editproduct($id)
    {
        $data['breadcrumb'] = [
            'parent' => 'Dashboard',
            'child' => ''
        ];

        $data['listproductimage'] = $this->productService->listproductimage($id);
        $data['product'] = $this->productService->product($id);
        $data['product_type'] = $this->productService->listproduct_type();
        $data['category'] = $this->productService->listcategory();
        return view('backend.product.editproduct', $data);
    }

    public function addvarian($id)
    {
        $data['breadcrumb'] = [
            'parent' => 'Dashboard',
            'child' => ''
        ];

        $data['id'] = $id;
        $data['listproductimage'] = $this->productService->listproductimage($id);
        $data['product_type'] = $this->productService->listproduct_type();
        $data['category'] = $this->productService->listcategory();
        return view('backend.product.addvarian', $data);
    }

    public function editvariant($id)
    {
        $data['breadcrumb'] = [
            'parent' => 'Dashboard',
            'child' => ''
        ];

        $data['id'] = $id;
        $data['listproductimage'] = $this->productService->listproductimage($id);
        $data['product'] = $this->productService->product($id);
        $data['product_type'] = $this->productService->listproduct_type();
        $data['category'] = $this->productService->listcategory();
        // dd($data);
        return view('backend.product.editvarian', $data);
    }

    public function modaleditvariant($id)
    {
        $data['id'] = $id;
        $data['mpv'] = $this->productService->mapping_varian_products($id);
        $data['listproductimage'] = [];
        if($this->productService->mapping_varian_products($id)){
            $data['listproductimage'] = $this->productService->listproductimage($this->productService->mapping_varian_products($id)->product_id);
        }
        return view('backend.product.modaleditvarian', $data);
    }

    public function addprice($id)
    {
        $data['breadcrumb'] = [
            'parent' => 'Dashboard',
            'child' => ''
        ];

        $data['id'] = $id;
        $data['product_type'] = $this->productService->listproduct_type();
        $data['category'] = $this->productService->listcategory();
        return view('backend.product.addprice', $data);
    }

    public function detailproduct($id)
    {
        $data['breadcrumb'] = [
            'parent' => 'Dashboard',
            'child' => ''
        ];

        $data['id'] = $id;
        $data['product'] = $this->productService->product($id);
        $data['product_type'] = $this->productService->listproduct_type();
        return view('backend.product.detailproduct', $data);
    }



    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'account_type' => 'required',
            'product_name' => 'required',
            'product_description' => 'required',
            'sku' => 'required',
            'product_type_id' => 'required',
            'avatar' => 'image|mimes:jpeg,png,jpg',
        ]);
        if ($validator->fails()) {
			$label = '';
			$errors = $validator->errors();
			foreach ($errors->all() as $message) {
				$label .= ' '.$message;
			}
			return redirect()->back()->withErrors([$label]);
        }


        $sessionUser = $request->session()->get('session_user');
        if ($request->hasFile('avatar')) {
            $img       = $request->file('avatar');
            $destinationPath = 'glasses';
            $myimage = $img->getClientOriginalName();
            $path = $destinationPath.'/'.$myimage;
            $request->avatar->move(public_path($destinationPath), $myimage);

            // $path = Storage::putFileAs('public/glasses', $img, $img->getClientOriginalName());
            $request->url_thumnail = $path;
            $id = $this->productService->save($request, $sessionUser);

        }
        $product_id = $id;
        if ($request->hasFile('image')) {
            $image       = $request->file('image');
            foreach($image as $img){
                $destinationPath = 'glasses';
                $myimage = $img->getClientOriginalName();
                $path = $destinationPath.'/'.$myimage;
                $img->move(public_path($destinationPath), $myimage);

                // $path = Storage::putFileAs('public/glasses', $img, $img->getClientOriginalName());
                $this->productService->saveVariantImage($product_id, null, $path);
            }
        }

        if ($id > 0){
            return redirect()->route('backend.product.addvarian', ['id' => $id]);
        }


        return redirect()->back()->withErrors(['Bukti Transaksi Harus di Upload Min 1']);

    }

    public function edit(Request $request){
        $validator = Validator::make($request->all(), [
            'account_type' => 'required',
            'product_name' => 'required',
            'product_description' => 'required',
            'sku' => 'required',
            'product_type_id' => 'required',
        ]);
        if ($validator->fails()) {
			$label = '';
			$errors = $validator->errors();
			foreach ($errors->all() as $message) {
				$label .= ' '.$message;
			}
			return redirect()->back()->withErrors([$label]);
        }


        $sessionUser = $request->session()->get('session_user');
        if ($request->hasFile('avatar')) {
            $img       = $request->file('avatar');
            $destinationPath = 'glasses';
            $myimage = $img->getClientOriginalName();
            $path = $destinationPath.'/'.$myimage;
            $request->avatar->move(public_path($destinationPath), $myimage);

            // $path = Storage::putFileAs('public/glasses', $img, $img->getClientOriginalName());
            $request->url_thumnail = $path;

        }

        $id = $this->productService->edit($request, $sessionUser);
        $product_id = $id;
        if ($request->hasFile('image')) {
            $image       = $request->file('image');
            foreach($image as $img){
                $destinationPath = 'glasses';
                $myimage = $img->getClientOriginalName();
                $path = $destinationPath.'/'.$myimage;
                $img->move(public_path($destinationPath), $myimage);

                // $path = Storage::putFileAs('public/glasses', $img, $img->getClientOriginalName());
                $this->productService->saveVariantImage($product_id, null, $path);
            }
        }


        return redirect()->route('backend.product.editvariant', ['id' => $request->id]);

    }

    public function savevariant(Request $request){
        $validator = Validator::make($request->all(), [
            'variant_name' => 'required',
        ]);

        if ($validator->fails()) {
			$label = '';
			$errors = $validator->errors();
			foreach ($errors->all() as $message) {
				$label .= ' '.$message;
			}
			return redirect()->back()->withErrors([$label]);
        }
        if (count($request->variant_name) != count($request->thumnails)) {
            $label = 'Thumnail Not Selected!';
            return redirect()->back()->withErrors([$label]);
        }

        $i = 0;
        foreach($request->variant_name as $key=>$val){

            $id = $this->productService->saveVariant($val, $request->id, $request->current_price[$key], $request->price[$key], $request->qty[$key]);

            $this->productService->updateVariantImage($request->thumnails[$key], $id);
            $i++;
        }

        if ($id > 0){
            return redirect()->route('backend.product.index');
            // return redirect()->to('backend/product/addvarian', $id)->with('berhasil', 'Create Category Success!');
        }

        return redirect()->back()->withErrors(['Bukti Transaksi Harus di Upload Min 1']);

    }
    public function updatevariant(Request $request){
        $validator = Validator::make($request->all(), [
            'variant_name' => 'required',
        ]);

        if (count($request->id_variant) != count($request->thumnails)) {
            $label = 'Thumnail Not Selected!';
            return redirect()->back()->withErrors([$label]);
        }
        if ($validator->fails()) {
			$label = '';
			$errors = $validator->errors();
			foreach ($errors->all() as $message) {
				$label .= ' '.$message;
			}
			return redirect()->back()->withErrors([$label]);
        }
        $i = 0;
        DB::table('mapping_image_varian_products')
        ->where('product_id',$request->id)
        ->update([
            'variant_id' => null
        ]);
        foreach($request->variant_name as $key=>$val){
            if(isset($request->id_variant)){
                $id = $this->productService->updateVariant($val, $request->id, $request->id_variant[$key], $request->current_price[$key], $request->price[$key], $request->qty[$key]);

                $this->productService->updateVariantImageEdit($request->thumnails[$key], $request->id_variant[$key]);
            }else{
                $id = $this->productService->saveVariant($val, $request->id, $request->current_price[$key], $request->price[$key], $request->qty[$key]);

                $this->productService->updateVariantImage($request->thumnails[$key], $id);
            }

            $i++;
        }

        if ($request->id > 0){
            return redirect()->route('backend.product.index');
            // return redirect()->to('backend/product/addvarian', $id)->with('berhasil', 'Create Category Success!');
        }

        return redirect()->back()->withErrors(['Bukti Transaksi Harus di Upload Min 1']);

    }
    public function variantupdate(Request $request){
        $validator = Validator::make($request->all(), [
            'variant_name' => 'required',
        ]);

        if (count($request->id_variant) != count($request->thumnails)) {
            $label = 'Thumnail Not Selected!';
            return redirect()->back()->withErrors([$label]);
        }
        if ($validator->fails()) {
			$label = '';
			$errors = $validator->errors();
			foreach ($errors->all() as $message) {
				$label .= ' '.$message;
			}
			return redirect()->back()->withErrors([$label]);
        }
        $i = 0;
        foreach($request->variant_name as $key=>$val){
            if(isset($request->id_variant)){
                $id = $this->productService->updateVariant($val, $request->id, $request->id_variant[$key], $request->current_price[$key], $request->price[$key], $request->qty[$key]);

                $this->productService->updateVariantImageEdit($request->thumnails[$key], $request->id_variant[$key]);
            }else{
                $id = $this->productService->saveVariant($val, $request->id, $request->current_price[$key], $request->price[$key], $request->qty[$key]);

                $this->productService->updateVariantImage($request->thumnails[$key], $id);
            }

            $i++;
        }

        if ($request->id > 0){
            return redirect()->route('backend.product.index');
            // return redirect()->to('backend/product/addvarian', $id)->with('berhasil', 'Create Category Success!');
        }

        return redirect()->back()->withErrors(['Bukti Transaksi Harus di Upload Min 1']);

    }

    public function saveprice(Request $request){
        $validator = Validator::make($request->all(), [
            'price' => 'required',
            'selling_price' => 'required'
        ]);

        if ($validator->fails()) {
			$label = '';
			$errors = $validator->errors();
			foreach ($errors->all() as $message) {
				$label .= ' '.$message;
			}
			return redirect()->back()->withErrors([$label]);
        }
        $id = $this->productService->savePrice($request);

        if ($id > 0){
            return redirect()->route('backend.product.index');
        }

        return redirect()->back()->withErrors(['Bukti Transaksi Harus di Upload Min 1']);

    }


	public function listproduct(){
		$hasil = DB::select('SELECT *
								FROM products');
		return $hasil;
	}



    public function setproducttype(Request $request)
    {
        $input = $request->all();
		$data = DB::table('product_types')->where('category_id', $request->id)->get();
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'List Product Category Success!', 'data'=> $data);
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'List Product Category Failed!', 'data'=> [] );
			return response()->json($result);
		}
    }


    public function delele(Request $request)
    {
		$mivp = DB::table('mapping_image_varian_products')->where('product_id','=', $request->id)->delete();
		$mpv = DB::table('mapping_varian_products')->where('product_id','=', $request->id)->delete();
		$data = DB::table('products')->where('id','=', $request->id)->delete();
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Delete Product Success!', 'data'=> $data);
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Delete Product Failed!', 'data'=> [] );
			return response()->json($result);
		}
    }


    public function delelevariant(Request $request)
    {
		$mivp = DB::table('mapping_image_varian_products')->where('id','=', $request->id)->delete();
		$data = DB::table('mapping_varian_products')->where('id','=', $request->id)->delete();

		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Delete Variant Success!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Delete Variant Failed!');
			return response()->json($result);
		}
    }

}
