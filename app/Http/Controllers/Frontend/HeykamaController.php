<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\FlashMessageTraits;
use App\Services\UserService;
use DB;
class HeykamaController extends Controller
{
    use FlashMessageTraits;
    public function __construct() {
		$this->userService = app(UserService::class);

    }

    public function index()
    {
        // $instagram = new \InstagramScraper\Instagram(new \GuzzleHttp\Client());
        // $account = $instagram->getMedias('hey.kama');
        $data['listcart'] = $this->listcart();
        $data['homeproduct'] = $this->homeproduct();
        $data['featuredproduct'] = $this->featuredproduct();

        $data['listjournal'] = $this->listjournal();
        // $data['instagram'] = $account;
        return
            view('frontend.landing.index', $data)
        ;
    }
    public function about()
    {

        $data['listcart'] = $this->listcart();
        return
            view('frontend.landing.about', $data)
        ;
    }
    public function contact()
    {

        $data['listcart'] = $this->listcart();
        return
            view('frontend.landing.contact', $data)
        ;
    }
    public function blog()
    {

        $data['banner'] = $this->banner('JOURNAL');
        $data['listjournal'] = $this->listjournal();
        $data['listcart'] = $this->listcart();
        return
            view('frontend.landing.blog', $data)
        ;
    }
    public function detailjournal($id)
    {

        $data['banner'] = $this->banner('JOURNAL');
        $data['journaldetail'] = $this->journaldetail($id);
        $data['listcart'] = $this->listcart();
        return
            view('frontend.landing.detailjournal', $data)
        ;
    }
    public function features()
    {

        $data['listcart'] = $this->listcart();
        return
            view('frontend.landing.features', $data)
        ;
    }
    public function promo()
    {
        $data['listcart'] = $this->listcart();
        return
            view('frontend.landing.promo', $data)
        ;
    }
    public function shop()
    {
        $data['banner'] = $this->banner('SHOP');

        $data['listcart'] = $this->listcart();
        $data['listproduct'] = $this->listproduct();
        return view('frontend.landing.shop', $data);
    }
    public function shopdetail($id)
    {

        $data['id'] = $id;
        $data['detailproduct'] = $this->detailproductvariant($id);
        $data['product'] = $this->detailproduct($id);
        $data['listvatiant'] = $this->listvatiant($this->detailproductvariant($id)->product_id);
        $data['listcart'] = $this->listcart();
        $data['listproduct'] = $this->listproduct();
        $data['listlens'] = $this->listlens();
        // dd($data);
        return
            view('frontend.landing.shopdetail', $data)
        ;
    }
    public function trfpage($id)
    {

        $data['listcart'] = $this->listcart();
        return
            view('frontend.landing.trfdetail', $data)
        ;
    }
    public function cart(Request $request)
    {
        $sessionUser = $request->session()->get('session_user');
        $data['listcart'] = $this->listcart();
        $data['detail_user'] = $this->userService->detail_user($sessionUser);
        $data['mapping_address_users'] = $this->userService->mapping_address_users($sessionUser);
        return view('frontend.landing.cart', $data);
    }
    public function order(Request $request)
    {

        $sessionUser = $request->session()->get('session_user');
        $data['listorder'] = $this->listorder();
        $data['listcart'] = $this->listcart();
        $data['detail_user'] = $this->userService->detail_user($sessionUser);
        return view('frontend.landing.order', $data);
    }


    public function profile(Request $request)
    {
        $data['breadcrumb'] = [
            'parent' => 'Dashboard',
            'child' => ''
        ];

        $sessionUser = $request->session()->get('session_user');
        $data['detail_user'] = $this->userService->detail_user($sessionUser);
        $data['listcart'] = $this->listcart();
        return view('frontend.landing.profile', $data);
    }

    public function addtocart(Request $request)
    {
        $input = $request->all();
        $sessionUser = $request->session()->get('session_user');
		$check = DB::table('carts')
                    ->where('variant_id', $input['id'])
                    ->where('user_id', $sessionUser->id)
                    ->first();
        if ($check){
            $data = DB::table('carts')
			->where('variant_id', $input['id'])
            ->where('user_id', $sessionUser->id)
			->update([
					'qty' => $check->qty+1,
					'total' => ($check->qty+1)*$check->price,
			]);
        }else{
            $variant = DB::table('mapping_varian_products')
            ->where('id', $input['id'])
            ->first();
            $product = DB::table('products')
            ->where('id', $variant->product_id)
            ->first();
            $data = DB::table('carts')->insert(
                ['variant_id' =>  $input['id'], 'product_id' =>  $product->id, 'user_id' => $sessionUser->id, 'qty' => 1, 'price' => $variant->price, 'total' => $variant->price]
            );
        }


		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Add To Cart Success!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Add To Cart Failed!' );
			return response()->json($result);
		}
    }



    function listproduct() {
		$hasil = DB::select(DB::raw("
                SELECT
                    products.id,
                    products.id AS product_id,
                    products.name,
                    mapping_varian_products.`price` AS selling_price,
                    products.`preorder`,
                    products.`sku`,
                    products.url_thumnail
                FROM products
                LEFT JOIN mapping_varian_products ON products.id = mapping_varian_products.`product_id`
                WHERE products.`category_id` = 1
                GROUP BY products.id, products.name, mapping_varian_products.price, products.preorder, products.sku, products.url_thumnail
		"));

		return $hasil;
	}


    function listlens() {
		$lens = DB::select(DB::raw("
                SELECT *
                FROM products o where category_id = 2
		"));
        $data = [];
        foreach($lens as $value) {
            $product = DB::select(DB::raw("
                        SELECT
                            products.id,
                            products.name,
                            mapping_varian_products.id AS variant_id,
                            mapping_varian_products.name AS variant_name,
                            mapping_varian_products.`price` AS selling_price,
                            products.`preorder`,
                            products.`sku`,
                            products.url_thumnail
                        FROM products
                        LEFT JOIN mapping_varian_products ON products.id = mapping_varian_products.`product_id`
                        WHERE products.`category_id` = 2
                        AND products.id = :product_id
                        GROUP BY products.id, products.name, mapping_varian_products.id, mapping_varian_products.price, products.preorder, products.sku, products.url_thumnail, mapping_varian_products.name
            "), array(
                'product_id' => $value->id,
            ));
            foreach($product as $new6){


                if($new6->id == $value->id){

                    $value->variant = $product;
                }
            }
            array_push($data, $value);
        }

		return $data;
	}

    function homeproduct() {
		$hasil = DB::select(DB::raw("
                SELECT
                    products.id,
                    products.id AS product_id,
                    products.name,
                    mapping_varian_products.`price` AS selling_price,
                    products.`preorder`,
                    products.`sku`,
                    mivp.url AS url_thumnail

                FROM products
                LEFT JOIN mapping_varian_products ON products.id = mapping_varian_products.`product_id`
                LEFT JOIN (
                SELECT
                    variant_id,
                    url
                FROM mapping_image_varian_products
                ) mivp ON mapping_varian_products.id = mivp.`variant_id`
                WHERE products.`category_id` = 1
                GROUP BY products.id, products.name, mapping_varian_products.price, products.preorder, products.sku
                limit 4
		"));

		return $hasil;
	}


    function featuredproduct() {
		$hasil = DB::select(DB::raw("
                SELECT
                    products.id,
                    products.id AS product_id,
                    products.name,
                    mapping_varian_products.`price` AS selling_price,
                    products.`preorder`,
                    products.`sku`,
                    mivp.url AS url_thumnail

                FROM products
                LEFT JOIN mapping_varian_products ON products.id = mapping_varian_products.`product_id`
                LEFT JOIN (
                SELECT
                    variant_id,
                    url
                FROM mapping_image_varian_products
                ) mivp ON mapping_varian_products.id = mivp.`variant_id`
                WHERE products.`category_id` = 1
                GROUP BY products.id, products.name, mapping_varian_products.price, products.preorder, products.sku
                limit 20
		"));

		return $hasil;
	}

    function banner($param) {
		$hasil = DB::table('settings')->where('type_setting', $param)->first();

		return $hasil;
	}

    function listjournal() {
		$hasil = DB::table('journals')->get();

		return $hasil;
	}

    function journaldetail($id) {
		$hasil = DB::table('journals')->where('id', $id)->first();

		return $hasil;
	}

    function listcart() {
		$hasil = DB::select(DB::raw("

        SELECT p.name, c.qty, c.price, c.total, p.url_thumnail, p.diskon, mvp.name AS variant_name
        FROM carts c
        LEFT JOIN mapping_varian_products mvp ON c.variant_id = mvp.id
        LEFT JOIN products p ON c.product_id = p.id
        LEFT JOIN (
          SELECT
            variant_id,
            url
          FROM mapping_image_varian_products
          WHERE set_default = TRUE
        ) mivp ON mvp.id = mivp.`variant_id`
		"));

		return $hasil;
	}
    function listorder() {
		$order = DB::select(DB::raw("
                SELECT *
                FROM orders o
		"));
        $data = [];
        foreach($order as $value) {

            $product = DB::select(DB::raw("
                    SELECT *
                    FROM products o
                    left join mapping_order_products mop on mop.product_id = o.id
                    where mop.order_id = :order_id
            "), array(
                'order_id' => $value->id,
            ));
            foreach($product as $new6){


                if($new6->order_id == $value->id){

                    $value->product = $product;
                }
            }
            array_push($data, $value);
        }
		return $data;
	}

    function detail_user() {
		$hasil = DB::table('users')->where('id', 1)->first();

		return $hasil;
	}


    function detailproduct($id) {
		$hasil = DB::table('products')->where('id', $id)->first();

		return $hasil;
	}

    function detailproductvariant($id) {
		$variant = DB::table('mapping_varian_products')->where('product_id', $id)->first();

        $product = DB::table('mapping_image_varian_products')->where('product_id', $id)->get();

        foreach($product as $new6){


            if($new6->variant_id == $variant->id){

                $variant->image_variant = $product;
            }
        }
		return $variant;
	}


    function listvatiant($id) {
		$variant = DB::table('mapping_varian_products')->where('product_id', $id)->get();
        $data = [];
        foreach($variant as $value) {

            $product = DB::table('mapping_image_varian_products')->where('variant_id', $value->id)->get();

            foreach($product as $new6){


                if($new6->variant_id == $value->id){

                    $value->image_variant = $product;
                }
            }
            array_push($data, $value);
        }
		return $data;
	}

    public function addaddress(Request $request)
    {
        $sessionUser = $request->session()->get('session_user');
        $input = $request->all();
		$data = DB::table('mapping_address_users')->insert(
			['user_id' => $sessionUser->id, 'name' => $input['name'], 'address' => $input['address'], 'phone_number' => $input['phone_number']]
		);
		if ( $data ) {
			$result = array( 'success' => true, 'message' => 'Address Created Success!');
			return response()->json($result);
		} else {
			$result = array( 'success' => false, 'message' => 'Address Created Failed!' );
			return response()->json($result);
		}
    }




    public function checkout(Request $request){
        $sessionUser = $request->session()->get('session_user');
        $id = $this->saveOrder($request, $sessionUser);

        if ($id > 0){
            return redirect()->route('heykama.trfpage',['id'=> $id]);
        }

        return redirect()->back()->withErrors(['Bukti Transaksi Harus di Upload Min 1']);

    }

    function saveOrder($request, $sessionUser) {


        $cart = DB::table('carts')
        ->where('user_id', $sessionUser->id)
        ->get();

		DB::beginTransaction();
        try {
            $grandtotal = 0;
            $total = 0;
            $qty = 0;
            foreach($cart as $product){
                $total += $product->price;
                $qty += $product->qty;
                $grandtotal += $product->total;
            }
            DB::table('orders')
            ->insert([
                'order_id' => 'ORD'.rand(10,100),
                'invoice_id' => 'INV'.rand(10,100),
                'qty' => $qty,
                'shipping_price' => 6000,
                'total' => $total,
                'discount' => 0,
                'grand_total' => $grandtotal,
                'shipping_address' => $sessionUser->address,
                'phone_number' => $sessionUser->mobile_phone,
                'email' => $sessionUser->email,
                'status' => 'WAITING PAYMENT',
                'user_id' => $sessionUser->id,
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $id = DB::getPdo()->lastInsertId();
            foreach($cart as $product){
                DB::table('mapping_order_products')
                ->insert([
                    'order_id' => $id,
                    'product_id' => $product->product_id,
                    'qty' => $product->qty,
                    'variant_id' => $product->variant_id,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
            }

            DB::table('carts')
			->where('user_id', $sessionUser->id)
			->delete();

            DB::commit();
		} catch(Exception $e){
			DB::rollback();

			return 0;
		}
		return $id;
	}


}
