@extends('backend.layouts.backend')

@section('content')

    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex align-items-start">
            <!--begin::Toolbar container-->
            <div class="d-flex flex-column flex-row-fluid">
                <!--begin::Toolbar wrapper-->
                <div class="d-flex align-items-center pt-1">
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">
                            <a href="../../demo34/dist/index.html" class="text-white text-hover-primary">
                                <i class="ki-outline ki-home text-gray-700 fs-6"></i>
                            </a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">Management Items</li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">Promotion</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Toolbar wrapper=-->
                <!--begin::Toolbar wrapper=-->
                <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-13 pb-6">
                    <!--begin::Page title-->
                    <div class="page-title me-5">
                        <!--begin::Title-->
                        <h1 class="page-heading d-flex text-white fw-bold fs-2 flex-column justify-content-center my-0">Promotion
                        <!--begin::Description-->
                        <span class="page-desc text-gray-700 fw-semibold fs-6 pt-3">List Product Promotion</span>
                        <!--end::Description--></h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Page title-->
                </div>
                <!--end::Toolbar wrapper=-->
            </div>
            <!--end::Toolbar container=-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Wrapper container-->
    <div class="app-container container-xxl">
        <!--begin::Main-->
        <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
            <!--begin::Content wrapper-->
            <div class="d-flex flex-column flex-column-fluid">
                <!--begin::Content-->
                <div id="kt_app_content" class="app-content flex-column-fluid">
                    <!--begin::Products-->
                    <div class="card card-flush" style="min-height: 100vh; height: auto" >
                        <!--begin::Card header-->
                        <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <!--begin::Search-->
                                <div class="d-flex align-items-center position-relative my-1">
                                    <i class="ki-outline ki-magnifier fs-3 position-absolute ms-4"></i>
                                    <input type="text" data-kt-ecommerce-product-filter="search" id="myInput" class="form-control form-control-solid w-250px ps-12" placeholder="Search Product" />
                                </div>
                                <!--end::Search-->
                            </div>
                            <!--end::Card title-->
                            <!--begin::Card toolbar-->
                            <div class="card-toolbar flex-row-fluid justify-content-end gap-5">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <select class="form-select form-control-solid  ps-12" id="product_type_id" name="product_type_id" data-control="select2" required data-placeholder="Select an option">
                                        <option value="FIX_PRICE">FIX PRICE</option>
                                        <option value="PERCENT">PERCENT</option>
                                    </select>
                                    <input type="text" class="form-control  w-250px ps-12" placeholder="Set Discount" />
                                    <button type="submit" class="btn btn-lg btn-primary">Save</button>
                                </div>
                            </div>
                            <!--end::Card toolbar-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0">
                            <!--begin::Table-->
                            <table id="example" class="table align-middle table-row-dashed fs-6 gy-5" style="width:100%">
                                <thead class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th>Product</th>
                                        <th>Description</th>
                                        <th>SKU</th>
                                    </tr>
                                </thead>
                            </table>
                            <!--end::Table-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Products-->
                </div>
                <!--end::Content-->
            </div>
            <!--end::Content wrapper-->
        </div>
        <!--end:::Main-->
@endsection

@section('onpage-js')
<script>
    $(document).ready(function() {
        function format(d) {
            var html = '<table id="example_child" class="table align-middle table-row-dashed fs-6 gy-5" style="width:100%"><thead class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">' +
                        '    <tr>' +
                        '         <th class="min-w-10px"></th>' +
                        '         <th class="min-w-200px">Variant Name</th>' +
                        '         <th class="text-end min-w-70px">Current Price</th>' +
                        '         <th class="text-end min-w-70px">Price</th>' +
                        '         <th class="text-end min-w-70px">QTY</th>' +
                        '     </tr>' +
                        '  </thead><tbody>';

            d.mapping_varian_products.forEach((element) => {
                html += '<tr>' +
                    '<td class="text-end pe-0"> <input type="checkbox" id="child_id_'+element.id+'" id="child_id_'+element.id+'" value="'+element.id+'"> </td>' +
                    '<td>' +
                        '    <div class="d-flex align-items-center">' +
                        '        <a href="#" class="symbol symbol-50px">' +
                        '            <span class="symbol-label" style="background-image:url(&#39;{{url("")}}/'+element.url+'&#39;)"></span>' +
                        '        </a>' +
                        '        <div class="ms-5">' +
                            '             <a href="" class="text-gray-800 text-hover-primary fs-5 fw-bold" data-kt-ecommerce-product-filter="product_name">'+element.name +'</a>' +
                            '        </div>' +
                            '    </div>' +
                    '</td>' +

                    '<td class="text-end pe-0">' +
                        element.currrent_price +
                    '</td>' +
                    '<td class="text-end pe-0">' +
                        element.price +
                    '</td>' +
                    '<td class="text-end pe-0">' +
                        element.qty +
                    '</td>' +
                '</tr>';
            });
            html += '</tbody> </table>';

            // `d` is the original data object for the row
            return (
                html
            );
        }

        $('#example_child').DataTable();
        var table = $('#example').DataTable({
            ajax: '{{route("backend.promotion.listdatatable")}}',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            columns: [
                {
                    className: 'dt-control',
                    orderable: false,
                    data: null,
                    defaultContent: ''
                },
                {
                    "data": "id",
                    "render": function ( data, type, row, meta ) {
                        if (type === 'display') {
                            return '  <input type="checkbox" id="parent_id_'+data+'" id="parent_id_'+data+'" value="'+data+'" onChange="setChild(this)"> <input type="checkbox>'
                        } else {
                            return data;
                        }
                    }
                },
                {
                    "data": "name",
                    "render": function ( data, type, row, meta ) {
                        if (type === 'display') {
                        return '    <div class="d-flex align-items-center">' +
                            '        <a href="#" class="symbol symbol-50px">' +
                            '            <span class="symbol-label" style="background-image:url(&#39;{{url("")}}/'+row.url_thumnail+'&#39;)"></span>' +
                            '        </a>' +
                            '        <div class="ms-5">' +
                            '             <a href="" class="text-gray-800 text-hover-primary fs-5 fw-bold" data-kt-ecommerce-product-filter="product_name">'+data +'</a>' +
                            '        </div>' +
                            '    </div>'
                        } else {
                        return data;
                        }
                    }
                },
                { data: 'description' },
                { data: 'sku' }
            ],
            order: [[1, 'asc']]
        });

        // Add event listener for opening and closing details
        $('#example tbody').on('click', 'td.dt-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
            }
            else {
                // Open this row
                row.child(format(row.data())).show();
            }
        });


        $('#myInput').on( 'keyup', function () {
            table.search( this.value ).draw();
        } );
	} );
    function setChild(data){
        var optionSelected = $("option:selected", data);
        var valueSelected = data.value;
        alert(valueSelected);
    }
</script>

@endsection
