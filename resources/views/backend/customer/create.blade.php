@extends('backend.layouts.backend')

@section('content')

					<!--begin::Toolbar-->
					<div id="kt_app_toolbar" class="app-toolbar py-6">
						<!--begin::Toolbar container-->
						<div id="kt_app_toolbar_container" class="app-container container-xxl d-flex align-items-start">
							<!--begin::Toolbar container-->
							<div class="d-flex flex-column flex-row-fluid">
								<!--begin::Toolbar wrapper-->
								<div class="d-flex align-items-center pt-1">
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-semibold">
										<!--begin::Item-->
										<li class="breadcrumb-item text-white fw-bold lh-1">
											<a href="{{ route('heykama.dashboard') }}" class="text-white text-hover-primary">
												<i class="ki-outline ki-home text-gray-700 fs-6"></i>
											</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-white fw-bold lh-1">Dashboards</li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Toolbar wrapper=-->
								<!--begin::Toolbar wrapper=-->
								<div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-13 pb-6">
									<!--begin::Page title-->
									<div class="page-title me-5">
										<!--begin::Title-->
										<h1 class="page-heading d-flex text-white fw-bold fs-2 flex-column justify-content-center my-0">Welcome back, Amanda
										<!--begin::Description-->
										<span class="page-desc text-gray-700 fw-semibold fs-6 pt-3">Your are #1 seller across market’s Marketing Category</span>
										<!--end::Description--></h1>
										<!--end::Title-->
									</div>
									<!--end::Page title-->
									<!--begin::Stats-->
									<div class="d-flex align-self-center flex-center flex-shrink-0">
										<a href="#" class="btn btn-flex btn-sm btn-outline btn-active-color-primary btn-custom px-4" data-bs-toggle="modal" data-bs-target="#kt_modal_invite_friends">
										<i class="ki-outline ki-plus-square fs-4 me-2"></i>Invite</a>
										<a href="#" class="btn btn-sm btn-active-color-primary btn-outline btn-custom ms-3 px-4" data-bs-toggle="modal" data-bs-target="#kt_modal_new_target">Set Your Target</a>
									</div>
									<!--end::Stats-->
								</div>
								<!--end::Toolbar wrapper=-->
							</div>
							<!--end::Toolbar container=-->
						</div>
						<!--end::Toolbar container-->
					</div>
					<!--end::Toolbar-->
					<!--begin::Wrapper container-->
					<div class="app-container container-xxl">
						<!--begin::Main-->
						<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
							<!--begin::Content wrapper-->
							<div class="d-flex flex-column flex-column-fluid">
								<!--begin::Content-->
								<div id="kt_app_content" class="app-content flex-column-fluid">
									<!--begin::Row-->
									<div class="row g-5 g-xl-8">
										<!--begin::Col-->
										<div class="col-xl-4">
											<div class="card card-flush mb-xl-8">
												<!--begin::Header-->
												<div class="card-header py-7">
													<!--begin::Statistics-->
													<div class="m-0">
														<!--begin::Heading-->
														<div class="d-flex align-items-center mb-2">
															<!--begin::Title-->
															<span class="fs-2hx fw-bold text-gray-800 me-2 lh-1 ls-n2">0.37%</span>
															<!--end::Title-->
															<!--begin::Badge-->
															<span class="badge badge-light-danger fs-base">
															<i class="ki-outline ki-arrow-up fs-5 text-danger ms-n1"></i>8.02%</span>
															<!--end::Badge-->
														</div>
														<!--end::Heading-->
														<!--begin::Description-->
														<span class="fs-6 fw-semibold text-gray-400">Online store convertion rate</span>
														<!--end::Description-->
													</div>
													<!--end::Statistics-->
													<!--begin::Toolbar-->
													<div class="card-toolbar">
														<!--begin::Menu-->
														<button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
															<i class="ki-outline ki-dots-square fs-1 text-gray-400 me-n1"></i>
														</button>
														<!--begin::Menu 2-->
														<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px" data-kt-menu="true">
															<!--begin::Menu item-->
															<div class="menu-item px-3">
																<div class="menu-content fs-6 text-dark fw-bold px-3 py-4">Quick Actions</div>
															</div>
															<!--end::Menu item-->
															<!--begin::Menu separator-->
															<div class="separator mb-3 opacity-75"></div>
															<!--end::Menu separator-->
															<!--begin::Menu item-->
															<div class="menu-item px-3">
																<a href="#" class="menu-link px-3">New Ticket</a>
															</div>
															<!--end::Menu item-->
															<!--begin::Menu item-->
															<div class="menu-item px-3">
																<a href="#" class="menu-link px-3">New Customer</a>
															</div>
															<!--end::Menu item-->
															<!--begin::Menu item-->
															<div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">
																<!--begin::Menu item-->
																<a href="#" class="menu-link px-3">
																	<span class="menu-title">New Group</span>
																	<span class="menu-arrow"></span>
																</a>
																<!--end::Menu item-->
																<!--begin::Menu sub-->
																<div class="menu-sub menu-sub-dropdown w-175px py-4">
																	<!--begin::Menu item-->
																	<div class="menu-item px-3">
																		<a href="#" class="menu-link px-3">Admin Group</a>
																	</div>
																	<!--end::Menu item-->
																	<!--begin::Menu item-->
																	<div class="menu-item px-3">
																		<a href="#" class="menu-link px-3">Staff Group</a>
																	</div>
																	<!--end::Menu item-->
																	<!--begin::Menu item-->
																	<div class="menu-item px-3">
																		<a href="#" class="menu-link px-3">Member Group</a>
																	</div>
																	<!--end::Menu item-->
																</div>
																<!--end::Menu sub-->
															</div>
															<!--end::Menu item-->
															<!--begin::Menu item-->
															<div class="menu-item px-3">
																<a href="#" class="menu-link px-3">New Contact</a>
															</div>
															<!--end::Menu item-->
															<!--begin::Menu separator-->
															<div class="separator mt-3 opacity-75"></div>
															<!--end::Menu separator-->
															<!--begin::Menu item-->
															<div class="menu-item px-3">
																<div class="menu-content px-3 py-3">
																	<a class="btn btn-primary btn-sm px-4" href="#">Generate Reports</a>
																</div>
															</div>
															<!--end::Menu item-->
														</div>
														<!--end::Menu 2-->
														<!--end::Menu-->
													</div>
													<!--end::Toolbar-->
												</div>
												<!--end::Header-->
												<!--begin::Body-->
												<div class="card-body pt-0">
													<!--begin::Items-->
													<div class="mb-0">
														<!--begin::Item-->
														<div class="d-flex flex-stack">
															<!--begin::Section-->
															<div class="d-flex align-items-center me-5">
																<!--begin::Symbol-->
																<div class="symbol symbol-30px me-5">
																	<span class="symbol-label">
																		<i class="ki-outline ki-magnifier fs-3 text-gray-600"></i>
																	</span>
																</div>
																<!--end::Symbol-->
																<!--begin::Content-->
																<div class="me-5">
																	<!--begin::Title-->
																	<a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Search Retargeting</a>
																	<!--end::Title-->
																	<!--begin::Desc-->
																	<span class="text-gray-400 fw-semibold fs-7 d-block text-start ps-0">Direct link clicks</span>
																	<!--end::Desc-->
																</div>
																<!--end::Content-->
															</div>
															<!--end::Section-->
															<!--begin::Wrapper-->
															<div class="d-flex align-items-center">
																<!--begin::Number-->
																<span class="text-gray-800 fw-bold fs-6 me-3">0.24%</span>
																<!--end::Number-->
																<!--begin::Info-->
																<div class="d-flex flex-center">
																	<!--begin::label-->
																	<span class="badge badge-light-success fs-base">
																	<i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i>2.4%</span>
																	<!--end::label-->
																</div>
																<!--end::Info-->
															</div>
															<!--end::Wrapper-->
														</div>
														<!--end::Item-->
														<!--begin::Separator-->
														<div class="separator separator-dashed my-3"></div>
														<!--end::Separator-->
														<!--begin::Item-->
														<div class="d-flex flex-stack">
															<!--begin::Section-->
															<div class="d-flex align-items-center me-5">
																<!--begin::Symbol-->
																<div class="symbol symbol-30px me-5">
																	<span class="symbol-label">
																		<i class="ki-outline ki-tiktok fs-3 text-gray-600"></i>
																	</span>
																</div>
																<!--end::Symbol-->
																<!--begin::Content-->
																<div class="me-5">
																	<!--begin::Title-->
																	<a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Social Retargeting</a>
																	<!--end::Title-->
																	<!--begin::Desc-->
																	<span class="text-gray-400 fw-semibold fs-7 d-block text-start ps-0">Direct link clicks</span>
																	<!--end::Desc-->
																</div>
																<!--end::Content-->
															</div>
															<!--end::Section-->
															<!--begin::Wrapper-->
															<div class="d-flex align-items-center">
																<!--begin::Number-->
																<span class="text-gray-800 fw-bold fs-6 me-3">0.94%</span>
																<!--end::Number-->
																<!--begin::Info-->
																<div class="d-flex flex-center">
																	<!--begin::label-->
																	<span class="badge badge-light-danger fs-base">
																	<i class="ki-outline ki-arrow-down fs-5 text-danger ms-n1"></i>9.4%</span>
																	<!--end::label-->
																</div>
																<!--end::Info-->
															</div>
															<!--end::Wrapper-->
														</div>
														<!--end::Item-->
														<!--begin::Separator-->
														<div class="separator separator-dashed my-3"></div>
														<!--end::Separator-->
														<!--begin::Item-->
														<div class="d-flex flex-stack">
															<!--begin::Section-->
															<div class="d-flex align-items-center me-5">
																<!--begin::Symbol-->
																<div class="symbol symbol-30px me-5">
																	<span class="symbol-label">
																		<i class="ki-outline ki-sms fs-3 text-gray-600"></i>
																	</span>
																</div>
																<!--end::Symbol-->
																<!--begin::Content-->
																<div class="me-5">
																	<!--begin::Title-->
																	<a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Email Retargeting</a>
																	<!--end::Title-->
																	<!--begin::Desc-->
																	<span class="text-gray-400 fw-semibold fs-7 d-block text-start ps-0">Direct link clicks</span>
																	<!--end::Desc-->
																</div>
																<!--end::Content-->
															</div>
															<!--end::Section-->
															<!--begin::Wrapper-->
															<div class="d-flex align-items-center">
																<!--begin::Number-->
																<span class="text-gray-800 fw-bold fs-6 me-3">1.23%</span>
																<!--end::Number-->
																<!--begin::Info-->
																<div class="d-flex flex-center">
																	<!--begin::label-->
																	<span class="badge badge-light-success fs-base">
																	<i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i>0.2%</span>
																	<!--end::label-->
																</div>
																<!--end::Info-->
															</div>
															<!--end::Wrapper-->
														</div>
														<!--end::Item-->
														<!--begin::Separator-->
														<div class="separator separator-dashed my-3"></div>
														<!--end::Separator-->
														<!--begin::Item-->
														<div class="d-flex flex-stack">
															<!--begin::Section-->
															<div class="d-flex align-items-center me-5">
																<!--begin::Symbol-->
																<div class="symbol symbol-30px me-5">
																	<span class="symbol-label">
																		<i class="ki-outline ki-icon fs-3 text-gray-600"></i>
																	</span>
																</div>
																<!--end::Symbol-->
																<!--begin::Content-->
																<div class="me-5">
																	<!--begin::Title-->
																	<a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Referrals Customers</a>
																	<!--end::Title-->
																	<!--begin::Desc-->
																	<span class="text-gray-400 fw-semibold fs-7 d-block text-start ps-0">Direct link clicks</span>
																	<!--end::Desc-->
																</div>
																<!--end::Content-->
															</div>
															<!--end::Section-->
															<!--begin::Wrapper-->
															<div class="d-flex align-items-center">
																<!--begin::Number-->
																<span class="text-gray-800 fw-bold fs-6 me-3">0.08%</span>
																<!--end::Number-->
																<!--begin::Info-->
																<div class="d-flex flex-center">
																	<!--begin::label-->
																	<span class="badge badge-light-danger fs-base">
																	<i class="ki-outline ki-arrow-down fs-5 text-danger ms-n1"></i>0.4%</span>
																	<!--end::label-->
																</div>
																<!--end::Info-->
															</div>
															<!--end::Wrapper-->
														</div>
														<!--end::Item-->
														<!--begin::Separator-->
														<div class="separator separator-dashed my-3"></div>
														<!--end::Separator-->
														<!--begin::Item-->
														<div class="d-flex flex-stack">
															<!--begin::Section-->
															<div class="d-flex align-items-center me-5">
																<!--begin::Symbol-->
																<div class="symbol symbol-30px me-5">
																	<span class="symbol-label">
																		<i class="ki-outline ki-abstract-25 fs-3 text-gray-600"></i>
																	</span>
																</div>
																<!--end::Symbol-->
																<!--begin::Content-->
																<div class="me-5">
																	<!--begin::Title-->
																	<a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Other</a>
																	<!--end::Title-->
																	<!--begin::Desc-->
																	<span class="text-gray-400 fw-semibold fs-7 d-block text-start ps-0">Direct link clicks</span>
																	<!--end::Desc-->
																</div>
																<!--end::Content-->
															</div>
															<!--end::Section-->
															<!--begin::Wrapper-->
															<div class="d-flex align-items-center">
																<!--begin::Number-->
																<span class="text-gray-800 fw-bold fs-6 me-3">0.46%</span>
																<!--end::Number-->
																<!--begin::Info-->
																<div class="d-flex flex-center">
																	<!--begin::label-->
																	<span class="badge badge-light-success fs-base">
																	<i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i>8.3%</span>
																	<!--end::label-->
																</div>
																<!--end::Info-->
															</div>
															<!--end::Wrapper-->
														</div>
														<!--end::Item-->
													</div>
													<!--end::Items-->
												</div>
												<!--end::Body-->
											</div>
											<!--end::List widget 7-->
										</div>
										<!--end::Col-->
										<!--begin::Col-->
										<div class="col-xl-8 ps-xl-12">
											<!--begin::Engage widget 1-->
											<div class="card bgi-position-y-bottom bgi-position-x-end bgi-no-repeat bgi-size-cover min-h-250px bg-body mb-5 mb-xl-8" style="background-position: 100% 50px;background-size: 500px auto;background-image:url('assets/media/misc/city.png')" dir="ltr">
												<!--begin::Body-->
												<div class="card-body d-flex flex-column justify-content-center ps-lg-12">
													<!--begin::Title-->
													<h3 class="text-dark fs-2qx fw-bold mb-7">We are working
													<br />to boost lovely mood</h3>
													<!--end::Title-->
													<!--begin::Action-->
													<div class="m-0">
														<a href='#' class="btn btn-dark fw-semibold px-6 py-3" data-bs-toggle="modal" data-bs-target="#kt_modal_create_app">Create a Store</a>
													</div>
													<!--begin::Action-->
												</div>
												<!--end::Body-->
											</div>
											<!--end::Engage widget 1-->
											<!--begin::Row-->
											<div class="row g-5 g-xl-8 mb-5 mb-xl-8">
												<!--begin::Col-->
												<div class="col-xl-6">
													<!--begin::Chart widget 45-->
													<div class="card card-flush h-xl-100 mb-xl-8">
														<!--begin::Header-->
														<div class="card-header pt-5">
															<!--begin::Title-->
															<h3 class="card-title fw-bold text-dark">Trends</h3>
															<!--end::Title-->
															<!--begin::Toolbar-->
															<div class="card-toolbar">
																<!--begin::Menu-->
																<button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
																	<i class="ki-outline ki-element-plus fs-2"></i>
																</button>
																<!--begin::Menu 3-->
																<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px py-3" data-kt-menu="true">
																	<!--begin::Heading-->
																	<div class="menu-item px-3">
																		<div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Payments</div>
																	</div>
																	<!--end::Heading-->
																	<!--begin::Menu item-->
																	<div class="menu-item px-3">
																		<a href="#" class="menu-link px-3">Create Invoice</a>
																	</div>
																	<!--end::Menu item-->
																	<!--begin::Menu item-->
																	<div class="menu-item px-3">
																		<a href="#" class="menu-link flex-stack px-3">Create Payment
																		<span class="ms-2" data-bs-toggle="tooltip" title="Specify a target name for future usage and reference">
																			<i class="ki-outline ki-information fs-6"></i>
																		</span></a>
																	</div>
																	<!--end::Menu item-->
																	<!--begin::Menu item-->
																	<div class="menu-item px-3">
																		<a href="#" class="menu-link px-3">Generate Bill</a>
																	</div>
																	<!--end::Menu item-->
																	<!--begin::Menu item-->
																	<div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-end">
																		<a href="#" class="menu-link px-3">
																			<span class="menu-title">Subscription</span>
																			<span class="menu-arrow"></span>
																		</a>
																		<!--begin::Menu sub-->
																		<div class="menu-sub menu-sub-dropdown w-175px py-4">
																			<!--begin::Menu item-->
																			<div class="menu-item px-3">
																				<a href="#" class="menu-link px-3">Plans</a>
																			</div>
																			<!--end::Menu item-->
																			<!--begin::Menu item-->
																			<div class="menu-item px-3">
																				<a href="#" class="menu-link px-3">Billing</a>
																			</div>
																			<!--end::Menu item-->
																			<!--begin::Menu item-->
																			<div class="menu-item px-3">
																				<a href="#" class="menu-link px-3">Statements</a>
																			</div>
																			<!--end::Menu item-->
																			<!--begin::Menu separator-->
																			<div class="separator my-2"></div>
																			<!--end::Menu separator-->
																			<!--begin::Menu item-->
																			<div class="menu-item px-3">
																				<div class="menu-content px-3">
																					<!--begin::Switch-->
																					<label class="form-check form-switch form-check-custom form-check-solid">
																						<!--begin::Input-->
																						<input class="form-check-input w-30px h-20px" type="checkbox" value="1" checked="checked" name="notifications" />
																						<!--end::Input-->
																						<!--end::Label-->
																						<span class="form-check-label text-muted fs-6">Recuring</span>
																						<!--end::Label-->
																					</label>
																					<!--end::Switch-->
																				</div>
																			</div>
																			<!--end::Menu item-->
																		</div>
																		<!--end::Menu sub-->
																	</div>
																	<!--end::Menu item-->
																	<!--begin::Menu item-->
																	<div class="menu-item px-3 my-1">
																		<a href="#" class="menu-link px-3">Settings</a>
																	</div>
																	<!--end::Menu item-->
																</div>
																<!--end::Menu 3-->
																<!--end::Menu-->
															</div>
															<!--end::Toolbar-->
														</div>
														<!--end::Header-->
														<!--begin::Body-->
														<div class="card-body d-flex justify-content-between flex-column pt-0">
															<!--begin::Chart-->
															<div class="m-0" id="kt_charts_widget_45" data-kt-chart-color="dark" style="height: 90px"></div>
															<!--end::Chart-->
															<!--begin::Items-->
															<div class="m-0">
																<!--begin::Item-->
																<div class="d-flex flex-stack mb-9">
																	<!--begin::Section-->
																	<div class="d-flex align-items-center me-2">
																		<!--begin::Symbol-->
																		<div class="symbol symbol-50px me-5">
																			<div class="symbol-label bg-light">
																				<img src="assets/media/svg/brand-logos/plurk.svg" class="h-50" alt="" />
																			</div>
																		</div>
																		<!--end::Symbol-->
																		<!--begin::Title-->
																		<div>
																			<a href="#" class="fs-6 text-gray-800 text-hover-primary fw-bold">Top Authors</a>
																			<div class="fs-7 text-muted fw-semibold mt-1">Successful Fellas</div>
																		</div>
																		<!--end::Title-->
																	</div>
																	<!--end::Section-->
																	<!--begin::Label-->
																	<div class="badge badge-light badge-lg fw-bold p-2 text-gray-600">+82$</div>
																	<!--end::Label-->
																</div>
																<!--end::Item-->
																<!--begin::Item-->
																<div class="d-flex flex-stack mb-9">
																	<!--begin::Section-->
																	<div class="d-flex align-items-center me-2">
																		<!--begin::Symbol-->
																		<div class="symbol symbol-50px me-5">
																			<div class="symbol-label bg-light">
																				<img src="assets/media/svg/brand-logos/telegram-2.svg" class="h-50" alt="" />
																			</div>
																		</div>
																		<!--end::Symbol-->
																		<!--begin::Title-->
																		<div>
																			<a href="#" class="fs-6 text-gray-800 text-hover-primary fw-bold">Binford Ltd.</a>
																			<div class="fs-7 text-muted fw-semibold mt-1">Most Successful</div>
																		</div>
																		<!--end::Title-->
																	</div>
																	<!--end::Section-->
																	<!--begin::Label-->
																	<div class="badge badge-light badge-lg fw-bold p-2 text-gray-600">+280$</div>
																	<!--end::Label-->
																</div>
																<!--end::Item-->
																<!--begin::Item-->
																<div class="d-flex flex-stack">
																	<!--begin::Section-->
																	<div class="d-flex align-items-center me-2">
																		<!--begin::Symbol-->
																		<div class="symbol symbol-50px me-5">
																			<div class="symbol-label bg-light">
																				<img src="assets/media/svg/brand-logos/vimeo.svg" class="h-50" alt="" />
																			</div>
																		</div>
																		<!--end::Symbol-->
																		<!--begin::Title-->
																		<div>
																			<a href="#" class="fs-6 text-gray-800 text-hover-primary fw-bold">Biffco Enterprises</a>
																			<div class="fs-7 text-muted fw-semibold mt-1">Most Successful Fellas</div>
																		</div>
																		<!--end::Title-->
																	</div>
																	<!--end::Section-->
																	<!--begin::Label-->
																	<div class="badge badge-light badge-lg fw-bold p-2 text-gray-600">+4500$</div>
																	<!--end::Label-->
																</div>
																<!--end::Item-->
															</div>
															<!--end::Items-->
														</div>
														<!--end::Body-->
													</div>
													<!--end::Chart widget 45-->
												</div>
												<!--end::Col-->
												<!--begin::Col-->
												<div class="col-xl-6">
													<!--begin::List widget 7-->
													<div class="card card-flush h-xl-100 mb-xl-8">
														<!--begin::Header-->
														<div class="card-header py-7">
															<!--begin::Statistics-->
															<div class="m-0">
																<!--begin::Heading-->
																<div class="d-flex align-items-center mb-2">
																	<!--begin::Title-->
																	<span class="fs-2hx fw-bold text-gray-800 me-2 lh-1 ls-n2">0.37%</span>
																	<!--end::Title-->
																	<!--begin::Badge-->
																	<span class="badge badge-light-danger fs-base">
																	<i class="ki-outline ki-arrow-up fs-5 text-danger ms-n1"></i>8.02%</span>
																	<!--end::Badge-->
																</div>
																<!--end::Heading-->
																<!--begin::Description-->
																<span class="fs-6 fw-semibold text-gray-400">Online store convertion rate</span>
																<!--end::Description-->
															</div>
															<!--end::Statistics-->
															<!--begin::Toolbar-->
															<div class="card-toolbar">
																<!--begin::Menu-->
																<button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
																	<i class="ki-outline ki-dots-square fs-1 text-gray-400 me-n1"></i>
																</button>
																<!--begin::Menu 2-->
																<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px" data-kt-menu="true">
																	<!--begin::Menu item-->
																	<div class="menu-item px-3">
																		<div class="menu-content fs-6 text-dark fw-bold px-3 py-4">Quick Actions</div>
																	</div>
																	<!--end::Menu item-->
																	<!--begin::Menu separator-->
																	<div class="separator mb-3 opacity-75"></div>
																	<!--end::Menu separator-->
																	<!--begin::Menu item-->
																	<div class="menu-item px-3">
																		<a href="#" class="menu-link px-3">New Ticket</a>
																	</div>
																	<!--end::Menu item-->
																	<!--begin::Menu item-->
																	<div class="menu-item px-3">
																		<a href="#" class="menu-link px-3">New Customer</a>
																	</div>
																	<!--end::Menu item-->
																	<!--begin::Menu item-->
																	<div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">
																		<!--begin::Menu item-->
																		<a href="#" class="menu-link px-3">
																			<span class="menu-title">New Group</span>
																			<span class="menu-arrow"></span>
																		</a>
																		<!--end::Menu item-->
																		<!--begin::Menu sub-->
																		<div class="menu-sub menu-sub-dropdown w-175px py-4">
																			<!--begin::Menu item-->
																			<div class="menu-item px-3">
																				<a href="#" class="menu-link px-3">Admin Group</a>
																			</div>
																			<!--end::Menu item-->
																			<!--begin::Menu item-->
																			<div class="menu-item px-3">
																				<a href="#" class="menu-link px-3">Staff Group</a>
																			</div>
																			<!--end::Menu item-->
																			<!--begin::Menu item-->
																			<div class="menu-item px-3">
																				<a href="#" class="menu-link px-3">Member Group</a>
																			</div>
																			<!--end::Menu item-->
																		</div>
																		<!--end::Menu sub-->
																	</div>
																	<!--end::Menu item-->
																	<!--begin::Menu item-->
																	<div class="menu-item px-3">
																		<a href="#" class="menu-link px-3">New Contact</a>
																	</div>
																	<!--end::Menu item-->
																	<!--begin::Menu separator-->
																	<div class="separator mt-3 opacity-75"></div>
																	<!--end::Menu separator-->
																	<!--begin::Menu item-->
																	<div class="menu-item px-3">
																		<div class="menu-content px-3 py-3">
																			<a class="btn btn-primary btn-sm px-4" href="#">Generate Reports</a>
																		</div>
																	</div>
																	<!--end::Menu item-->
																</div>
																<!--end::Menu 2-->
																<!--end::Menu-->
															</div>
															<!--end::Toolbar-->
														</div>
														<!--end::Header-->
														<!--begin::Body-->
														<div class="card-body pt-0">
															<!--begin::Items-->
															<div class="mb-0">
																<!--begin::Item-->
																<div class="d-flex flex-stack">
																	<!--begin::Section-->
																	<div class="d-flex align-items-center me-5">
																		<!--begin::Symbol-->
																		<div class="symbol symbol-30px me-5">
																			<span class="symbol-label">
																				<i class="ki-outline ki-magnifier fs-3 text-gray-600"></i>
																			</span>
																		</div>
																		<!--end::Symbol-->
																		<!--begin::Content-->
																		<div class="me-5">
																			<!--begin::Title-->
																			<a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Search Retargeting</a>
																			<!--end::Title-->
																			<!--begin::Desc-->
																			<span class="text-gray-400 fw-semibold fs-7 d-block text-start ps-0">Direct link clicks</span>
																			<!--end::Desc-->
																		</div>
																		<!--end::Content-->
																	</div>
																	<!--end::Section-->
																	<!--begin::Wrapper-->
																	<div class="d-flex align-items-center">
																		<!--begin::Number-->
																		<span class="text-gray-800 fw-bold fs-6 me-3">0.24%</span>
																		<!--end::Number-->
																		<!--begin::Info-->
																		<div class="d-flex flex-center">
																			<!--begin::label-->
																			<span class="badge badge-light-success fs-base">
																			<i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i>2.4%</span>
																			<!--end::label-->
																		</div>
																		<!--end::Info-->
																	</div>
																	<!--end::Wrapper-->
																</div>
																<!--end::Item-->
																<!--begin::Separator-->
																<div class="separator separator-dashed my-3"></div>
																<!--end::Separator-->
																<!--begin::Item-->
																<div class="d-flex flex-stack">
																	<!--begin::Section-->
																	<div class="d-flex align-items-center me-5">
																		<!--begin::Symbol-->
																		<div class="symbol symbol-30px me-5">
																			<span class="symbol-label">
																				<i class="ki-outline ki-tiktok fs-3 text-gray-600"></i>
																			</span>
																		</div>
																		<!--end::Symbol-->
																		<!--begin::Content-->
																		<div class="me-5">
																			<!--begin::Title-->
																			<a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Social Retargeting</a>
																			<!--end::Title-->
																			<!--begin::Desc-->
																			<span class="text-gray-400 fw-semibold fs-7 d-block text-start ps-0">Direct link clicks</span>
																			<!--end::Desc-->
																		</div>
																		<!--end::Content-->
																	</div>
																	<!--end::Section-->
																	<!--begin::Wrapper-->
																	<div class="d-flex align-items-center">
																		<!--begin::Number-->
																		<span class="text-gray-800 fw-bold fs-6 me-3">0.94%</span>
																		<!--end::Number-->
																		<!--begin::Info-->
																		<div class="d-flex flex-center">
																			<!--begin::label-->
																			<span class="badge badge-light-danger fs-base">
																			<i class="ki-outline ki-arrow-down fs-5 text-danger ms-n1"></i>9.4%</span>
																			<!--end::label-->
																		</div>
																		<!--end::Info-->
																	</div>
																	<!--end::Wrapper-->
																</div>
																<!--end::Item-->
																<!--begin::Separator-->
																<div class="separator separator-dashed my-3"></div>
																<!--end::Separator-->
																<!--begin::Item-->
																<div class="d-flex flex-stack">
																	<!--begin::Section-->
																	<div class="d-flex align-items-center me-5">
																		<!--begin::Symbol-->
																		<div class="symbol symbol-30px me-5">
																			<span class="symbol-label">
																				<i class="ki-outline ki-sms fs-3 text-gray-600"></i>
																			</span>
																		</div>
																		<!--end::Symbol-->
																		<!--begin::Content-->
																		<div class="me-5">
																			<!--begin::Title-->
																			<a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Email Retargeting</a>
																			<!--end::Title-->
																			<!--begin::Desc-->
																			<span class="text-gray-400 fw-semibold fs-7 d-block text-start ps-0">Direct link clicks</span>
																			<!--end::Desc-->
																		</div>
																		<!--end::Content-->
																	</div>
																	<!--end::Section-->
																	<!--begin::Wrapper-->
																	<div class="d-flex align-items-center">
																		<!--begin::Number-->
																		<span class="text-gray-800 fw-bold fs-6 me-3">1.23%</span>
																		<!--end::Number-->
																		<!--begin::Info-->
																		<div class="d-flex flex-center">
																			<!--begin::label-->
																			<span class="badge badge-light-success fs-base">
																			<i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i>0.2%</span>
																			<!--end::label-->
																		</div>
																		<!--end::Info-->
																	</div>
																	<!--end::Wrapper-->
																</div>
																<!--end::Item-->
																<!--begin::Separator-->
																<div class="separator separator-dashed my-3"></div>
																<!--end::Separator-->
																<!--begin::Item-->
																<div class="d-flex flex-stack">
																	<!--begin::Section-->
																	<div class="d-flex align-items-center me-5">
																		<!--begin::Symbol-->
																		<div class="symbol symbol-30px me-5">
																			<span class="symbol-label">
																				<i class="ki-outline ki-icon fs-3 text-gray-600"></i>
																			</span>
																		</div>
																		<!--end::Symbol-->
																		<!--begin::Content-->
																		<div class="me-5">
																			<!--begin::Title-->
																			<a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Referrals Customers</a>
																			<!--end::Title-->
																			<!--begin::Desc-->
																			<span class="text-gray-400 fw-semibold fs-7 d-block text-start ps-0">Direct link clicks</span>
																			<!--end::Desc-->
																		</div>
																		<!--end::Content-->
																	</div>
																	<!--end::Section-->
																	<!--begin::Wrapper-->
																	<div class="d-flex align-items-center">
																		<!--begin::Number-->
																		<span class="text-gray-800 fw-bold fs-6 me-3">0.08%</span>
																		<!--end::Number-->
																		<!--begin::Info-->
																		<div class="d-flex flex-center">
																			<!--begin::label-->
																			<span class="badge badge-light-danger fs-base">
																			<i class="ki-outline ki-arrow-down fs-5 text-danger ms-n1"></i>0.4%</span>
																			<!--end::label-->
																		</div>
																		<!--end::Info-->
																	</div>
																	<!--end::Wrapper-->
																</div>
																<!--end::Item-->
																<!--begin::Separator-->
																<div class="separator separator-dashed my-3"></div>
																<!--end::Separator-->
																<!--begin::Item-->
																<div class="d-flex flex-stack">
																	<!--begin::Section-->
																	<div class="d-flex align-items-center me-5">
																		<!--begin::Symbol-->
																		<div class="symbol symbol-30px me-5">
																			<span class="symbol-label">
																				<i class="ki-outline ki-abstract-25 fs-3 text-gray-600"></i>
																			</span>
																		</div>
																		<!--end::Symbol-->
																		<!--begin::Content-->
																		<div class="me-5">
																			<!--begin::Title-->
																			<a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Other</a>
																			<!--end::Title-->
																			<!--begin::Desc-->
																			<span class="text-gray-400 fw-semibold fs-7 d-block text-start ps-0">Direct link clicks</span>
																			<!--end::Desc-->
																		</div>
																		<!--end::Content-->
																	</div>
																	<!--end::Section-->
																	<!--begin::Wrapper-->
																	<div class="d-flex align-items-center">
																		<!--begin::Number-->
																		<span class="text-gray-800 fw-bold fs-6 me-3">0.46%</span>
																		<!--end::Number-->
																		<!--begin::Info-->
																		<div class="d-flex flex-center">
																			<!--begin::label-->
																			<span class="badge badge-light-success fs-base">
																			<i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i>8.3%</span>
																			<!--end::label-->
																		</div>
																		<!--end::Info-->
																	</div>
																	<!--end::Wrapper-->
																</div>
																<!--end::Item-->
															</div>
															<!--end::Items-->
														</div>
														<!--end::Body-->
													</div>
													<!--end::List widget 7-->
												</div>
												<!--end::Col-->
											</div>
										</div>
										<!--end::Col-->
									</div>
									<!--end::Row-->
								</div>
								<!--end::Content-->
							</div>
							<!--end::Content wrapper-->
							<!--begin::Footer-->
							<div id="kt_app_footer" class="app-footer d-flex flex-column flex-md-row align-items-center flex-center flex-md-stack py-2 py-lg-4">
								<!--begin::Copyright-->
								<div class="text-dark order-2 order-md-1">
									<span class="text-muted fw-semibold me-1">2023&copy;</span>
									<a href="https://keenthemes.com" target="_blank" class="text-gray-800 text-hover-primary">Keenthemes</a>
								</div>
								<!--end::Copyright-->
								<!--begin::Menu-->
								<ul class="menu menu-gray-600 menu-hover-primary fw-semibold order-1">
									<li class="menu-item">
										<a href="https://keenthemes.com" target="_blank" class="menu-link px-2">About</a>
									</li>
									<li class="menu-item">
										<a href="https://devs.keenthemes.com" target="_blank" class="menu-link px-2">Support</a>
									</li>
									<li class="menu-item">
										<a href="https://1.envato.market/EA4JP" target="_blank" class="menu-link px-2">Purchase</a>
									</li>
								</ul>
								<!--end::Menu-->
							</div>
							<!--end::Footer-->
						</div>
						<!--end:::Main-->
					</div>
					<!--end::Wrapper container-->

@endsection

@section('customjs')
