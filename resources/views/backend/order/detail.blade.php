@extends('backend.layouts.backend')

@section('content')

    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex align-items-start">
            <!--begin::Toolbar container-->
            <div class="d-flex flex-column flex-row-fluid">
                <!--begin::Toolbar wrapper-->
                <div class="d-flex align-items-center pt-1">
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">
                            <a href="../../demo34/dist/index.html" class="text-white text-hover-primary">
                                <i class="ki-outline ki-home text-gray-700 fs-6"></i>
                            </a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">Sales</li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">Order </li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Toolbar wrapper=-->
                <!--begin::Toolbar wrapper=-->
                <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-13 pb-6">
                    <!--begin::Page title-->
                    <div class="page-title me-5">
                        <!--begin::Title-->
                        <h1 class="page-heading d-flex text-white fw-bold fs-2 flex-column justify-content-center my-0">Detail Invoice
                        <!--begin::Description-->
                        <span class="page-desc text-gray-700 fw-semibold fs-6 pt-3">Page Description</span>
                        <!--end::Description--></h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Page title-->
                </div>
                <!--end::Toolbar wrapper=-->
            </div>
            <!--end::Toolbar container=-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Wrapper container-->
    <div class="app-container container-xxl">
        <!--begin::Main-->
        <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
            <!--begin::Content wrapper-->
            <div class="d-flex flex-column flex-column-fluid">
                <!--begin::Content-->
                <div id="kt_app_content" class="app-content flex-column-fluid">
                    <!--begin::Products-->
                    <div class="card card-flush" >
                        <!--begin::Card body-->

                        <!-- begin::Body-->
                        <div class="card-body py-20">
                            <!-- begin::Wrapper-->
                            <div class="mw-lg-950px mx-auto w-100">
                                <!-- begin::Header-->
                                <div class="d-flex justify-content-between flex-column flex-sm-row mb-19">
                                    <h4 class="fw-bolder text-gray-800 fs-2qx pe-5 pb-7">INVOICE</h4>
                                    <!--end::Logo-->
                                    <div class="text-sm-end">
                                        <!--begin::Logo-->
                                        <a href="#" class="d-block mw-150px ms-sm-auto">
                                            <img alt="Logo" src="{{ asset('assets/media/logos/Logo-03.png') }}" class="w-100" />
                                        </a>
                                        <!--end::Logo-->
                                        <!--begin::Text-->
                                        <!--div class="text-sm-end fw-semibold fs-4 text-muted mt-7">
                                            <div>Cecilia Chapman, 711-2880 Nulla St, Mankato</div>
                                            <div>Mississippi 96522</div>
                                        </div-->
                                        <!--end::Text-->
                                    </div>
                                </div>
                                <!--end::Header-->
                                <!--begin::Body-->
                                <div class="pb-12">
                                    <!--begin::Wrapper-->
                                    <div class="d-flex flex-column gap-7 gap-md-10">
                                        <!--begin::Message-->
                                        <div class="fw-bold fs-2">Dear {{@$userorder->name}}
                                        <span class="fs-6">({{@$userorder->email}})</span>,
                                        <br />
                                        <span class="text-muted fs-5">Here are your order details. We thank you for your purchase.</span></div>
                                        <!--begin::Message-->
                                        <!--begin::Separator-->
                                        <div class="separator"></div>
                                        <!--begin::Separator-->
                                        <!--begin::Order details-->
                                        <div class="d-flex flex-column flex-sm-row gap-7 gap-md-10 fw-bold">
                                            <div class="flex-root d-flex flex-column">
                                                <span class="text-muted">Order ID</span>
                                                <span class="fs-5">#{{@$listorder->order_id}}</span>
                                            </div>
                                            <div class="flex-root d-flex flex-column">
                                                <span class="text-muted">Date</span>
                                                <span class="fs-5">{{@$listorder->created_at}}</span>
                                            </div>
                                            <div class="flex-root d-flex flex-column">
                                                <span class="text-muted">Invoice ID</span>
                                                <span class="fs-5">#{{@$listorder->invoice_id}}</span>
                                            </div>
                                        </div>
                                        <!--end::Order details-->
                                        <!--begin::Billing & shipping-->
                                        <div class="d-flex flex-column flex-sm-row gap-7 gap-md-10 fw-bold">
                                            <div class="flex-root d-flex flex-column">
                                                <span class="text-muted">Billing Address</span>
                                                <span class="fs-6">
                                                    {{@$listorder->shipping_address}}
                                                </span>
                                            </div>
                                            <div class="flex-root d-flex flex-column">
                                                <span class="text-muted">Shipping Address</span>
                                                <span class="fs-6">{{@$listorder->shipping_address}}</span>
                                            </div>
                                        </div>
                                        <!--end::Billing & shipping-->
                                        <!--begin:Order summary-->
                                        <div class="d-flex justify-content-between flex-column">
                                            <!--begin::Table-->
                                            <div class="table-responsive border-bottom mb-9">
                                                <table class="table align-middle table-row-dashed fs-6 gy-5 mb-0">
                                                    <thead>
                                                        <tr class="border-bottom fs-6 fw-bold text-muted">
                                                            <th class="min-w-175px pb-2">Products</th>
                                                            <th class="min-w-70px text-end pb-2">SKU</th>
                                                            <th class="min-w-100px text-end pb-2">Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="fw-semibold text-gray-600">
                                                        @foreach($listorder->product as $product)
                                                        <tr>
                                                            <td>
                                                                <div class="d-flex align-items-center">
                                                                    <!--begin::Thumbnail-->
                                                                    <a href="#" class="symbol symbol-50px">
                                                                        <span class="symbol-label" style="background-image:url('{{ asset($product->url_thumnail)}}')"></span>
                                                                    </a>
                                                                    <!--end::Thumbnail-->
                                                                    <!--begin::Title-->
                                                                    <div class="ms-5">
                                                                        <div class="fw-bold">{{$product->name}}</div>
                                                                        <!--div class="fs-7 text-muted">Delivery Date: 19/07/2023</div-->
                                                                    </div>
                                                                    <!--end::Title-->
                                                                </div>
                                                            </td>
                                                            <td class="text-end">{{$product->sku}}</td>
                                                            <td class="text-end">{{$product->order_qty}}</td>
                                                            <td class="text-end">{{$product->selling_price}}</td>
                                                        </tr>

                                                        @endforeach
                                                        <tr>
                                                            <td colspan="3" class="text-end">Subtotal</td>
                                                            <td class="text-end">{{$listorder->total}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" class="text-end">VAT (0%)</td>
                                                            <td class="text-end">{{$listorder->discount}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" class="text-end">Shipping Rate</td>
                                                            <td class="text-end">{{$listorder->shipping_price}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" class="fs-3 text-dark fw-bold text-end">Grand Total</td>
                                                            <td class="text-dark fs-3 fw-bolder text-end">{{$listorder->grand_total}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--end::Table-->
                                        </div>
                                        <!--end:Order summary-->
                                    </div>
                                    <!--end::Wrapper-->
                                </div>
                                <!--end::Body-->
                                <!-- begin::Footer-->
                                <div class="d-flex flex-stack flex-wrap mt-lg-20 pt-13">
                                    <!-- begin::Actions-->
                                    <div class="my-1 me-5">
                                        <!-- begin::Pint-->
                                        <button type="button" class="btn btn-success my-1 me-12" onclick="window.print();">Print Invoice</button>
                                        <!-- end::Pint-->
                                        <!-- begin::Download-->
                                        <button type="button" class="btn btn-light-success my-1">Download</button>
                                        <!-- end::Download-->
                                    </div>
                                    <!-- end::Actions-->
                                    <!-- begin::Action-->
                                    <a href="../../demo34/dist/apps/invoices/create.html" class="btn btn-primary my-1">Create Invoice</a>
                                    <!-- end::Action-->
                                </div>
                                <!-- end::Footer-->
                            </div>
                            <!-- end::Wrapper-->
                        </div>
                        <!-- end::Body-->
                        <!--end::Card body-->
                    </div>
                    <!--end::Products-->
                </div>
                <!--end::Content-->
            </div>
            <!--end::Content wrapper-->
        </div>
        <!--end:::Main-->
@endsection

