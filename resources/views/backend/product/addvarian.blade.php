@extends('backend.layouts.backend')

@section('content')
<style>
    .wall {
        padding: 10px;
        border: 1px solid #DBDFE9;
    }
    .brick { width:100px; height:100px; background:#f2f2f2;}
    .tile-picker  {
    position:relative;
    cursor:pointer;
    background-position: center center;
    background-size: cover;
    box-shadow:inset 0 0 10px rgba(0,0,0,.1);
    outline: 2px solid #fff;
    outline-offset: -3px;
    border:1px solid #bbb;  }

    .active.tile-picker { border-color: #aaa; }

    .tile-picker input[type=checkbox] {
    opacity:0;
    position:absolute;
    left:-80px;  }

    .tile-checked {
    display:block;
    font-style:normal;
    width:20px; height:20px;
    position:absolute;
    top:-2px;
    right:-4px;
    }

    .tile-checked:after {
    content:'\2713';
    display:block;
    line-height:18px; width:18px; height:18px;

    background-color:#1481b8; color:#fff;
    border-radius:2px;
    font-size:13px;
    text-align:center; font-weight:bold;

    opacity:0;
    transition: opacity .34s ease-in-out; }


    input[type=checkbox]:checked ~ .tile-checked:after {
    opacity:1; }
</style>
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex align-items-start">
            <!--begin::Toolbar container-->
            <div class="d-flex flex-column flex-row-fluid">
                <!--begin::Toolbar wrapper-->
                <div class="d-flex align-items-center pt-1">
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">
                            <a href="../../demo34/dist/index.html" class="text-white text-hover-primary">
                                <i class="ki-outline ki-home text-gray-700 fs-6"></i>
                            </a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">Catalog</li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">Product</li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">Add Product</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Toolbar wrapper=-->
                <!--begin::Toolbar wrapper=-->
                <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-13 pb-6">
                    <!--begin::Page title-->
                    <div class="page-title me-5">
                        <!--begin::Title-->
                        <h1 class="page-heading d-flex text-white fw-bold fs-2 flex-column justify-content-center my-0">Product Form
                        <!--begin::Description-->
                        <span class="page-desc text-gray-700 fw-semibold fs-6 pt-3">Create Product</span>
                        <!--end::Description--></h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Page title-->
                </div>
                <!--end::Toolbar wrapper=-->
            </div>
            <!--end::Toolbar container=-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Wrapper container-->
    <div class="app-container container-xxl">
        <!--begin::Main-->
        <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
            <!--begin::Content wrapper-->
            <div class="d-flex flex-column flex-column-fluid">
                <!--begin::Content-->
                <div id="kt_app_content" class="app-content flex-column-fluid">
                    <!--begin::Card-->
                    <div class="card">
                        <!--begin::Card body-->
                        <div class="card-body">
                            <!--begin::Stepper-->
                            <div class="stepper stepper-links d-flex flex-column pt-15" id="kt_create_account_stepper">
                                <!--begin::Nav-->
                                <div class="stepper-nav mb-5">
                                    <!--begin::Step 1-->
                                    <div class="stepper-item " data-kt-stepper-element="nav">
                                        <h3 class="stepper-title">Product</h3>
                                    </div>
                                    <!--end::Step 1-->
                                    <!--begin::Step 3-->
                                    <div class="stepper-item current" data-kt-stepper-element="nav">
                                        <h3 class="stepper-title">Varian & Price</h3>
                                    </div>
                                    <!--end::Step 3-->
                                </div>
                                <!--end::Nav-->
                                <!--begin::Form-->
                                <form class="mx-auto mw-600px w-100 pt-15 pb-10" action="{{route('backend.product.savevariant')}}" enctype="multipart/form-data" method="POST" id="kt_create_account_form">


                                    @csrf
                                    <input type="hidden" name="id" id="id" class="form-control mb-2" placeholder="Product name"  value="{{$id}}" required />
                                    <!--begin::Step 1-->
                                    <div data-kt-stepper-element="content">

                                    </div>
                                    <!--end::Step 1-->
                                    <!--begin::Step 3-->
                                    <div data-kt-stepper-element="content" class="current">
                                        <!--begin::Wrapper-->
                                        <div class="w-100">
                                            <!--begin::Heading-->
                                            <div class="pb-10 pb-lg-12" id="variant0">
                                                <div class="row" >
                                                    <!--begin::Input group-->

                                                    <div class="mb-10 fv-row col-12">
                                                        <!--begin::Label-->
                                                        <label class="required form-label">Variant Name</label>
                                                        <!--end::Label-->
                                                        <!--begin::Input-->
                                                        <input type="text" name="variant_name[]" id="variant_name[]" class="form-control mb-2" placeholder="Variant name"  value="{{ old('product_name') }}" required />
                                                        <!--end::Input-->
                                                        <!--begin::Description-->
                                                        <div class="text-muted fs-7">A Variant name is required and recommended to be unique.</div>
                                                        <!--end::Description-->
                                                    </div>
                                                    <div class="mb-10 fv-row col-6">
                                                        <label class="required form-label">Current Price</label>
                                                    <input type="text" name="current_price[]" id="current_price[]" class="form-control mb-2" placeholder="Variant name"  value="{{ old('product_name') }}" required />
                                                    </div>
                                                    <div class="mb-10 fv-row col-6">
                                                        <label class="required form-label">Selling Price</label>
                                                        <input type="text" name="price[]" id="price[]" class="form-control mb-2" placeholder="Variant name"  value="{{ old('product_name') }}" required />
                                                    </div>

                                                    <div class="mb-10 fv-row col-12">
                                                        <label class="required form-label">Quantity</label>
                                                        <div class="d-flex gap-3">
                                                            <input type="number" id="qty[]" name="qty[]" class="form-control mb-2" placeholder="In warehouse"  value="{{ old('qty') ? 0 :  old('qty')}}" required />
                                                        </div>
                                                        <div class="text-muted fs-7">Enter the product quantity.</div>
                                                    </div>
                                                    <div class="mb-10 fv-row col-12">
                                                        <label class="required form-label">Thumnails</label>
                                                        <div class="wall">
                                                            @foreach($listproductimage as $product)
                                                                <label class="brick tile-picker" style="background-image:url('{{ asset($product->url) }}')">
                                                                <input type="checkbox"  id="thumnails[]" name="thumnails[]" value='{{$product->id}}'>
                                                                    <i class="tile-checked"></i>
                                                                </label>
                                                            @endforeach
                                                        </div>
                                                        <div class="text-muted fs-7">Select Thumnail.</div>
                                                    </div>

                                                 </div>
                                            </div>
                                            <div id="addvariant"></div>
                                            <a href="javascript:void(0)" class="btn btn-primary er fs-6 px-8 py-4" onclick="addVariant()">Add Another Variant</a>
                                            <!--end::Heading-->
                                        </div>
                                        <!--end::Wrapper-->
                                    </div>
                                    <!--end::Step 3-->
                                    <!--begin::Step 4-->
                                    <div data-kt-stepper-element="content">
                                    </div>
                                    <!--end::Step 4-->
                                    <!--begin::Actions-->
                                    <div class="d-flex flex-stack pt-15">
                                        <!--begin::Wrapper-->
                                        <div class="mr-2">
                                            <button type="button" class="btn btn-lg btn-light-primary me-3" data-kt-stepper-action="previous">
                                            <i class="ki-outline ki-arrow-left fs-4 me-1"></i>Back</button>
                                        </div>
                                        <!--end::Wrapper-->
                                        <!--begin::Wrapper-->
                                        <div>
                                            <button type="submit" class="btn btn-lg btn-primary">Publish
                                            <i class="ki-outline ki-arrow-right fs-4 ms-1 me-0"></i></button>
                                        </div>
                                        <!--end::Wrapper-->
                                    </div>
                                    <!--end::Actions-->
                                </form>
                                <!--end::Form-->
                            </div>
                            <!--end::Stepper-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Card-->
                </div>
                <!--end::Content-->
            </div>
            <!--end::Content wrapper-->
        </div>
        <!--end:::Main-->
@endsection

@section('onpage-js')

<script type="text/javascript">

    $(document).ready(function() {
        $(document).on("click", ".tile-picker input", function(e) {
        if ($(this).is(":checked")) {
            $(this).closest(".tile-picker").addClass("active");
        } else {
            $(this).closest(".tile-picker").removeClass("active");
        }
        });

        $('input[type="checkbox"]').on('change', function() {
            $('input[type="checkbox"]').not(this).prop('checked', false);
        });
    });
    var variant_id  = 1
    function addVariant(){
        var html = '<div class="pb-10 pb-lg-12" id="variant'+variant_id+'">'+
                        '<div class="row" >'+
                            '<div class="mb-10 fv-row col-12">'+
                            ' <label class="required form-label">Variant Name</label>'+
                            '  <input type="text" name="variant_name[]" id="variant_name[]" class="form-control mb-2" placeholder="Variant name"  value="{{ old("product_name") }}" required />'+
                            '   <div class="text-muted fs-7">A Variant name is required and recommended to be unique.</div>'+
                            '</div>'+
                            '<div class="mb-10 fv-row col-6">'+
                            '    <label class="required form-label">Current Price</label>'+
                            '    <input type="text" name="current_price[]" id="current_price[]" class="form-control mb-2" placeholder="Variant name"  value="{{ old("current_price") }}" required />'+
                            '</div>'+
                            '<div class="mb-10 fv-row col-6" >'+
                            '        <label class="required form-label">Selling Price</label>'+
                            '    <input type="text" name="price[]" id="price[]" class="form-control mb-2" placeholder="Variant name"  value="{{ old("price") }}" required />'+
                            '</div>'+
                            '<div class="mb-10 fv-row col-12">'+
                            '    <label class="required form-label">Quantity</label>'+
                            '    <div class="d-flex gap-3">'+
                            '        <input type="number" id="qty[]" name="qty[]" class="form-control mb-2" placeholder="In warehouse"  value="{{ old("qty") ? 0 :  old("qty")}}" required />'+
                            '    </div>'+
                            '    <div class="text-muted fs-7">Enter the product quantity.</div>'+
                            '</div>'+
                            '<div class="mb-10 fv-row col-12">'+
                                '    <label class="required form-label">Thumnails</label>'+
                                '    <div class="wall">'+
                                            @foreach($listproductimage as $product)
                                            @php($url = asset($product->url))
                                    `            <label class="brick tile-picker" style="background-image:url('{{$url}}')">`+
                                        '            <input type="checkbox"  id="thumnails[]" name="thumnails[]" value="{{$product->id}}">'+
                                        '                <i class="tile-checked"></i>'+
                                        '            </label>'+
                                                 @endforeach
                                        '    </div>'+
                                        '     <div class="text-muted fs-7">Select Thumnail.</div>'+
                                        ' </div>'+
                            '     <div class="text-muted fs-7">Set the product media gallery.</div>'+
                            '  <a href="javascript:void(0)" class="btn btn-danger er fs-6 px-8 py-4" onclick="removeVariant('+variant_id+')">Remove</a>'
                            '    </div>';
                        '    </div>';
                    '    </div>';
        $('#addvariant').append(html);

        variant_id++;
    }
    function removeVariant(variant_id){
        $('#variant'+variant_id).empty();

        variant_id--;
    }

    @if($errors->any())
        @foreach($errors->all() as $error)
        swal("Failed!", "{!! $error !!}", "error");
        @endforeach
    @endif
</script>

@endsection
