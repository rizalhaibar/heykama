
<style>
    .wall {
        padding: 10px;
        border: 1px solid #DBDFE9;
    }
    .brick { width:100px; height:100px; background:#f2f2f2;}
    .tile-picker  {
    position:relative;
    cursor:pointer;
    background-position: center center;
    background-size: cover;
    box-shadow:inset 0 0 10px rgba(0,0,0,.1);
    outline: 2px solid #fff;
    outline-offset: -3px;
    border:1px solid #bbb;  }

    .active.tile-picker { border-color: #aaa; }

    .tile-picker input[type=checkbox] {
    opacity:0;
    position:absolute;
    left:-80px;  }

    .tile-checked {
    display:block;
    font-style:normal;
    width:20px; height:20px;
    position:absolute;
    top:-2px;
    right:-4px;
    }

    .tile-checked:after {
    content:'\2713';
    display:block;
    line-height:18px; width:18px; height:18px;

    background-color:#1481b8; color:#fff;
    border-radius:2px;
    font-size:13px;
    text-align:center; font-weight:bold;

    opacity:0;
    transition: opacity .34s ease-in-out; }


    input[type=checkbox]:checked ~ .tile-checked:after {
    opacity:1; }
</style>

<!--begin::Form-->
<form class="mx-auto mw-600px w-100 pt-15 pb-10" action="{{route('backend.product.updatevariant')}}" enctype="multipart/form-data" method="POST" id="kt_create_account_form">
    @csrf
    @if($errors->any())
        @foreach($errors->all() as $error)
        {!! $error !!}
        @endforeach
    @endif
    <input type="hidden" name="id" id="id" class="form-control mb-2" placeholder="Product name"  value="{{$id}}" required />
    <!--begin::Step 1-->
    <div data-kt-stepper-element="content">

    </div>
    <!--end::Step 1-->
    <!--begin::Step 3-->
    <div data-kt-stepper-element="content" class="current">
        <!--begin::Wrapper-->
        <div class="w-100">
            <!--begin::Heading-->
        @php($i = 0)
        @if(isset($mpv))
                <div class="pb-10 pb-lg-12" id="variant{{$i}}">
                    <div class="row" >
                        <!--begin::Input group-->

                        <div class="mb-10 fv-row col-12">
                            <!--begin::Label-->
                            <label class="required form-label">Variant Name</label>
                            <!--end::Label-->
                            <!--begin::Input-->
                            <input type="hidden" name="id_variant[]" id="id_variant[]" class="form-control mb-2" placeholder="Variant name"  value="{{ $mpv->id }}" required />
                            <input type="text" name="variant_name[]" id="variant_name[]" class="form-control mb-2" placeholder="Variant name"  value="{{ $mpv->name }}" required />
                            <!--end::Input-->
                            <!--begin::Description-->
                            <div class="text-muted fs-7">A Variant name is required and recommended to be unique.</div>
                            <!--end::Description-->
                        </div>
                        <div class="mb-10 fv-row col-6">
                            <label class="required form-label">Current Price</label>
                        <input type="text" name="current_price[]" id="current_price[]" class="form-control mb-2" placeholder="Variant name"  value="{{ $mpv->currrent_price }}" required />
                        </div>
                        <div class="mb-10 fv-row col-6">
                            <label class="required form-label">Selling Price</label>
                            <input type="text" name="price[]" id="price[]" class="form-control mb-2" placeholder="Variant name"  value="{{ $mpv->price }}" required />
                        </div>

                        <div class="mb-10 fv-row col-12">
                            <label class="required form-label">Quantity</label>
                            <div class="d-flex gap-3">
                                <input type="number" id="qty[]" name="qty[]" class="form-control mb-2" placeholder="In warehouse"  value="{{ $mpv->qty }}" required />
                            </div>
                            <div class="text-muted fs-7">Enter the product quantity.</div>
                        </div>
                        <div class="mb-10 fv-row col-12">
                            <label class="required form-label">Thumnails</label>
                            <div class="wall">
                                @foreach($listproductimage as $img)
                                @if($mpv->mapping_image_varian_products)
                                    @foreach($mpv->mapping_image_varian_products as $mivp)
                                        <label class="brick tile-picker" style="background-image:url('{{ asset($img->url) }}')">
                                        <input type="checkbox"  id="thumnails[]" name="thumnails[]" {{$mivp->id == $img->id ? 'checked' : '' }} value='{{$img->id}}'>
                                            <i class="tile-checked"></i>
                                        </label>
                                    @endforeach
                                @else
                                        <label class="brick tile-picker" style="background-image:url('{{ asset($img->url) }}')">
                                        <input type="checkbox"  id="thumnails[]" name="thumnails[]" value='{{$img->id}}'>
                                            <i class="tile-checked"></i>
                                        </label>
                                @endif
                            @endforeach
                            </div>
                            <div class="text-muted fs-7">Select Thumnail.</div>
                        </div>

                    </div>
                </div>
                <!--end::Heading-->
            </div>

        @endif
    </div>


    <!--end::Step 3-->
    <!--begin::Step 4-->
    <div data-kt-stepper-element="content">
    </div>
    <!--end::Step 4-->
    <!--begin::Actions-->
    <div class="d-flex flex-stack pt-15">
        <!--begin::Wrapper-->
        <div>
            <button type="submit" class="btn btn-lg btn-primary">Publish
            <i class="ki-outline ki-arrow-right fs-4 ms-1 me-0"></i></button>
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Actions-->
</form>
<!--end::Form-->


<script type="text/javascript">

    $(document).ready(function() {
        $(document).on("click", ".tile-picker input", function(e) {
        if ($(this).is(":checked")) {
            $(this).closest(".tile-picker").addClass("active");
        } else {
            $(this).closest(".tile-picker").removeClass("active");
        }
        });

        $('input[type="checkbox"]').on('change', function() {
            $('input[type="checkbox"]').not(this).prop('checked', false);
        });
    });

</script>

