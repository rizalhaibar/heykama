@extends('backend.layouts.backend')

@section('content')

    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex align-items-start">
            <!--begin::Toolbar container-->
            <div class="d-flex flex-column flex-row-fluid">
                <!--begin::Toolbar wrapper-->
                <div class="d-flex align-items-center pt-1">
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">
                            <a href="../../demo34/dist/index.html" class="text-white text-hover-primary">
                                <i class="ki-outline ki-home text-gray-700 fs-6"></i>
                            </a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">Catalog</li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">Product</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Toolbar wrapper=-->
                <!--begin::Toolbar wrapper=-->
                <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-13 pb-6">
                    <!--begin::Page title-->
                    <div class="page-title me-5">
                        <!--begin::Title-->
                        <h1 class="page-heading d-flex text-white fw-bold fs-2 flex-column justify-content-center my-0">Products
                        <!--begin::Description-->
                        <span class="page-desc text-gray-700 fw-semibold fs-6 pt-3">List Product</span>
                        <!--end::Description--></h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Page title-->
                </div>
                <!--end::Toolbar wrapper=-->
            </div>
            <!--end::Toolbar container=-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Wrapper container-->
    <div class="app-container container-xxl">
        <!--begin::Main-->
        <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
            <!--begin::Content wrapper-->
            <div class="d-flex flex-column flex-column-fluid">
                <!--begin::Content-->
                <div id="kt_app_content" class="app-content flex-column-fluid">
                    <!--begin::Products-->
                    <div class="card card-flush"  style="min-height: 100vh; height: auto"  >
                        <!--begin::Card header-->
                        <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <!--begin::Search-->
                                <div class="d-flex align-items-center position-relative my-1">
                                    <i class="ki-outline ki-magnifier fs-3 position-absolute ms-4"></i>
                                    <input type="text" data-kt-ecommerce-product-filter="search" id="myInput" class="form-control form-control-solid w-250px ps-12" placeholder="Search Product" />
                                </div>
                                <!--end::Search-->
                            </div>
                            <!--end::Card title-->
                            <!--begin::Card toolbar-->
                            <div class="card-toolbar flex-row-fluid justify-content-end gap-5">
                                <!--begin::Add product-->
                                <a href="{{route('backend.product.addproduct')}}" class="btn btn-primary">Add Product</a>
                                <!--end::Add product-->
                            </div>
                            <!--end::Card toolbar-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0">
                            <!--begin::Table-->
                            <table class="table align-middle table-row-dashed fs-6 gy-5" id="produk">
                                <thead>
                                    <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                        <th class=""></th>
                                        <th class="min-w-70px">Product</th>
                                        <th class="min-w-70px">Description</th>
                                        <th class="min-w-70px">SKU</th>
                                        <th class="min-w-70px">Created By</th>
                                        <th class="min-w-70px">Updated By</th>
                                        <th class="min-w-70px">Created At</th>
                                        <th class="min-w-70px">Updated At</th>
                                        <th class="text-end min-w-70px">Actions</th>
                                    </tr>
                                </thead>

                            </table>



                            <!--end::Table-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Products-->
                </div>
                <!--end::Content-->
            </div>
            <!--end::Content wrapper-->
        </div>
        <!--end:::Main-->



        <div id="panel-modal" class="modal fade" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg p-9" role="document">
                <div class="modal-content modal-rounded">
                    <div class="modal-header py-7 d-flex justify-content-between">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
							<i class="ki-outline ki-cross fs-1"></i>
						</div>
                    </div>
                    <div class="modal-body scroll-y m-5">
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('onpage-js')
<script>
  $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function() {

        function format(d) {
            var html = '<table id="example_child" class="table align-middle table-row-dashed fs-6 gy-5" style="width:100%"><thead class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">' +
                        '    <tr>' +
                        '         <th class="min-w-10px"></th>' +
                        '         <th class="min-w-200px">Variant Name</th>' +
                        '         <th class="text-end min-w-70px">Current Price</th>' +
                        '         <th class="text-end min-w-70px">Price</th>' +
                        '         <th class="text-end min-w-70px">QTY</th>' +
                        '         <th class="text-end min-w-70px">Action</th>' +
                        '     </tr>' +
                        '  </thead><tbody>';

            d.mapping_varian_products.forEach((element) => {
                html += '<tr>' +
                    '<td class="text-end pe-0"> </td>' +
                    '<td>' +
                        '    <div class="d-flex align-items-center">' +
                        '        <a href="#" class="symbol symbol-50px">' +
                        '            <span class="symbol-label" style="background-image:url(&#39;{{url("")}}/'+element.url+'&#39;)"></span>' +
                        '        </a>' +
                        '        <div class="ms-5">' +
                            '             <a href="" class="text-gray-800 text-hover-primary fs-5 fw-bold" data-kt-ecommerce-product-filter="product_name">'+element.name +'</a>' +
                            '        </div>' +
                            '    </div>' +
                    '</td>' +

                    '<td class="text-end pe-0">' +
                        element.currrent_price +
                    '</td>' +
                    '<td class="text-end pe-0">' +
                        element.price +
                    '</td>' +
                    '<td class="text-end pe-0">' +
                        element.qty +
                    '</td>' +
                    '<td class="text-end pe-0">' +
                        '<a href="#" class="btn btn-sm btn-light btn-flex btn-center btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions' +
                                '            <i class="ki-outline ki-down fs-5 ms-1"></i></a>' +
                                '           <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true">' +
                                '                 <div class="menu-item px-3">' +
                                '                     <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#panel-modal" onClick="editData('+element.id+')" class="menu-link px-3">Edit</a>' +
                                '                  </div>' +
                                // '                  <div class="menu-item px-3">' +
                                // '                      <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#panel-modal" onClick="setPrice('+element.id+')" class="menu-link px-3">Set Price</a>' +
                                // '                   </div>' +
                                // '                  <div class="menu-item px-3">' +
                                // '                      <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#panel-modal" onClick="setQty('+element.id+')" class="menu-link px-3">Set Qty</a>' +
                                // '                   </div>' +
                                '                  <div class="menu-item px-3">' +
                                '                    <a href="javascript:void(0)" class="menu-link px-3" onClick="deleteVariant('+element.id+')">Delete</a>' +
                                '                </div>' +
                                '           </div>'
                    '</td>' +
                '</tr>';
            });
            html += '</tbody> </table>';
            // `d` is the original data object for the row
            return (
                html
            );
        }


        var table = $('#produk').DataTable({
            ajax: '{{route("backend.promotion.listdatatable")}}',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            destroy: true,
            columns: [
                {
                    className: 'dt-control',
                    orderable: false,
                    data: null,
                    defaultContent: ''
                },
                {
                    "data": "name",
                    "render": function ( data, type, row, meta ) {
                        if (type === 'display') {
                        return '    <div class="d-flex align-items-center">' +
                            '        <a href="#" class="symbol symbol-50px">' +
                            '            <span class="symbol-label" style="background-image:url(&#39;{{url("")}}/'+row.url_thumnail+'&#39;)"></span>' +
                            '        </a>' +
                            '        <div class="ms-5">' +
                            '             <a href="" class="text-gray-800 text-hover-primary fs-5 fw-bold" data-kt-ecommerce-product-filter="product_name">'+data +'</a>' +
                            '        </div>' +
                            '    </div>'
                        } else {
                        return data;
                        }
                    }
                },
                { data: 'description' },
                { data: 'sku' },
                { data: 'created_name' },
                { data: 'updated_name' },
                { data: 'created_at' },
                { data: 'updated_at' },
                {
                    "data": "id",
                    "render": function ( data, type, row, meta ) {
                        if (type === 'display') {
                            var urldetail = '{{route("backend.product.detailproduct", ["id"=> ":id"])}}';
                            urldetail = urldetail.replace(':id', data);
                            var urleditproduct = '{{route("backend.product.editproduct", ["id"=> ":id"])}}';
                            urleditproduct = urleditproduct.replace(':id', data);
                            return '<a href="#" class="btn btn-sm btn-light btn-flex btn-center btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions' +
                                '            <i class="ki-outline ki-down fs-5 ms-1"></i></a>' +
                                '           <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4" data-kt-menu="true">' +
                                '                 <div class="menu-item px-3">' +
                                '                     <a href="'+urldetail+'" class="menu-link px-3">Detail</a>' +
                                '                  </div>' +
                                '                  <div class="menu-item px-3">' +
                                '                      <a href="'+urleditproduct+'" class="menu-link px-3">Edit</a>' +
                                '                   </div>' +
                                '                  <div class="menu-item px-3">' +
                                '                    <a href="#" class="menu-link px-3" onClick="deleteProduct('+data+')">Delete</a>' +
                                '                </div>' +
                                '           </div>'
                        } else {
                            return data;
                        }
                    }
                }
            ],
            order: [[1, 'asc']]
        });

        // Add event listener for opening and closing details
        $('#produk tbody').on('click', 'td.dt-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
            }
            else {
                // Open this row
                row.child(format(row.data())).show();

            }

            // var table_child = $('#example_child').DataTable();

            // table_child.on("draw", function () {
                KTMenu.createInstances();
            // })
        });


        table.on("draw", function () {
            KTMenu.createInstances();
        })


        $('#myInput').on( 'keyup', function () {
            table.search( this.value ).draw();
        } );
	} );


    function deleteProduct(id){
        swal({
            title: 'Delete Product!',
            text: 'Are you sure to delete this product ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ok'
        }).then(function () {
            var datapost={
                "id"  :   id
            };
            $.ajax({
                type: "POST",
                url: "{{route('backend.product.delele')}}",
                data : JSON.stringify(datapost),
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function(response) {
                if (response.success == true) {

                    swal({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        }).then(function () {
                            window.location.href = "{{route('backend.product.index')}}";
                        })



                } else{
                    swal("Failed!", response.message, "error");
                }
                }
            });
        })
    }


    function deleteVariant(id){
        swal({
            title: 'Delete Variant!',
            text: 'Are you sure to delete this Variant ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ok'
        }).then(function () {
            var datapost={
                "id"  :   id
            };
            $.ajax({
                type: "POST",
                url: "{{route('backend.product.delelevariant')}}",
                data : JSON.stringify(datapost),
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function(response) {
                if (response.success == true) {

                    swal({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        }).then(function () {
                            window.location.href = "{{route('backend.product.index')}}";
                        })



                } else{
                    swal("Failed!", response.message, "error");
                }
                }
            });
        })
    }

    function editData(id){
        var url = "{{route('backend.product.modaleditvariant',['id'=>':id']) }}"
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Edit Variant');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

    function setPrice(id){
        var url = "{{route('backend.product.modaleditvariant',['id'=>':id']) }}"
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Set Price');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

    function setQty(id){
        var url = "{{route('backend.product.modaleditvariant',['id'=>':id']) }}"
        url = url.replace(':id', id);
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load(url);
        $('#panel-modal  .modal-title').html('Set Qty');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }

</script>

@endsection
