@extends('backend.layouts.backend')

@section('content')
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex align-items-start">
            <!--begin::Toolbar container-->
            <div class="d-flex flex-column flex-row-fluid">
                <!--begin::Toolbar wrapper-->
                <div class="d-flex align-items-center pt-1">
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">
                            <a href="../../demo34/dist/index.html" class="text-white text-hover-primary">
                                <i class="ki-outline ki-home text-gray-700 fs-6"></i>
                            </a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">Catalog</li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">Product</li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">Add Product</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Toolbar wrapper=-->
                <!--begin::Toolbar wrapper=-->
                <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-13 pb-6">
                    <!--begin::Page title-->
                    <div class="page-title me-5">
                        <!--begin::Title-->
                        <h1 class="page-heading d-flex text-white fw-bold fs-2 flex-column justify-content-center my-0">Product Form
                        <!--begin::Description-->
                        <span class="page-desc text-gray-700 fw-semibold fs-6 pt-3">Create Product</span>
                        <!--end::Description--></h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Page title-->
                </div>
                <!--end::Toolbar wrapper=-->
            </div>
            <!--end::Toolbar container=-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Wrapper container-->
    <div class="app-container container-xxl">
        <!--begin::Main-->
        <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
            <!--begin::Content wrapper-->
            <div class="d-flex flex-column flex-column-fluid">
                <!--begin::Content-->
                <div id="kt_app_content" class="app-content flex-column-fluid">
                    <!--begin::Card-->
                    <div class="card">
                        <!--begin::Card body-->
                        <div class="card-body">
                            <!--begin::Stepper-->
                            <div class="stepper stepper-links d-flex flex-column pt-15" id="kt_create_account_stepper">
                                <!--begin::Nav-->
                                <div class="stepper-nav mb-5">
                                    <!--begin::Step 1-->
                                    <div class="stepper-item current" data-kt-stepper-element="nav">
                                        <h3 class="stepper-title">Product</h3>
                                    </div>
                                    <!--end::Step 1-->
                                    <!--begin::Step 3-->
                                    <div class="stepper-item" data-kt-stepper-element="nav">
                                        <h3 class="stepper-title">Varian & Price</h3>
                                    </div>
                                    <!--end::Step 3-->
                                </div>
                                <!--end::Nav-->
                                <!--begin::Form-->
                                <form class="mx-auto mw-900px w-100 pt-15 pb-10" action="{{route('backend.product.save')}}" enctype="multipart/form-data" method="POST" id="kt_create_account_form">
                                    <!--begin::Step 1-->
                                    <div class="current" data-kt-stepper-element="content">
                                        <!--begin::Wrapper-->
                                        <div class="w-100">

                                            <!--begin::Input group-->
                                            <div class="fv-row">
                                                <!--begin::Row-->
                                                <div class="row">

                                                    @csrf

                                                    @if($errors->any())
                                                        @foreach($errors->all() as $error)
                                                        {!! $error !!}
                                                        @endforeach
                                                    @endif
                                                        <!--begin::Input group-->
                                                        <div class="pb-10 pb-lg-15">
                                                            <!--begin::Title-->
                                                            <h2 class="fw-bold d-flex align-items-center text-dark">Choose Product Type
                                                            <span class="ms-1" data-bs-toggle="tooltip" title="Billing is issued based on your selected account typ">
                                                                <i class="ki-outline ki-information-5 text-gray-500 fs-6"></i>
                                                            </span></h2>
                                                            <!--end::Title-->
                                                            <!--begin::Notice-->
                                                            <div class="text-muted fw-semibold fs-6">Select product type.</div>
                                                            <!--end::Notice-->
                                                        </div>

															<!--begin::Input group-->
															<div class="fv-row">
																<!--begin::Row-->
																<div class="row">
																	<!--begin::Col-->
																	<div class="col-lg-4">
																		<!--begin::Option-->
																		<input type="radio" class="btn-check" name="account_type" value="1" checked="checked" id="kt_create_account_form_account_type_personal"  onclick="showPo(1,false)" />
																		<label class="btn btn-outline btn-outline-dashed btn-active-light-primary p-7 d-flex align-items-center mb-10" for="kt_create_account_form_account_type_personal">
																			<i class="la la-outline la-glasses fs-3x me-5"></i>
																			<!--begin::Info-->
																			<span class="d-block fw-semibold text-start">
																				<span class="text-dark fw-bold d-block fs-4 mb-2">Frame</span>
																			</span>
																			<!--end::Info-->
																		</label>
																		<!--end::Option-->
																	</div>
																	<!--end::Col-->
																	<!--begin::Col-->
																	<div class="col-lg-4">
																		<!--begin::Option-->
																		<input type="radio" class="btn-check" name="account_type" value="2" id="kt_create_account_form_account_type_corporate" onclick="showPo(2, true)" />
																		<label class="btn btn-outline btn-outline-dashed btn-active-light-primary p-7 d-flex align-items-center" for="kt_create_account_form_account_type_corporate">
																			<i class="la la-outline la-circle fs-3x me-5"></i>
																			<!--begin::Info-->
																			<span class="d-block fw-semibold text-start">
																				<span class="text-dark fw-bold d-block fs-4 mb-2">Lens</span>
																			</span>
																			<!--end::Info-->
																		</label>
																		<!--end::Option-->
																	</div>
																	<!--end::Col-->
																	<!--begin::Col-->
																	<div class="col-lg-4">
																		<!--begin::Option-->
																		<input type="radio" class="btn-check" name="account_type" value="3" id="kt_create_account_form_account_type_accesories" onclick="showPo(3, false)" />
																		<label class="btn btn-outline btn-outline-dashed btn-active-light-primary p-7 d-flex align-items-center" for="kt_create_account_form_account_type_accesories">
																			<i class="la la-outline la-circle fs-3x me-5"></i>
																			<!--begin::Info-->
																			<span class="d-block fw-semibold text-start">
																				<span class="text-dark fw-bold d-block fs-4 mb-2">Accessories</span>
																			</span>
																			<!--end::Info-->
																		</label>
																		<!--end::Option-->
																	</div>
																	<!--end::Col-->
																</div>
																<!--end::Row-->
															</div>
															<!--end::Input group-->
                                                        <div class="mb-10 fv-row">
                                                            <!--begin::Label-->
                                                            <label class="required form-label">Product Name</label>
                                                            <!--end::Label-->
                                                            <!--begin::Input-->
                                                            <input type="text" name="product_name" id="product_name" class="form-control mb-2" placeholder="Product name"  value="{{ old('product_name') }}" required />
                                                            <!--end::Input-->
                                                            <!--begin::Description-->
                                                            <div class="text-muted fs-7">A product name is required and recommended to be unique.</div>
                                                            <!--end::Description-->
                                                        </div>
                                                        <!--begin::Input group-->
                                                        <div class="mb-10 fv-row">
                                                            <!--begin::Label-->
                                                            <label class="required form-label">Product Description</label>
                                                            <!--end::Label-->
                                                            <!--begin::Input-->
                                                            <textarea class="form-control mb-2" id="product_description" name="product_description"  required> {{ old('product_description') }}</textarea>
                                                            <!--end::Input-->
                                                        </div>
                                                        <!--end::Input group-->
                                                        <!--begin::Input group-->
                                                        <div class="mb-10 fv-row">
                                                            <!--begin::Label-->
                                                            <label class="required form-label">SKU</label>
                                                            <!--end::Label-->
                                                            <!--begin::Input-->
                                                            <input type="text" name="sku" id="sku" class="form-control mb-2" placeholder="SKU Number"  value="{{ old('sku') }}" required />
                                                            <!--end::Input-->
                                                            <!--begin::Description-->
                                                            <div class="text-muted fs-7">Enter the product SKU.</div>
                                                            <!--end::Description-->
                                                        </div>
                                                        <!--end::Input group-->
                                                        <!--begin::Input group-->
                                                        <!--begin::Input group-->
                                                        <div class="mb-10 fv-row">
                                                            <label class="form-label">Product Type</label>
                                                            <!--end::Label-->
                                                            <!--begin::Select2-->

                                                            <!--end::Select2-->
                                                            <!--begin::Description-->
                                                            <span id='pti'></span>
                                                            <div class="text-muted fs-7 mb-7">Add product to a category.</div>
                                                        </div>

                                                        <!--end::Input group-->

                                                        <!--begin::Card body-->
                                                        <div class="card-body text-center pt-0">
                                                            <!--begin::Image input-->
                                                            <!--begin::Image input placeholder-->
                                                            <style>.image-input-placeholder { background-image: url('assets/media/svg/files/blank-image.svg'); } [data-bs-theme="dark"] .image-input-placeholder { background-image: url('assets/media/svg/files/blank-image-dark.svg'); }</style>
                                                            <!--end::Image input placeholder-->
                                                            <div class="image-input image-input-empty image-input-outline image-input-placeholder mb-3" data-kt-image-input="true">
                                                                <!--begin::Preview existing avatar-->
                                                                <div class="image-input-wrapper w-150px h-150px"></div>
                                                                <!--end::Preview existing avatar-->
                                                                <!--begin::Label-->
                                                                <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                                                    <i class="ki-outline ki-pencil fs-7"></i>
                                                                    <!--begin::Inputs-->
                                                                    <input type="file" id="avatar" name="avatar" required accept=".png, .jpg, .jpeg" />
                                                                    <input type="hidden" name="avatar_remove" />
                                                                    <!--end::Inputs-->
                                                                </label>
                                                                <!--end::Label-->
                                                                <!--begin::Cancel-->
                                                                <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                                                                    <i class="ki-outline ki-cross fs-2"></i>
                                                                </span>
                                                                <!--end::Cancel-->
                                                                <!--begin::Remove-->
                                                                <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                                                                    <i class="ki-outline ki-cross fs-2"></i>
                                                                </span>
                                                                <!--end::Remove-->
                                                            </div>
                                                            <!--end::Image input-->
                                                            <!--begin::Description-->
                                                            <div class="text-muted fs-7">Set the product thumbnail image. Only *.png, *.jpg and *.jpeg image files are accepted</div>
                                                            <!--end::Description-->
                                                        </div>

                                                        <div class="fv-row mb-2  col-12">
                                                            <!--begin::Dropzone-->
                                                            <div class="dropzone" id="kt_ecommerce_add_product_media">
                                                                <!--begin::Message-->
                                                                <div class="dz-message needsclick">
                                                                    <!--begin::Icon-->
                                                                    <i class="ki-outline ki-file-up text-primary fs-3x"></i>
                                                                    <!--end::Icon-->
                                                                    <!--begin::Info-->
                                                                    <div class="ms-4">
                                                                        <h3 class="fs-5 fw-bold text-gray-900 mb-1">Click to upload.</h3>

                                                                        <input type="file" id="image[]" name="image[]" accept=".png, .jpg, .jpeg" multiple  />
                                                                        <span class="fs-7 fw-semibold text-gray-400">Upload up to 10 files</span>

                                                                    </div>
                                                                    <!--end::Info-->
                                                                </div>
                                                            </div>
                                                            <!--end::Dropzone-->
                                                        </div>
                                                        <!--end::Input group-->
                                                        <!--begin::Description-->
                                                        <div class="text-muted fs-7">Set the product media gallery.</div>
                                                        <!--end::Description-->


                                                        <div id="preordershow" style="display: none">
                                                            <div class="d-flex flex-stack " >
                                                                <!--begin::Label-->
                                                                <div class="me-5">
                                                                    <label class="fs-6 fw-semibold form-label">Make Pre Order Product?</label>
                                                                    <div class="fs-7 fw-semibold text-muted">Product will be place to pre order on 3-7 days</div>
                                                                </div>
                                                                <!--end::Label-->
                                                                <!--begin::Switch-->
                                                                <label class="form-check form-switch form-check-custom form-check-solid">
                                                                    <input class="form-check-input"  id="preorder" name="preorder"  type="checkbox" value="1" checked="checked">
                                                                    <span class="form-check-label fw-semibold text-muted">Pre Order</span>
                                                                </label>
                                                                <!--end::Switch-->
                                                            </div>
                                                        </div>
                                                </div>
                                                <!--end::Row-->
                                            </div>
                                            <!--end::Input group-->
                                        </div>
                                        <!--end::Wrapper-->
                                    </div>
                                    <!--end::Step 1-->
                                    <!--begin::Actions-->
                                    <div class="d-flex flex-stack pt-15">
                                        <!--begin::Wrapper-->
                                        <div>
                                            <button type="submit" class="btn btn-lg btn-primary">Continue
                                            <i class="ki-outline ki-arrow-right fs-4 ms-1 me-0"></i></button>
                                        </div>
                                        <!--end::Wrapper-->
                                    </div>
                                    <!--end::Actions-->
                                </form>
                                <!--end::Form-->
                            </div>
                            <!--end::Stepper-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Card-->
                </div>
                <!--end::Content-->
            </div>
            <!--end::Content wrapper-->
        </div>
        <!--end:::Main-->

@endsection
@section('onpage-js')
<script type="text/javascript">
    $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
   });

   $(document).ready(function() {
        document.getElementById("preordershow").style.display = 'none';
        @if($errors->any())
           @foreach($errors->all() as $error)
               testNotif('Failed Message','{!! $error !!}','danger', 'la la-close')
           @endforeach

        @elseif(session()->get('berhasil'))
           @if(is_array(json_decode(session()->get('berhasil'), true)))
               testNotif('Success Message','{!! implode('', session()->get("berhasil")->all(":message<br/>")) !!}','primary', 'la la-check')

           @else
               testNotif('Success Message','{!! session()->get("berhasil") !!}','primary', 'la la-check')
           @endif
        @endif

        var datapost={
            "id"  :   1
        };
        $.ajax({
            type: "POST",
            url: "{{route('backend.product.setproducttype')}}",
            data : JSON.stringify(datapost),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function(response) {
            if (response.success == true) {
                 var isi = ''
                response.data.forEach((score) => {

                    isi += '<option value="'+score.id+'">'+score.name+'</option>'

                    });
                var html = '<select class="form-select mb-2" id="product_type_id" name="product_type_id" data-control="select2" required data-placeholder="Select an option">'
                    +isi
                    +'</select>'
                    $('#pti').append(html);
            } else{
                swal("Failed!", response.message, "error");
            }
            }
        });
   } );


   function showPo(id, status){
        if(status == true){

            document.getElementById("preordershow").style.display = 'block';
        }else{

            document.getElementById("preordershow").style.display = 'none';
        }
        $('#pti').empty();
        var datapost={
            "id"  :   id
        };
        $.ajax({
            type: "POST",
            url: "{{route('backend.product.setproducttype')}}",
            data : JSON.stringify(datapost),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function(response) {
            if (response.success == true) {
                 var isi = ''
                response.data.forEach((score) => {

                    isi += '<option value="'+score.id+'">'+score.name+'</option>'

                    });
                var html = '<select class="form-select mb-2" id="product_type_id" name="product_type_id" data-control="select2" required data-placeholder="Select an option">'
                    +isi
                    +'</select>'
                    $('#pti').append(html);
            } else{
                swal("Failed!", response.message, "error");
            }
            }
        });
   }
   function testNotif(title, message, type, icon){
       var content = {};
       content.message = message;
       content.title = title;
       content.icon = 'icon ' + icon;

       var notify = $.notify(content, {
           type: type,
           allow_dismiss: true,
           newest_on_top: true,
           mouse_over:  true,
           showProgressbar:  false,
           spacing: 10,
           timer: 2000,
           placement: {
               from: 'top',
               align: 'right'
           },
           offset: {
               x: 30,
               y: 30
           },
           delay: 1000,
           z_index: 10000,
           animate: {
               enter: 'animated tada' ,
               exit: 'animated bounceOut'
           }
       });


   }


</script>
@endsection
