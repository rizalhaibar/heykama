@extends('backend.layouts.backend')

@section('content')

    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex align-items-start">
            <!--begin::Toolbar container-->
            <div class="d-flex flex-column flex-row-fluid">
                <!--begin::Toolbar wrapper-->
                <div class="d-flex align-items-center pt-1">
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">
                            <a href="../../demo34/dist/index.html" class="text-white text-hover-primary">
                                <i class="ki-outline ki-home text-gray-700 fs-6"></i>
                            </a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">Setting</li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-white fw-bold lh-1">About</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Toolbar wrapper=-->
                <!--begin::Toolbar wrapper=-->
                <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-13 pb-6">
                    <!--begin::Page title-->
                    <div class="page-title me-5">
                        <!--begin::Title-->
                        <h1 class="page-heading d-flex text-white fw-bold fs-2 flex-column justify-content-center my-0">About
                        <!--begin::Description-->
                        <span class="page-desc text-gray-700 fw-semibold fs-6 pt-3">Detail About</span>
                        <!--end::Description--></h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Page title-->
                </div>
                <!--end::Toolbar wrapper=-->
            </div>
            <!--end::Toolbar container=-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Wrapper container-->
    <div class="app-container container-xxl">
        <!--begin::Main-->
        <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
            <!--begin::Content wrapper-->
            <div class="d-flex flex-column flex-column-fluid">
                <!--begin::Content-->
                <div id="kt_app_content" class="app-content flex-column-fluid">
                    <!--begin::Products-->
                    <div class="card card-flush">
                        <div class="card-header align-items-center py-5 gap-2 gap-md-5">

                            <!--begin::Card toolbar-->
                            <div class="card-toolbar flex-row-fluid justify-content-end gap-5">

                            </div>
                            <!--end::Card toolbar-->
                        </div>
                        <!--begin::Card body-->
                        <div class="card-body pt-0">
                            <!--begin::Card body-->
                            <div class="card-body text-center pt-0">
                                <!--begin::Image input-->
                                <!--begin::Image input placeholder-->
                                <style>.image-input-placeholder { background-image: url('assets/media/svg/files/blank-image.svg'); } [data-bs-theme="dark"] .image-input-placeholder { background-image: url('assets/media/svg/files/blank-image-dark.svg'); }</style>
                                <!--end::Image input placeholder-->
                                <div class="image-input image-input-empty image-input-outline image-input-placeholder mb-3" data-kt-image-input="true">
                                    <!--begin::Preview existing avatar-->
                                    <div class="image-input-wrapper w-150px h-150px"></div>
                                    <!--end::Preview existing avatar-->
                                    <!--begin::Label-->
                                    <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change banner">
                                        <i class="ki-outline ki-pencil fs-7"></i>
                                        <!--begin::Inputs-->
                                        <input type="file" id="avatar" name="avatar" required accept=".png, .jpg, .jpeg" />
                                        <input type="hidden" name="avatar_remove" />
                                        <!--end::Inputs-->
                                    </label>
                                    <!--end::Label-->
                                    <!--begin::Cancel-->
                                    <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel banner">
                                        <i class="ki-outline ki-cross fs-2"></i>
                                    </span>
                                    <!--end::Cancel-->
                                    <!--begin::Remove-->
                                    <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove banner">
                                        <i class="ki-outline ki-cross fs-2"></i>
                                    </span>
                                    <!--end::Remove-->
                                </div>
                                <!--end::Image input-->
                                <!--begin::Description-->
                                <div class="text-muted fs-7">Set the banner thumbnail image. Only *.png, *.jpg and *.jpeg image files are accepted</div>
                                <!--end::Description-->
                            </div>
                            <!--begin::Input group-->
                            <div class="mb-10 fv-row">
                                <!--begin::Label-->
                                <label class="required form-label mb-3">Title</label>
                                <!--end::Label-->
                                <input type="text" class="form-control form-control-lg " id="title" name="title" placeholder="Title" value="" />
                            </div>
                            <div class="mb-10 fv-row">
                                <!--begin::Label-->
                                <label class="required form-label mb-3">Tagline</label>
                                <!--end::Label-->
                                    <textarea class="form-control" id="tagline" name="tagline"  data-kt-autosize="true"></textarea>
                            </div>
                            <div class="mb-10 fv-row">
                                <!--begin::Label-->
                                <label class="required form-label mb-3">Body</label>
                                <!--end::Label-->
                                <div class="tinymce">
                                    <textarea class="form-control" id="body" name="body"  data-kt-autosize="true"></textarea>
                                </div>
                            </div>

                            <div class=" d-flex justify-content-end py-6">
                                <button type="reset" class="btn btn-light  me-2" style="border: 1px solid #cfc7c7">Discard</button>

                                <button type="button" onClick="createData()" class="btn btn-primary">Save</button>
                            </div>

                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Products-->
                </div>
                <!--end::Content-->
            </div>
            <!--end::Content wrapper-->
        </div>
        <!--end:::Main-->


@endsection

@section('onpage-js')
<script src="https://cdn.tiny.cloud/1/Add-your-API-key/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
var KTTinymce = function () {
    // Private functions
    var demos = function () {
        tinymce.init({
            selector: '#body',
            menubar: false,
            toolbar: ['styleselect fontselect fontsizeselect',
                'undo redo | cut copy paste | bold italic | link image | alignleft aligncenter alignright alignjustify',
                'bullist numlist | outdent indent | blockquote subscript superscript | advlist | autolink | lists charmap | print preview |  code'],
            plugins : 'advlist autolink link image lists charmap print preview code'
        });
    }

    return {
        // public functions
        init: function() {
            demos();
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    KTTinymce.init();
});

function editData(id){
    // var url = "{{route('heykama.editviewcategory',['id'=>':id']) }}'
    url = url.replace(':id', id);
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .modal-body').load(url);
    $('#panel-modal  .modal-title').html('Edit Category');
    $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function viewData(id){
    // var url = "{{route('heykama.editviewcategory',['id'=>':id']) }}'
    url = url.replace(':id', id);
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .modal-body').load(url);
    $('#panel-modal  .modal-title').html('Edit Category');
    $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function deleteData(id){
    // var url = "{{route('heykama.editviewcategory',['id'=>':id']) }}'
    url = url.replace(':id', id);
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .modal-body').load(url);
    $('#panel-modal  .modal-title').html('Edit Category');
    $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function createData(){
       var title   = $('#title').val();
       var body   = tinyMCE.get('body').getContent();
       var status   = 'PUBLISH';

              if(title){
                  var datapost={
                      "title"  :   title,
                      "body"  :   body,
                      "status"  :   status,
                    };
                    $.ajax({
                      type: "POST",
                      url: "{{ route('backend.journal.create') }}",
                      data : JSON.stringify(datapost),
                      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                      dataType: 'json',
                      contentType: 'application/json; charset=utf-8',
                      success: function(response) {
                        if (response.success == true) {

                            swal({
                                  title: 'Success!',
                                  text: response.message,
                                  type: 'success',
                                  showCancelButton: false,
                                  confirmButtonText: 'Ok'
                              }).then(function () {
                                 window.location.href = "{{ route('backend.journal.index') }}";
                              })



                        } else{
                          swal("Failed!", response.message, "error");
                        }
                      }
                    });
              }else{
                  swal("Failed!", "Nama Belum Terisi!", "error");
              }

  }

</script>

@endsection
