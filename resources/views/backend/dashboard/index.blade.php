@extends('backend.layouts.backend')

@section('content')
					<!--begin::Toolbar-->
					<div id="kt_app_toolbar" class="app-toolbar py-6">
						<!--begin::Toolbar container-->
						<div id="kt_app_toolbar_container" class="app-container container-xxl d-flex align-items-start">
							<!--begin::Toolbar container-->
							<div class="d-flex flex-column flex-row-fluid">
								<!--begin::Toolbar wrapper-->
								<div class="d-flex align-items-center pt-1">
									<!--begin::Breadcrumb-->
									<ul class="breadcrumb breadcrumb-separatorless fw-semibold">
										<!--begin::Item-->
										<li class="breadcrumb-item text-white fw-bold lh-1">
											<a href="{{ route('heykama.dashboard') }}" class="text-white text-hover-primary">
												<i class="ki-outline ki-home text-gray-700 fs-6"></i>
											</a>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item">
											<i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
										</li>
										<!--end::Item-->
										<!--begin::Item-->
										<li class="breadcrumb-item text-white fw-bold lh-1">Dashboards</li>
										<!--end::Item-->
									</ul>
									<!--end::Breadcrumb-->
								</div>
								<!--end::Toolbar wrapper=-->
								<!--begin::Toolbar wrapper=-->
								<div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-13 pb-6">
									<!--begin::Page title-->
									<div class="page-title me-5">
										<!--begin::Title-->
										<h1 class="page-heading d-flex text-white fw-bold fs-2 flex-column justify-content-center my-0">Welcome back, {{ Auth::user()->name }}
										<!--begin::Description-->
										<span class="page-desc text-gray-700 fw-semibold fs-6 pt-3"></span>
										<!--end::Description--></h1>
										<!--end::Title-->
									</div>
									<!--end::Page title-->
								</div>
								<!--end::Toolbar wrapper=-->
							</div>
							<!--end::Toolbar container=-->
						</div>
						<!--end::Toolbar container-->
					</div>
					<!--end::Toolbar-->
					<!--begin::Wrapper container-->
					<div class="app-container container-xxl">
						<!--begin::Main-->
						<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
							<!--begin::Content wrapper-->
							<div class="d-flex flex-column flex-column-fluid">
								<!--begin::Content-->
								<div id="kt_app_content" class="app-content flex-column-fluid">
									<!--begin::Row-->
									<div class="row g-5 g-xl-8">
										<!--begin::Col-->

                                        <div class="col-xl-12 mb-5 mb-xl-10">

                                            <div id="kt_carousel_2_carousel" class="carousel carousel-custom slide" data-bs-ride="carousel" data-bs-interval="4000">
                                                <!--begin::Heading-->
                                                <div class="d-flex align-items-center justify-content-between flex-wrap">
                                                    <!--begin::Label-->
                                                    <span class="fs-4 fw-bold pe-2">Title</span>
                                                    <!--end::Label-->

                                                    <!--begin::Carousel Indicators-->
                                                    <ol class="p-0 m-0 carousel-indicators carousel-indicators-bullet">
                                                        <li data-bs-target="#kt_carousel_2_carousel" data-bs-slide-to="0" class="ms-1 active"></li>
                                                        <li data-bs-target="#kt_carousel_2_carousel" data-bs-slide-to="1" class="ms-1"></li>
                                                    </ol>
                                                    <!--end::Carousel Indicators-->
                                                </div>
                                                <!--end::Heading-->

                                                <!--begin::Carousel-->
                                                <div class="carousel-inner pt-8">
                                                    <!--begin::Item-->
                                                    <div class="carousel-item active">
                                                        <div class="row">
                                                            <div class="col-xl-3 mb-5 mb-xl-10">

                                                                <div class="card overflow-hidden">
                                                                    <!--begin::Card body-->
                                                                    <div class="card-body d-flex justify-content-between flex-column px-0 pb-0">
                                                                        <!--begin::Statistics-->
                                                                        <div class="mb-4 px-9">
                                                                            <!--begin::Info-->
                                                                            <div class="d-flex align-items-center mb-2">
                                                                                <!--begin::Value-->
                                                                                <span class="fs-2hx fw-bold text-gray-800 me-2 lh-1 ls-n2">47,769,700</span>
                                                                                <!--end::Value-->
                                                                                <!--begin::Label-->
                                                                                <span class="d-flex align-items-end text-gray-400 fs-6 fw-semibold">Rupiah</span>
                                                                                <!--end::Label-->
                                                                            </div>
                                                                            <!--end::Info-->
                                                                            <!--begin::Description-->
                                                                            <span class="fs-6 fw-semibold text-gray-400">Total Online Sales</span>
                                                                            <!--end::Description-->
                                                                        </div>
                                                                        <!--end::Statistics-->
                                                                        <!--begin::Chart-->
                                                                        <div id="kt_card_widget_12_chart" class="min-h-auto" style="height: 125px"></div>
                                                                        <!--end::Chart-->
                                                                    </div>
                                                                    <!--end::Card body-->
                                                                </div>
                                                            </div>

                                                            <div class="col-xl-3 mb-md-5 mb-xl-10">

                                                                <!--end::Chart widget 6-->
                                                                <div class="card overflow-hidden ">
                                                                    <!--begin::Card body-->
                                                                    <div class="card-body d-flex justify-content-between flex-column px-0 pb-0">
                                                                        <!--begin::Statistics-->
                                                                        <div class="mb-4 px-9">
                                                                            <!--begin::Statistics-->
                                                                            <div class="d-flex align-items-center mb-2">
                                                                                <!--begin::Value-->
                                                                                <span class="fs-2hx fw-bold text-gray-800 me-2 lh-1 ls-n2">259,786</span>
                                                                                <!--end::Value-->
                                                                                <!--begin::Label-->
                                                                                <!--end::Label-->
                                                                            </div>
                                                                            <!--end::Statistics-->
                                                                            <!--begin::Description-->
                                                                            <span class="fs-6 fw-semibold text-gray-400">Total Stock Out</span>
                                                                            <!--end::Description-->
                                                                        </div>
                                                                        <!--end::Statistics-->
                                                                        <!--begin::Chart-->
                                                                        <div id="kt_card_widget_13_chart" class="min-h-auto" style="height: 125px"></div>
                                                                        <!--end::Chart-->
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="col-xl-3 mb-md-5 mb-xl-10">

                                                                <!--end::Chart widget 6-->
                                                                <div class="card overflow-hidden ">
                                                                    <!--begin::Card body-->
                                                                    <div class="card-body d-flex justify-content-between flex-column px-0 pb-0">
                                                                        <!--begin::Statistics-->
                                                                        <div class="mb-4 px-9">
                                                                            <!--begin::Statistics-->
                                                                            <div class="d-flex align-items-center mb-2">
                                                                                <!--begin::Value-->
                                                                                <span class="fs-2hx fw-bold text-gray-800 me-2 lh-1 ls-n2">259,786</span>
                                                                                <!--end::Value-->
                                                                                <!--begin::Label-->
                                                                                <!--end::Label-->
                                                                            </div>
                                                                            <!--end::Statistics-->
                                                                            <!--begin::Description-->
                                                                            <span class="fs-6 fw-semibold text-gray-400">Total Customers</span>
                                                                            <!--end::Description-->
                                                                        </div>
                                                                        <!--end::Statistics-->
                                                                        <!--begin::Chart-->
                                                                        <div id="kt_card_widget_13_chart" class="min-h-auto" style="height: 125px"></div>
                                                                        <!--end::Chart-->
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="col-xl-3 mb-md-5 mb-xl-10">

                                                                <!--end::Chart widget 6-->
                                                                <div class="card overflow-hidden ">
                                                                    <!--begin::Card body-->
                                                                    <div class="card-body d-flex justify-content-between flex-column px-0 pb-0">
                                                                        <!--begin::Statistics-->
                                                                        <div class="mb-4 px-9">
                                                                            <!--begin::Statistics-->
                                                                            <div class="d-flex align-items-center mb-2">
                                                                                <!--begin::Value-->
                                                                                <span class="fs-2hx fw-bold text-gray-800 me-2 lh-1 ls-n2">259,786</span>
                                                                                <!--end::Value-->
                                                                                <!--begin::Label-->
                                                                                <!--end::Label-->
                                                                            </div>
                                                                            <!--end::Statistics-->
                                                                            <!--begin::Description-->
                                                                            <span class="fs-6 fw-semibold text-gray-400">Cancel Order(Item)</span>
                                                                            <!--end::Description-->
                                                                        </div>
                                                                        <!--end::Statistics-->
                                                                        <!--begin::Chart-->
                                                                        <div id="kt_card_widget_13_chart" class="min-h-auto" style="height: 125px"></div>
                                                                        <!--end::Chart-->
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end::Item-->

                                                    <!--begin::Item-->
                                                    <div class="carousel-item">
                                                        <div class="row">
                                                            <div class="col-xl-3 mb-md-5 mb-xl-10">

                                                                <!--end::Chart widget 6-->
                                                                <div class="card overflow-hidden ">
                                                                    <!--begin::Card body-->
                                                                    <div class="card-body d-flex justify-content-between flex-column px-0 pb-0">
                                                                        <!--begin::Statistics-->
                                                                        <div class="mb-4 px-9">
                                                                            <!--begin::Statistics-->
                                                                            <div class="d-flex align-items-center mb-2">
                                                                                <!--begin::Value-->
                                                                                <span class="fs-2hx fw-bold text-gray-800 me-2 lh-1 ls-n2">Rp. 259,786</span>
                                                                                <!--end::Value-->
                                                                                <!--begin::Label-->
                                                                                <!--end::Label-->
                                                                            </div>
                                                                            <!--end::Statistics-->
                                                                            <!--begin::Description-->
                                                                            <span class="fs-6 fw-semibold text-gray-400">Cancel Order(Rp)</span>
                                                                            <!--end::Description-->
                                                                        </div>
                                                                        <!--end::Statistics-->
                                                                        <!--begin::Chart-->
                                                                        <div id="kt_card_widget_13_chart" class="min-h-auto" style="height: 125px"></div>
                                                                        <!--end::Chart-->
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="col-xl-3 mb-md-5 mb-xl-10">

                                                                <!--end::Chart widget 6-->
                                                                <div class="card overflow-hidden ">
                                                                    <!--begin::Card body-->
                                                                    <div class="card-body d-flex justify-content-between flex-column px-0 pb-0">
                                                                        <!--begin::Statistics-->
                                                                        <div class="mb-4 px-9">
                                                                            <!--begin::Statistics-->
                                                                            <div class="d-flex align-items-center mb-2">
                                                                                <!--begin::Value-->
                                                                                <span class="fs-2hx fw-bold text-gray-800 me-2 lh-1 ls-n2">Rp. 259,786</span>
                                                                                <!--end::Value-->
                                                                                <!--begin::Label-->
                                                                                <!--end::Label-->
                                                                            </div>
                                                                            <!--end::Statistics-->
                                                                            <!--begin::Description-->
                                                                            <span class="fs-6 fw-semibold text-gray-400">Total add to cart(Rp)</span>
                                                                            <!--end::Description-->
                                                                        </div>
                                                                        <!--end::Statistics-->
                                                                        <!--begin::Chart-->
                                                                        <div id="kt_card_widget_13_chart" class="min-h-auto" style="height: 125px"></div>
                                                                        <!--end::Chart-->
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="col-xl-3 mb-md-5 mb-xl-10">

                                                                <!--end::Chart widget 6-->
                                                                <div class="card overflow-hidden ">
                                                                    <!--begin::Card body-->
                                                                    <div class="card-body d-flex justify-content-between flex-column px-0 pb-0">
                                                                        <!--begin::Statistics-->
                                                                        <div class="mb-4 px-9">
                                                                            <!--begin::Statistics-->
                                                                            <div class="d-flex align-items-center mb-2">
                                                                                <!--begin::Value-->
                                                                                <span class="fs-2hx fw-bold text-gray-800 me-2 lh-1 ls-n2">259,786</span>
                                                                                <!--end::Value-->
                                                                                <!--begin::Label-->
                                                                                <!--end::Label-->
                                                                            </div>
                                                                            <!--end::Statistics-->
                                                                            <!--begin::Description-->
                                                                            <span class="fs-6 fw-semibold text-gray-400">Total add to cart(Item)</span>
                                                                            <!--end::Description-->
                                                                        </div>
                                                                        <!--end::Statistics-->
                                                                        <!--begin::Chart-->
                                                                        <div id="kt_card_widget_13_chart" class="min-h-auto" style="height: 125px"></div>
                                                                        <!--end::Chart-->
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="col-xl-3 mb-md-5 mb-xl-10">

                                                                <!--end::Chart widget 6-->
                                                                <div class="card overflow-hidden ">
                                                                    <!--begin::Card body-->
                                                                    <div class="card-body d-flex justify-content-between flex-column px-0 pb-0">
                                                                        <!--begin::Statistics-->
                                                                        <div class="mb-4 px-9">
                                                                            <!--begin::Statistics-->
                                                                            <div class="d-flex align-items-center mb-2">
                                                                                <!--begin::Value-->
                                                                                <span class="fs-2hx fw-bold text-gray-800 me-2 lh-1 ls-n2">259,786,000</span>
                                                                                <!--end::Value-->
                                                                                <!--begin::Label-->
                                                                                <!--end::Label-->
                                                                            </div>
                                                                            <!--end::Statistics-->
                                                                            <!--begin::Description-->
                                                                            <span class="fs-6 fw-semibold text-gray-400">Total Click Product</span>
                                                                            <!--end::Description-->
                                                                        </div>
                                                                        <!--end::Statistics-->
                                                                        <!--begin::Chart-->
                                                                        <div id="kt_card_widget_13_chart" class="min-h-auto" style="height: 125px"></div>
                                                                        <!--end::Chart-->
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end::Item-->

                                                </div>
                                                <!--end::Carousel-->
                                            </div>
                                        </div>





										<div class="col-xl-12">
                                            <div class="card card-flush overflow-hidden h-lg-100">
												<!--begin::Header-->
												<div class="card-header pt-5">
													<!--begin::Title-->
													<h3 class="card-title align-items-start flex-column">
														<span class="card-label fw-bold text-dark">Tren</span>
														<span class="text-gray-400 mt-1 fw-semibold fs-6">1,046 Inbound Calls today</span>
													</h3>
													<!--end::Title-->
													<!--begin::Toolbar-->
													<div class="card-toolbar">
														<!--begin::Daterangepicker(defined in src/js/layout/app.js)-->
														<div data-kt-daterangepicker="true" data-kt-daterangepicker-opens="left" data-kt-daterangepicker-range="today" class="btn btn-sm btn-light d-flex align-items-center px-4">
															<!--begin::Display range-->
															<div class="text-gray-600 fw-bold">Loading date range...</div>
															<!--end::Display range-->
															<i class="ki-outline ki-calendar-8 fs-1 ms-2 me-0"></i>
														</div>
														<!--end::Daterangepicker-->
													</div>
													<!--end::Toolbar-->
												</div>
												<!--end::Header-->
												<!--begin::Card body-->
												<div class="card-body d-flex align-items-end p-0">
													<!--begin::Chart-->
													<div id="kt_charts_widget_36" class="min-h-auto w-100 ps-4 pe-6" style="height: 300px"></div>
													<!--end::Chart-->
												</div>
												<!--end::Card body-->
											</div>
                                        </div>
										<div class="col-xl-4">
                                            <!--begin::Chart widget 6-->
                                            <div class="card card-flush">
                                                <!--begin::Header-->
                                                <div class="card-header py-7 mb-3">
                                                    <!--begin::Title-->
                                                    <h3 class="card-title align-items-start flex-column">
                                                        <span class="card-label fw-bold text-gray-800">Top Selling Categories</span>
                                                        <span class="text-gray-400 mt-1 fw-semibold fs-6">8k social visitors</span>
                                                    </h3>
                                                    <!--end::Title-->
                                                    <!--begin::Toolbar-->
                                                    <div class="card-toolbar">
                                                        <a href="../../demo34/dist/apps/ecommerce/sales/listing.html" class="btn btn-sm btn-light">View All</a>
                                                    </div>
                                                    <!--end::Toolbar-->
                                                </div>
                                                <!--end::Header-->
                                                <!--begin::Body-->
                                                <div class="card-body py-0 ps-6 mt-n12">
                                                    <div id="kt_charts_widget_6"></div>
                                                </div>
                                                <!--end::Body-->
                                            </div>
										</div>
										<!--end::Col-->
										<!--begin::Col-->
										<div class="col-xl-8 ps-xl-12">
											<!--begin::Engage widget 1-->
											<div class="card bgi-position-y-bottom bgi-position-x-end bgi-no-repeat bgi-size-cover min-h-250px bg-body mb-5 mb-xl-8" style="background-position: 100% 50px;background-size: 500px auto;background-image:url('assets/media/misc/city.png')" dir="ltr">
												<!--begin::Body-->
												<div class="card-body d-flex flex-column justify-content-center ps-lg-12">
													<!--begin::Title-->
													<h3 class="text-dark fs-2qx fw-bold mb-7">We are working
													<br />to boost lovely mood</h3>
													<!--end::Title-->
												</div>
												<!--end::Body-->
											</div>
											<!--end::Engage widget 1-->
											<!--begin::Row-->
											<div class="row g-5 g-xl-8 mb-5 mb-xl-8">

											</div>
										</div>
										<!--end::Col-->


										<!--begin::Col-->
										<div class="col-xl-6 mb-5 mb-xl-10">
                                            <div class="card card-flush h-xl-100">
												<!--begin::Card header-->
												<div class="card-header pt-7">
													<!--begin::Title-->
													<h3 class="card-title align-items-start flex-column">
														<span class="card-label fw-bold text-dark">Stock Report</span>
														<span class="text-gray-400 mt-1 fw-semibold fs-6">Total 2,356 Items in the Stock</span>
													</h3>
													<!--end::Title-->
													<!--begin::Actions-->
													<div class="card-toolbar">
														<!--begin::Filters-->
														<div class="d-flex flex-stack flex-wrap gap-4">
															<!--begin::Status-->
															<div class="d-flex align-items-center fw-bold">
																<!--begin::Label-->
																<div class="text-muted fs-7 me-2">Status</div>
																<!--end::Label-->
																<!--begin::Select-->
																<select class="form-select form-select-transparent text-dark fs-7 lh-1 fw-bold py-0 ps-3 w-auto" data-control="select2" data-hide-search="true" data-dropdown-css-class="w-150px" data-placeholder="Select an option" data-kt-table-widget-5="filter_status">
																	<option></option>
																	<option value="Show All" selected="selected">Show All</option>
																	<option value="In Stock">In Stock</option>
																	<option value="Out of Stock">Out of Stock</option>
																	<option value="Low Stock">Low Stock</option>
																</select>
																<!--end::Select-->
															</div>
															<!--end::Status-->
															<!--begin::Search-->
															<a href="../../demo34/dist/apps/ecommerce/catalog/products.html" class="btn btn-light btn-sm">View Stock</a>
															<!--end::Search-->
														</div>
														<!--begin::Filters-->
													</div>
													<!--end::Actions-->
												</div>
												<!--end::Card header-->
												<!--begin::Card body-->
												<div class="card-body">
													<!--begin::Table-->
													<table class="table align-middle table-row-dashed fs-6 gy-3" id="kt_table_widget_5_table">
														<!--begin::Table head-->
														<thead>
															<!--begin::Table row-->
															<tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
																<th class="min-w-150px">Item</th>
																<th class="text-end pe-3 min-w-100px">SKU</th>
																<th class="text-end pe-3 min-w-150px">Date Added</th>
																<th class="text-end pe-3 min-w-100px">Price</th>
																<th class="text-end pe-3 min-w-100px">Status</th>
																<th class="text-end pe-0 min-w-75px">Qty</th>
															</tr>
															<!--end::Table row-->
														</thead>
														<!--end::Table head-->
														<!--begin::Table body-->
														<tbody class="fw-bold text-gray-600">
															<tr>
																<!--begin::Item-->
																<td>
																	<a href="#" class="text-dark text-hover-primary">Yumika</a>
																</td>
																<!--end::Item-->
																<!--begin::Product ID-->
																<td class="text-end">#XGY-356</td>
																<!--end::Product ID-->
																<!--begin::Date added-->
																<td class="text-end">02 Apr, 2023</td>
																<!--end::Date added-->
																<!--begin::Price-->
																<td class="text-end">Rp. 150.000</td>
																<!--end::Price-->
																<!--begin::Status-->
																<td class="text-end">
																	<span class="badge py-3 px-4 fs-7 badge-light-primary">In Stock</span>
																</td>
																<!--end::Status-->
																<!--begin::Qty-->
																<td class="text-end" data-order="58">
																	<span class="text-dark fw-bold">58 PCS</span>
																</td>
																<!--end::Qty-->
															</tr>
															<tr>
																<!--begin::Item-->
																<td>
																	<a href="#" class="text-dark text-hover-primary">Yumika</a>
																</td>
																<!--end::Item-->
																<!--begin::Product ID-->
																<td class="text-end">#XGY-356</td>
																<!--end::Product ID-->
																<!--begin::Date added-->
																<td class="text-end">02 Apr, 2023</td>
																<!--end::Date added-->
																<!--begin::Price-->
																<td class="text-end">Rp. 150.000</td>
																<!--end::Price-->
																<!--begin::Status-->
																<td class="text-end">
																	<span class="badge py-3 px-4 fs-7 badge-light-primary">In Stock</span>
																</td>
																<!--end::Status-->
																<!--begin::Qty-->
																<td class="text-end" data-order="58">
																	<span class="text-dark fw-bold">58 PCS</span>
																</td>
																<!--end::Qty-->
															</tr>
															<tr>
																<!--begin::Item-->
																<td>
																	<a href="#" class="text-dark text-hover-primary">Yumika</a>
																</td>
																<!--end::Item-->
																<!--begin::Product ID-->
																<td class="text-end">#XGY-356</td>
																<!--end::Product ID-->
																<!--begin::Date added-->
																<td class="text-end">02 Apr, 2023</td>
																<!--end::Date added-->
																<!--begin::Price-->
																<td class="text-end">Rp. 150.000</td>
																<!--end::Price-->
																<!--begin::Status-->
																<td class="text-end">
																	<span class="badge py-3 px-4 fs-7 badge-light-primary">In Stock</span>
																</td>
																<!--end::Status-->
																<!--begin::Qty-->
																<td class="text-end" data-order="58">
																	<span class="text-dark fw-bold">58 PCS</span>
																</td>
																<!--end::Qty-->
															</tr>
															<tr>
																<!--begin::Item-->
																<td>
																	<a href="#" class="text-dark text-hover-primary">Yumika</a>
																</td>
																<!--end::Item-->
																<!--begin::Product ID-->
																<td class="text-end">#XGY-356</td>
																<!--end::Product ID-->
																<!--begin::Date added-->
																<td class="text-end">02 Apr, 2023</td>
																<!--end::Date added-->
																<!--begin::Price-->
																<td class="text-end">Rp. 150.000</td>
																<!--end::Price-->
																<!--begin::Status-->
																<td class="text-end">
																	<span class="badge py-3 px-4 fs-7 badge-light-primary">In Stock</span>
																</td>
																<!--end::Status-->
																<!--begin::Qty-->
																<td class="text-end" data-order="58">
																	<span class="text-dark fw-bold">58 PCS</span>
																</td>
																<!--end::Qty-->
															</tr>
															<tr>
																<!--begin::Item-->
																<td>
																	<a href="#" class="text-dark text-hover-primary">Yumika</a>
																</td>
																<!--end::Item-->
																<!--begin::Product ID-->
																<td class="text-end">#XGY-356</td>
																<!--end::Product ID-->
																<!--begin::Date added-->
																<td class="text-end">02 Apr, 2023</td>
																<!--end::Date added-->
																<!--begin::Price-->
																<td class="text-end">Rp. 150.000</td>
																<!--end::Price-->
																<!--begin::Status-->
																<td class="text-end">
																	<span class="badge py-3 px-4 fs-7 badge-light-primary">In Stock</span>
																</td>
																<!--end::Status-->
																<!--begin::Qty-->
																<td class="text-end" data-order="58">
																	<span class="text-dark fw-bold">58 PCS</span>
																</td>
																<!--end::Qty-->
															</tr>
															<tr>
																<!--begin::Item-->
																<td>
																	<a href="#" class="text-dark text-hover-primary">Yumika</a>
																</td>
																<!--end::Item-->
																<!--begin::Product ID-->
																<td class="text-end">#XGY-356</td>
																<!--end::Product ID-->
																<!--begin::Date added-->
																<td class="text-end">02 Apr, 2023</td>
																<!--end::Date added-->
																<!--begin::Price-->
																<td class="text-end">Rp. 150.000</td>
																<!--end::Price-->
																<!--begin::Status-->
																<td class="text-end">
																	<span class="badge py-3 px-4 fs-7 badge-light-primary">In Stock</span>
																</td>
																<!--end::Status-->
																<!--begin::Qty-->
																<td class="text-end" data-order="58">
																	<span class="text-dark fw-bold">58 PCS</span>
																</td>
																<!--end::Qty-->
															</tr>
															<tr>
																<!--begin::Item-->
																<td>
																	<a href="#" class="text-dark text-hover-primary">Yumika</a>
																</td>
																<!--end::Item-->
																<!--begin::Product ID-->
																<td class="text-end">#XGY-356</td>
																<!--end::Product ID-->
																<!--begin::Date added-->
																<td class="text-end">02 Apr, 2023</td>
																<!--end::Date added-->
																<!--begin::Price-->
																<td class="text-end">Rp. 150.000</td>
																<!--end::Price-->
																<!--begin::Status-->
																<td class="text-end">
																	<span class="badge py-3 px-4 fs-7 badge-light-primary">In Stock</span>
																</td>
																<!--end::Status-->
																<!--begin::Qty-->
																<td class="text-end" data-order="58">
																	<span class="text-dark fw-bold">58 PCS</span>
																</td>
																<!--end::Qty-->
															</tr>
															<tr>
																<!--begin::Item-->
																<td>
																	<a href="#" class="text-dark text-hover-primary">Yumika</a>
																</td>
																<!--end::Item-->
																<!--begin::Product ID-->
																<td class="text-end">#XGY-356</td>
																<!--end::Product ID-->
																<!--begin::Date added-->
																<td class="text-end">02 Apr, 2023</td>
																<!--end::Date added-->
																<!--begin::Price-->
																<td class="text-end">Rp. 150.000</td>
																<!--end::Price-->
																<!--begin::Status-->
																<td class="text-end">
																	<span class="badge py-3 px-4 fs-7 badge-light-primary">In Stock</span>
																</td>
																<!--end::Status-->
																<!--begin::Qty-->
																<td class="text-end" data-order="58">
																	<span class="text-dark fw-bold">58 PCS</span>
																</td>
																<!--end::Qty-->
															</tr>
														</tbody>
														<!--end::Table body-->
													</table>
													<!--end::Table-->
												</div>
												<!--end::Card body-->
											</div>
											<!--end::Table Widget 5-->
                                        </div>
										<div class="col-xl-6 mb-5 mb-xl-10">
											<!--begin::Table Widget 4-->
											<div class="card card-flush h-xl-100">
												<!--begin::Card header-->
												<div class="card-header pt-7">
													<!--begin::Title-->
													<h3 class="card-title align-items-start flex-column">
														<span class="card-label fw-bold text-gray-800">Product Orders</span>
														<span class="text-gray-400 mt-1 fw-semibold fs-6">Avg. 57 orders per day</span>
													</h3>
													<!--end::Title-->
													<!--begin::Actions-->
													<div class="card-toolbar">
														<!--begin::Filters-->
														<div class="d-flex flex-stack flex-wrap gap-4">
															<!--begin::Destination-->
															<div class="d-flex align-items-center fw-bold">
																<!--begin::Label-->
																<div class="text-gray-400 fs-7 me-2">Cateogry</div>
																<!--end::Label-->
																<!--begin::Select-->
																<select class="form-select form-select-transparent text-graY-800 fs-base lh-1 fw-bold py-0 ps-3 w-auto" data-control="select2" data-hide-search="true" data-dropdown-css-class="w-150px" data-placeholder="Select an option">
																	<option></option>
																	<option value="Show All" selected="selected">Show All</option>
																	<option value="a">Category A</option>
																	<option value="b">Category A</option>
																</select>
																<!--end::Select-->
															</div>
															<!--end::Destination-->
															<!--begin::Status-->
															<div class="d-flex align-items-center fw-bold">
																<!--begin::Label-->
																<div class="text-gray-400 fs-7 me-2">Status</div>
																<!--end::Label-->
																<!--begin::Select-->
																<select class="form-select form-select-transparent text-dark fs-7 lh-1 fw-bold py-0 ps-3 w-auto" data-control="select2" data-hide-search="true" data-dropdown-css-class="w-150px" data-placeholder="Select an option" data-kt-table-widget-4="filter_status">
																	<option></option>
																	<option value="Show All" selected="selected">Show All</option>
																	<option value="Shipped">Shipped</option>
																	<option value="Confirmed">Confirmed</option>
																	<option value="Rejected">Rejected</option>
																	<option value="Pending">Pending</option>
																</select>
																<!--end::Select-->
															</div>
															<!--end::Status-->
															<!--begin::Search-->
															<div class="position-relative my-1">
																<i class="ki-outline ki-magnifier fs-2 position-absolute top-50 translate-middle-y ms-4"></i>
																<input type="text" data-kt-table-widget-4="search" class="form-control w-150px fs-7 ps-12" placeholder="Search" />
															</div>
															<!--end::Search-->
														</div>
														<!--begin::Filters-->
													</div>
													<!--end::Actions-->
												</div>
												<!--end::Card header-->
												<!--begin::Card body-->
												<div class="card-body pt-2">
													<!--begin::Table-->
													<table class="table align-middle table-row-dashed fs-6 gy-3" id="kt_table_widget_4_table">
														<!--begin::Table head-->
														<thead>
															<!--begin::Table row-->
															<tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
																<th class="min-w-100px">Order ID</th>
																<th class="text-end min-w-100px">Created</th>
																<th class="text-end min-w-125px">Customer</th>
																<th class="text-end min-w-100px">Total</th>
																<th class="text-end min-w-100px">Profit</th>
																<th class="text-end min-w-50px">Status</th>
																<th class="text-end"></th>
															</tr>
															<!--end::Table row-->
														</thead>
														<!--end::Table head-->
														<!--begin::Table body-->
														<tbody class="fw-bold text-gray-600">
															<tr data-kt-table-widget-4="subtable_template" class="d-none">
																<td colspan="2">
																	<div class="d-flex align-items-center gap-3">
																		<a href="#" class="symbol symbol-50px bg-secondary bg-opacity-25 rounded">
																			<img src="" data-kt-src-path="assets/media/stock/ecommerce/" alt="" data-kt-table-widget-4="template_image" />
																		</a>
																		<div class="d-flex flex-column text-muted">
																			<a href="#" class="text-gray-800 text-hover-primary fw-bold" data-kt-table-widget-4="template_name">Product name</a>
																			<div class="fs-7" data-kt-table-widget-4="template_description">Product description</div>
																		</div>
																	</div>
																</td>
																<td class="text-end">
																	<div class="text-gray-800 fs-7">Cost</div>
																	<div class="text-muted fs-7 fw-bold" data-kt-table-widget-4="template_cost">1</div>
																</td>
																<td class="text-end">
																	<div class="text-gray-800 fs-7">Qty</div>
																	<div class="text-muted fs-7 fw-bold" data-kt-table-widget-4="template_qty">1</div>
																</td>
																<td class="text-end">
																	<div class="text-gray-800 fs-7">Total</div>
																	<div class="text-muted fs-7 fw-bold" data-kt-table-widget-4="template_total">name</div>
																</td>
																<td class="text-end">
																	<div class="text-gray-800 fs-7 me-3">On hand</div>
																	<div class="text-muted fs-7 fw-bold" data-kt-table-widget-4="template_stock">32</div>
																</td>
																<td></td>
															</tr>
															<tr>
																<td>
																	<a href="../../demo34/dist/apps/ecommerce/catalog/edit-product.html" class="text-gray-800 text-hover-primary">#XGY-346</a>
																</td>
																<td class="text-end">7 min ago</td>
																<td class="text-end">
																	<a href="#" class="text-gray-600 text-hover-primary">Albert Flores</a>
																</td>
																<td class="text-end">$630.00</td>
																<td class="text-end">
																	<span class="text-gray-800 fw-bolder">$86.70</span>
																</td>
																<td class="text-end">
																	<span class="badge py-3 px-4 fs-7 badge-light-warning">Pending</span>
																</td>
																<td class="text-end">
																	<button type="button" class="btn btn-sm btn-icon btn-light btn-active-light-primary toggle h-25px w-25px" data-kt-table-widget-4="expand_row">
																		<i class="ki-outline ki-plus fs-4 m-0 toggle-off"></i>
																		<i class="ki-outline ki-minus fs-4 m-0 toggle-on"></i>
																	</button>
																</td>
															</tr>
															<tr>
																<td>
																	<a href="../../demo34/dist/apps/ecommerce/catalog/edit-product.html" class="text-gray-800 text-hover-primary">#YHD-047</a>
																</td>
																<td class="text-end">52 min ago</td>
																<td class="text-end">
																	<a href="#" class="text-gray-600 text-hover-primary">Jenny Wilson</a>
																</td>
																<td class="text-end">$25.00</td>
																<td class="text-end">
																	<span class="text-gray-800 fw-bolder">$4.20</span>
																</td>
																<td class="text-end">
																	<span class="badge py-3 px-4 fs-7 badge-light-primary">Confirmed</span>
																</td>
																<td class="text-end">
																	<button type="button" class="btn btn-sm btn-icon btn-light btn-active-light-primary toggle h-25px w-25px" data-kt-table-widget-4="expand_row">
																		<i class="ki-outline ki-plus fs-4 m-0 toggle-off"></i>
																		<i class="ki-outline ki-minus fs-4 m-0 toggle-on"></i>
																	</button>
																</td>
															</tr>
															<tr>
																<td>
																	<a href="../../demo34/dist/apps/ecommerce/catalog/edit-product.html" class="text-gray-800 text-hover-primary">#SRR-678</a>
																</td>
																<td class="text-end">1 hour ago</td>
																<td class="text-end">
																	<a href="#" class="text-gray-600 text-hover-primary">Robert Fox</a>
																</td>
																<td class="text-end">$1,630.00</td>
																<td class="text-end">
																	<span class="text-gray-800 fw-bolder">$203.90</span>
																</td>
																<td class="text-end">
																	<span class="badge py-3 px-4 fs-7 badge-light-warning">Pending</span>
																</td>
																<td class="text-end">
																	<button type="button" class="btn btn-sm btn-icon btn-light btn-active-light-primary toggle h-25px w-25px" data-kt-table-widget-4="expand_row">
																		<i class="ki-outline ki-plus fs-4 m-0 toggle-off"></i>
																		<i class="ki-outline ki-minus fs-4 m-0 toggle-on"></i>
																	</button>
																</td>
															</tr>
															<tr>
																<td>
																	<a href="../../demo34/dist/apps/ecommerce/catalog/edit-product.html" class="text-gray-800 text-hover-primary">#PXF-534</a>
																</td>
																<td class="text-end">3 hour ago</td>
																<td class="text-end">
																	<a href="#" class="text-gray-600 text-hover-primary">Cody Fisher</a>
																</td>
																<td class="text-end">$119.00</td>
																<td class="text-end">
																	<span class="text-gray-800 fw-bolder">$12.00</span>
																</td>
																<td class="text-end">
																	<span class="badge py-3 px-4 fs-7 badge-light-success">Shipped</span>
																</td>
																<td class="text-end">
																	<button type="button" class="btn btn-sm btn-icon btn-light btn-active-light-primary toggle h-25px w-25px" data-kt-table-widget-4="expand_row">
																		<i class="ki-outline ki-plus fs-4 m-0 toggle-off"></i>
																		<i class="ki-outline ki-minus fs-4 m-0 toggle-on"></i>
																	</button>
																</td>
															</tr>
															<tr>
																<td>
																	<a href="../../demo34/dist/apps/ecommerce/catalog/edit-product.html" class="text-gray-800 text-hover-primary">#XGD-249</a>
																</td>
																<td class="text-end">2 day ago</td>
																<td class="text-end">
																	<a href="#" class="text-gray-600 text-hover-primary">Arlene McCoy</a>
																</td>
																<td class="text-end">$660.00</td>
																<td class="text-end">
																	<span class="text-gray-800 fw-bolder">$52.26</span>
																</td>
																<td class="text-end">
																	<span class="badge py-3 px-4 fs-7 badge-light-success">Shipped</span>
																</td>
																<td class="text-end">
																	<button type="button" class="btn btn-sm btn-icon btn-light btn-active-light-primary toggle h-25px w-25px" data-kt-table-widget-4="expand_row">
																		<i class="ki-outline ki-plus fs-4 m-0 toggle-off"></i>
																		<i class="ki-outline ki-minus fs-4 m-0 toggle-on"></i>
																	</button>
																</td>
															</tr>
															<tr>
																<td>
																	<a href="../../demo34/dist/apps/ecommerce/catalog/edit-product.html" class="text-gray-800 text-hover-primary">#SKP-035</a>
																</td>
																<td class="text-end">2 day ago</td>
																<td class="text-end">
																	<a href="#" class="text-gray-600 text-hover-primary">Eleanor Pena</a>
																</td>
																<td class="text-end">$290.00</td>
																<td class="text-end">
																	<span class="text-gray-800 fw-bolder">$29.00</span>
																</td>
																<td class="text-end">
																	<span class="badge py-3 px-4 fs-7 badge-light-danger">Rejected</span>
																</td>
																<td class="text-end">
																	<button type="button" class="btn btn-sm btn-icon btn-light btn-active-light-primary toggle h-25px w-25px" data-kt-table-widget-4="expand_row">
																		<i class="ki-outline ki-plus fs-4 m-0 toggle-off"></i>
																		<i class="ki-outline ki-minus fs-4 m-0 toggle-on"></i>
																	</button>
																</td>
															</tr>
															<tr>
																<td>
																	<a href="../../demo34/dist/apps/ecommerce/catalog/edit-product.html" class="text-gray-800 text-hover-primary">#SKP-567</a>
																</td>
																<td class="text-end">7 min ago</td>
																<td class="text-end">
																	<a href="#" class="text-gray-600 text-hover-primary">Dan Wilson</a>
																</td>
																<td class="text-end">$590.00</td>
																<td class="text-end">
																	<span class="text-gray-800 fw-bolder">$50.00</span>
																</td>
																<td class="text-end">
																	<span class="badge py-3 px-4 fs-7 badge-light-success">Shipped</span>
																</td>
																<td class="text-end">
																	<button type="button" class="btn btn-sm btn-icon btn-light btn-active-light-primary toggle h-25px w-25px" data-kt-table-widget-4="expand_row">
																		<i class="ki-outline ki-plus fs-4 m-0 toggle-off"></i>
																		<i class="ki-outline ki-minus fs-4 m-0 toggle-on"></i>
																	</button>
																</td>
															</tr>
														</tbody>
														<!--end::Table body-->
													</table>
													<!--end::Table-->
												</div>
												<!--end::Card body-->
											</div>
											<!--end::Table Widget 4-->
										</div>
										<!--end::Col-->
									</div>
									<!--end::Row-->
								</div>
								<!--end::Content-->
							</div>
							<!--end::Content wrapper-->

@endsection

@section('customjs')
