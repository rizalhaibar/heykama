<!DOCTYPE html>
<html lang="en">
<head>

	<title>@yield('title')</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="{{ asset('landingPageHeyKama/images/icons/Logo-04.png')}}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('landingPageHeyKama/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('landingPageHeyKama/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('landingPageHeyKama/fonts/themify/themify-icons.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('landingPageHeyKama/fonts/Linearicons-Free-v1.0.0/icon-font.min.cs')}}s">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('landingPageHeyKama/fonts/elegant-font/html-css/style.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('landingPageHeyKama/vendor/animate/animate.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('landingPageHeyKama/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('landingPageHeyKama/vendor/animsition/css/animsition.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('landingPageHeyKama/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('landingPageHeyKama/vendor/daterangepicker/daterangepicker.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('landingPageHeyKama/vendor/slick/slick.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('landingPageHeyKama/vendor/lightbox2/css/lightbox.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('landingPageHeyKama/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('landingPageHeyKama/css/main.css')}}">
    <style>
        @font-face { font-family: HankenGrotesk; src: url('HankenGrotesk-VariableFont_wght.ttf'); }
        a{
            font-family: HankenGrotesk
        }


        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:90px;
            right:40px;
            background-color:#0C9;
            color:#FFF;
            border-radius:50px;
            text-align:center;
            box-shadow: 2px 2px 3px #999;
        }

        .my-float{
            margin-top:22px;
        }
    </style>
<!--===============================================================================================-->
</head>
<body class="animsition">


	<!-- Header -->
	<header class="header1"  style="z-index:100 !important; position: relative;">
		<!-- Header desktop -->
		<div class="container-menu-header">
			<div class="topbar">
				<div class="topbar-social">
                    <a href="https://www.tiktok.com/@hey.kama" class="topbar-social-item"><img src="{{ asset('icon/black/Tiktok Black.svg') }}" style="width:15px" alt="ICON"></a>
                    <a href="https://www.instagram.com/hey.kama/" class="topbar-social-item"><img src="{{ asset('icon/black/Instagram Black.svg') }}" style="width:15px" alt="ICON"></a>
					<a href="https://www.youtube.com/@heykama" class="topbar-social-item fa fa-youtube-play"></a>
				</div>


				<div class="topbar-child2">
					<span class="topbar-email">
						work.heykama@gmail.com
					</span>
				</div>
			</div>

			<div class="wrap_header">
				<!-- Logo -->
				<a href="{{ route('heykama.index') }}" class="logo grayscale">
					<img src="{{ asset('landingPageHeyKama/images/icons/Logo-03.png')}}" style="width: auto; height: 800px;" alt="IMG-LOGO">
				</a>

				<!-- Menu -->
				<div class="wrap_menu">
					<nav class="menu">
						<ul class="main_menu">


							<li>
								<a href="{{ route('heykama.index') }}">Home</a>
							</li>

							<li>
								<a href="#">Promo</a>
								<ul class="sub_menu">
									<li><a href="{{ route('heykama.promo') }}">Promo 1</a></li>
								</ul>
							</li>

							<li class="sale-noti">
								<a href="{{ route('heykama.shop') }}" >Shop</a>
								<ul class="sub_menu">
									<li>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="col-md-12 menugrid">
                                                    <a href="{{ route('heykama.shop') }}">ON SALE</a>
                                                </div>
                                                <div class="col-md-12 menugrid">
                                                    <a href="{{ route('heykama.shop') }}">NEW ARRIVALS</a>
                                                </div>
                                                <div class="col-md-12 menugrid">
                                                    <a href="{{ route('heykama.shop') }}">ALL PRODUCTS</a>
                                                </div>
                                                <div class="col-md-12 menugrid">
                                                    <a href="{{ route('heykama.shop') }}">SUNYATA COLLECTIONS</a>
                                                </div>
                                                <div class="col-md-12 menugrid">
                                                    <a href="{{ route('heykama.shop') }}">BEYOND THE SHADES</a>
                                                </div>
                                            </div>
                                            <div class="col-md-5" style="margin-left: -20px">
                                                <div class="col-md-12 menugrid">
                                                    <a href="{{ route('heykama.shop') }}">ALL FRAME</a>
                                                </div>
                                                <div class="col-md-12 menugrid">
                                                    <a href="{{ route('heykama.shop') }}">FRAME SQUARE</a>
                                                </div>
                                                <div class="col-md-12 menugrid">
                                                    <a href="{{ route('heykama.shop') }}">FRAME ROUND</a>
                                                </div>
                                                <div class="col-md-12 menugrid">
                                                    <a href="{{ route('heykama.shop') }}">FRAME CAT EYE</a>
                                                </div>
                                                <div class="col-md-12 menugrid">
                                                    <a href="{{ route('heykama.shop') }}">LENSES</a>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="margin-left: -70px">
                                                <div class="col-md-12 menugrid">
                                                    <a href="{{ route('heykama.shop') }}">ACCESORIESS</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
								</ul>
							</li>

							<li>
								<a href="{{ route('heykama.blog') }}">Journal</a>
							</li>

							<li>
								<a href="{{ route('heykama.about') }}">About</a>
							</li>
						</ul>
					</nav>
                    <div class="header-icons">
                        @guest
                        <a href="{{ route('login') }}" class="header-wrapicon1 dis-block">
                            Sign In
                        </a>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('heykama.profile') }}">
                                        {{ __('Profile') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('heykama.cart') }}">
                                        {{ __('Checkout') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('heykama.order') }}">
                                        {{ __('Orders') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>


                        <span class="linedivide1"></span>

                        @php($total_cart=count($listcart))
                        <div class="header-wrapicon2">
                            <!--img src="{{ asset('landingPageHeyKama/images/icons/icon-header-02.png') }}" class="header-icon1 js-show-header-dropdown" alt="ICON"-->
                            <img src="{{ asset('icon/black/Shopping Bag Black.svg') }}" class="header-icon1 js-show-header-dropdown" alt="ICON">
                            <span class="header-icons-noti">{{$total_cart}}</span>

                            <!-- Header cart noti -->
                            <div class="header-cart header-dropdown">
                                <ul class="header-cart-wrapitem">
                                    @php($total=0)
                                    @foreach($listcart as $cart)
                                        <li class="header-cart-item">
                                            <div class="header-cart-item-img">
                                                <img src="{{Storage::url($cart->url_thumnail)}}" alt="IMG">
                                            </div>

                                            <div class="header-cart-item-txt">
                                                <a href="#" class="header-cart-item-name">
                                                   {{$cart->name}} | {{$cart->variant_name}}
                                                </a>

                                                <span class="header-cart-item-info">
                                                    {{$cart->qty}} x {{$cart->price}}
                                                </span>
                                            </div>
                                        </li>
                                        @php($total+=$cart->qty*$cart->price)
                                    @endforeach
                                </ul>
                                <div class="header-cart-total">
                                    Total: Rp. {{$total}},-
                                </div>

                                <div class="header-cart-buttons">

                                    <div class="header-cart-wrapbtn">
                                        <!-- Button -->
                                        <a href="{{ route('heykama.cart') }}" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                            View Cart
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endguest
                    </div>
				</div>
            </div>
        </div>

		<!-- Header Mobile -->
		<div class="wrap_header_mobile">
			<!-- Logo moblie -->
			<a href="{{ route('login') }}" class="logo-mobile">
				<img src="{{ asset('landingPageHeyKama/images/icons/Logo-03.png')}}" alt="IMG-LOGO">
			</a>

			<!-- Button show menu -->
			<div class="btn-show-menu">
                @guest
                @else
				<!-- Header Icon mobile -->
				<div class="header-icons-mobile">

					<span class="linedivide2"></span>

					<div class="header-wrapicon2">
						<img src="{{ asset('icon/black/Shopping Bag Black.svg') }}" class="header-icon1 js-show-header-dropdown" alt="ICON">
						<span class="header-icons-noti">{{$total_cart}}</span>

						<!-- Header cart noti -->
						<div class="header-cart header-dropdown">
							<ul class="header-cart-wrapitem">
                                @php($total=0)
                                @foreach($listcart as $cart)
                                    <li class="header-cart-item">
                                        <div class="header-cart-item-img">
                                            <img src="{{Storage::url($cart->url_thumnail)}}" alt="IMG">
                                        </div>

                                        <div class="header-cart-item-txt">
                                            <a href="#" class="header-cart-item-name">
                                               {{$cart->name}} | {{$cart->variant_name}}
                                            </a>

                                            <span class="header-cart-item-info">
                                                {{$cart->qty}} x {{$cart->price}}
                                            </span>
                                        </div>
                                    </li>
                                    @php($total+=$cart->qty*$cart->price)
                                @endforeach
                            </ul>
                            <div class="header-cart-total">
                                Total: Rp. {{$total}},-
                            </div>

                            <div class="header-cart-buttons">

                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="{{ route('heykama.cart') }}" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        View Cart
                                    </a>
                                </div>
                            </div>
						</div>
					</div>
				</div>

                @endguest
				<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</div>
			</div>
		</div>

		<!-- Menu Mobile -->
		<div class="wrap-side-menu" >
			<nav class="side-menu">
				<ul class="main-menu">

					<li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
						<div class="topbar-child2-mobile">
							<span class="topbar-email">
								work.heykama@gmail.com
							</span>

						</div>
					</li>

					<li class="item-topbar-mobile p-l-10">
						<div class="topbar-social-mobile">
							<a href="#" class="topbar-social-item fa fa-facebook"></a>
							<a href="#" class="topbar-social-item fa fa-instagram"></a>
							<a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
							<a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
							<a href="#" class="topbar-social-item fa fa-youtube-play"></a>
						</div>
					</li>

					<li class="item-menu-mobile">
						<a href="{{ route('heykama.index') }}">Home</a>
					</li>

					<li class="item-menu-mobile">
                        <a href="{{ route('heykama.promo') }}">Promo</a>
					</li>

					<li class="item-menu-mobile">
						<a href="{{ route('heykama.shop') }}">Shop</a>
					</li>

					<li class="item-menu-mobile">
                        <a href="{{ route('heykama.blog') }}">Journal</a>
					</li>

					<li class="item-menu-mobile">
                        <a href="{{ route('heykama.about') }}">About</a>
					</li>
                    @guest

					<li class="item-menu-mobile">
                        <a href="{{ route('login') }}">Login</a>
					</li>
                    @else

                    @endguest
				</ul>
			</nav>
		</div>
    </header>
    @yield('content')


    <a href="https://wa.me/6281222400057?text=Hallo%20HeyKama,%20tanya%20dong!" target="_blank" class="float">
        <i class="fa fa-whatsapp my-float"></i>
    </a>

	<!-- Footer -->
	<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
		<div class="flex-w p-b-90">
			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					GET IN TOUCH
				</h4>

				<div>
					<h5 class="s-text12 p-b-30">
                        Customer Service
                    </h5>
                    <p>+62 811 240 7711
                        <br/>
                        work.heykama@gmail.com</p>

					<div class="flex-m p-t-30">
						<a href="https://www.tiktok.com/@hey.kama" class="fs-18 color1 p-r-20"><img src="{{ asset('icon/black/Tiktok Black.svg') }}" alt="ICON"></a>
						<a href="https://www.instagram.com/hey.kama/" class="fs-18 color1 p-r-20"><img src="{{ asset('icon/black/Instagram Black.svg') }}" alt="ICON"></a>
						<a href="https://www.youtube.com/@heykama" class="fs-18 color1 p-r-20 fa fa-youtube-play"></a>
					</div>
				</div>
			</div>

			<div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
				<h4 class="s-text12 p-b-30">
					Categories
				</h4>

				<ul>
					<li class="p-b-9">
						<a href="#" class="s-text7">
							Frame
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Lens
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Accesoriess
						</a>
					</li>
				</ul>
			</div>

			<div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
				<h4 class="s-text12 p-b-30">
					In Case You’re Wondering
				</h4>

				<ul>
					<li class="p-b-9">
						<a href="#" class="s-text7">
							FAQs
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							How To Order
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Return & Refund Policy
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Product Warranty
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							How To Clean
						</a>
					</li>
				</ul>
			</div>

			<div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">

			</div>

			<div class="w-size8 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					Newsletter
				</h4>

				<form>
					<div class="effect1 w-size9">
						<input class="s-text7 bg6 w-full p-b-5" type="text" name="email" placeholder="email@example.com">
						<span class="effect1-line"></span>
					</div>

					<div class="w-size2 p-t-20">
						<!-- Button -->
						<button class="flex-c-m size2 bg4 bo-rad-23 hov1 m-text3 trans-0-4">
							Subscribe
						</button>
					</div>

				</form>
			</div>
		</div>
	</footer>



	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
	<script type="text/javascript" src="{{ asset('landingPageHeyKama/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="{{ asset('landingPageHeyKama/vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="{{ asset('landingPageHeyKama/vendor/bootstrap/js/popper.js')}}"></script>
	<script type="text/javascript" src="{{ asset('landingPageHeyKama/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="{{ asset('landingPageHeyKama/vendor/select2/select2.min.js')}}"></script>
	<script type="text/javascript">
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="{{ asset('landingPageHeyKama/vendor/slick/slick.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('landingPageHeyKama/js/slick-custom.js')}}"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="{{ asset('landingPageHeyKama/vendor/countdowntime/countdowntime.js')}}"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="{{ asset('landingPageHeyKama/vendor/lightbox2/js/lightbox.min.js')}}"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="{{ asset('landingPageHeyKama/vendor/sweetalert/sweetalert.min.js')}}"></script>
	<script type="text/javascript">
		$('.block2-btn-addcart').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to cart !", "success");
			});
		});

		$('.block2-btn-addwishlist').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");
			});
		});
        // $('#modalSubcribe').modal('show')

	</script>

<!--===============================================================================================-->
	<script src="{{ asset('landingPageHeyKama/js/main.js')}}"></script>


<script src="{{ asset('landingPageHeyKama/js/map-custom.js')}}"></script>

<!-- javascript -->
<script src="{{asset('assetsfront/js/jquery.min.js')}}"></script>
<script src="{{asset('assetsfront/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assetsfront/js/gumshoe.polyfills.min.js')}}"></script>

<!-- Icon -->
<script src="{{asset('assetsfront/js/feather.min.js')}}"></script>
<!-- TYPED -->
<script src="{{asset('assetsfront/js/typed.init.js')}}"></script>
<script src="{{asset('assetsfront/js/jquery.event.move.js')}}"></script>
<script src="{{asset('assetsfront/js/jquery.twentytwenty.js')}}"></script>
<!-- Main Js -->
<script src="{{asset('assetsfront/js/app.js')}}"></script>

<script>
    setTimeout( $('#modalSubcribe').modal('hide'), 5000)
</script>
@yield('onpage-js')
<script>

    function saveToChart(id){

        var datapost={
            "id"  :   id,
        };
        $.ajax({
            type: "POST",
            url: "{{route('heykama.addtocart')}}",
            data : JSON.stringify(datapost),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function(response) {
                location.reload();
            }
        });


      }
    </script>

</body>
</html>
