@extends('frontend.layouts.landing')

@section('title', 'HeyKama - Home')

@section('content')
<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url({{ asset('landingPageHeyKama/images/HKM08708.jpg')}}); background-size:cover; padding-top: 200px">
    <h2 class="l-text2 t-center">
        Cart
    </h2>
</section>
@php($sub_total=0)
@php($total=0)
@php($shipping=6000)
@php($discount=0)
@php($i=0)
<!-- Cart -->
<section class="cart bgwhite p-t-70 p-b-100">
    <div class="container">
        <!-- Cart item -->
        <div class="container-table-cart pos-relative">
            <div class="wrap-table-shopping-cart bgwhite">
                <table class="table-shopping-cart">
                    <tr class="table-head">
                        <th class="column-1"></th>
                        <th class="column-2">Product</th>
                        <th class="column-3">Price</th>
                        <th class="column-4">Variant</th>
                        <th class="column-5">Type</th>
                        <th class="column-6">Quantity</th>
                        <th class="column-7">Total</th>
                    </tr>

                    @foreach($listcart as $cart)
                    <tr class="table-row">
                        <td class="column-1">
                            <div class="cart-img-product b-rad-4 o-f-hidden">
                                <img src="{{Storage::url($cart->url_thumnail)}}" alt="IMG-PRODUCT">
                            </div>
                        </td>
                        <td class="column-2">{{$cart->name}}</td>
                        <td class="column-3">{{$cart->price}}</td>
                        <td class="column-4">{{$cart->variant_name}}</td>
                        <td class="column-5">Rounded</td>
                        <td class="column-6">
                            <div class="flex-w bo5 of-hidden w-size17">
                                <button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2"  onclick="changeTotal({{$i}}, {{$cart->price}})">
                                    <i class="fs-12 fa fa-minus" aria-hidden="true"></i>
                                </button>

                                <input class="size8 m-text18 t-center num-product" type="number" id="num_product_{{$i}}" name="num_product_{{$i}}" value="{{$cart->qty}}">

                                <button class="btn-num-product-up color1 flex-c-m size7 bg8 eff2" onclick="changeTotal({{$i}}, {{$cart->price}})">
                                    <i class="fs-12 fa fa-plus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </td>
                        <td class="column-7"><input type='hidden' id='total_{{$i}}' name="total_{{$i}}" value='{{$cart->total}}'/><span id='total_cart_{{$i}}'>{{$cart->total}}</span></td>
                    </tr>
                    <tr class="table-row">
                        <td class="column-1">
                            Notes :
                        </td>
                        <td colspan="7">
                            <div class="size11 bo4 m-r-10">
                                <input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="coupon-code" placeholder="Notes">
                            </div>
                        </td>
                    </tr>

                    @php($sub_total+=$cart->total)
                    @php($discount+=$cart->diskon)
                    @endforeach
                </table>
            </div>
        </div>

        <div class="flex-w flex-sb-m p-t-25 p-b-25 bo8 p-l-35 p-r-60 p-lr-15-sm">
            <div class="flex-w flex-m w-full-sm">
                <div class="size11 bo4 m-r-10">
                    <input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="coupon-code" placeholder="Coupon Code">
                </div>

                <div class="size12 trans-0-4 m-t-10 m-b-10 m-r-10">
                    <!-- Button -->
                    <button class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
                        Apply coupon
                    </button>
                </div>
            </div>

            <!--div class="size10 trans-0-4 m-t-10 m-b-10">
                <button class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
                    Update Cart
                </button>
            </div-->
        </div>

        <!-- Total -->
        <div class="row">
            <div class="col-md-8">
                <div class="bo9 w-size20 p-l-40 p-r-40 p-t-30 p-b-38 m-t-30 m-r-0 m-l-auto p-lr-15-sm" style="width: 100%">
                    <h5 class="m-text20 p-b-24">
                        Select Address
                    </h5>
                    @if($mapping_address_users)
                        @foreach($mapping_address_users as $mau)
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="address" id="address">
                                <label class="form-check-label" for="address">
                                    {{$mau->name}} <br>{{$mau->address}}  | {{$mau->phone_number}}
                                </label>
                            </div>
                        @endforeach
                    @endif
                    <div class="size10 trans-0-4 m-t-10 m-b-10">
                        <a href="#" data-bs-toggle="modal" data-bs-target="#modal_add_category" class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
                            Add Address
                        </a>
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <div class="bo9 w-size18 p-l-40 p-r-40 p-t-30 p-b-38 m-t-30 m-r-0 m-l-auto p-lr-15-sm">
                    <h5 class="m-text20 p-b-24">
                        Cart Totals
                    </h5>

                    <div class="flex-w flex-sb-m p-b-12">
                        <span class="s-text18 w-size19 w-full-sm">
                            Subtotal:
                        </span>

                        <span class="m-text21 w-size20 w-full-sm">
                            Rp. <span id='sub_total'>{{$sub_total}}</span>
                        </span>
                    </div>


                    <!--  -->
                    <div class="flex-w flex-sb-m p-t-26 p-b-30">
                        <span class="m-text22 w-size19 w-full-sm">
                            Total Shipping:
                        </span>

                        <span class="m-text21 w-size20 w-full-sm">
                            Rp. 6.000
                        </span>
                    </div>
                    <div class="flex-w flex-sb-m p-t-26 p-b-30">
                        <span class="m-text22 w-size19 w-full-sm">
                            Discount:
                        </span>

                        <span class="m-text21 w-size20 w-full-sm">
                            Rp. 0
                        </span>
                    </div>

                    <!--  -->
                    <div class="flex-w flex-sb-m p-t-26 p-b-30">
                        <span class="m-text22 w-size19 w-full-sm">
                            Total:
                        </span>

                        <span class="m-text21 w-size20 w-full-sm">

                            @php($total+=$sub_total+$shipping-$discount)
                            Rp. <span id='grand_total'>{{$total}}</span>
                        </span>
                    </div>

                    <div class="size15 trans-0-4">
                        <form class="mx-auto mw-600px w-100 pt-15 pb-10" action="{{route('heykama.checkout')}}" enctype="multipart/form-data" method="POST" id="kt_create_account_form">


                            @csrf
                            <!-- Button -->
                            <button class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
                                Proceed to Checkout
                            </button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_add_category" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-lg p-9">
            <!--begin::Modal content-->
            <div class="modal-content modal-rounded">
                <!--begin::Modal header-->
                <div class="modal-header py-7 d-flex justify-content-between">
                    <!--begin::Modal title-->
                    <h4>Add Address</h4>
                    <!--end::Modal title-->
                    <!--begin::Close-->
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <i class="ki-outline ki-cross fs-1"></i>
                    </div>
                    <!--end::Close-->
                </div>
                <!--begin::Modal header-->
                <!--begin::Modal body-->
                <div class="modal-body scroll-y m-5">
                    <!--begin::Input group-->
                            <div class="mb-10 fv-row">
                                <!--begin::Label-->
                                <label class="required form-label mb-3">Title</label>
                                <!--end::Label-->
                                <input type="text" class="form-control form-control-lg " id="name" name="name" placeholder="Name" value="" />
                            </div>
                            <div class="mb-10 fv-row">
                                <!--begin::Label-->
                                <label class="required form-label mb-3">Phone Number</label>
                                <!--end::Label-->
                                <input type="text" class="form-control form-control-lg " id="phone_number" name="phone_number" placeholder="Phone Number" value="" />
                            </div>
                            <div class="mb-10 fv-row">
                                <!--begin::Label-->
                                <label class="required form-label mb-3">Address</label>
                                <!--end::Label-->
                                <textarea class="form-control" id="address" name="address"  data-kt-autosize="true"></textarea>
                            </div>

                            <div class=" d-flex justify-content-end py-6" style="margin-top: 10px">
                                <button type="reset" class="btn btn-light  me-2" style="border: 1px solid #cfc7c7">Discard</button>

                                <button type="button" onClick="createData()" class="btn btn-primary">Save</button>
                            </div>

                    </div>
                </div>
                <!--begin::Modal body-->
            </div>
        </div>
    </div>
</section>


@section('onpage-js')
<script type="text/javascript">
    function changeTotal(id, price){
        var qty = $('#num_product_'+id).val();
        qty = parseInt(qty) + 1
        var total = qty*price
        var discount = {{$discount}}
        var shipping = {{$shipping}}
        $('#total_cart_'+id).html(total);
        $('#total_'+id).val(total);
        $('#sub_total').html(total);
        $('#grand_total').html(total-discount+shipping);
    }

    function createData(){
       var name   = $('#name').val();
       var address   = $('#address').val();
       var phone_number   = $('#phone_number').val();

              if(name){
                  var datapost={
                      "name"  :   name,
                      "address"  :   address,
                      "phone_number"  :   phone_number,
                    };
                    $.ajax({
                      type: "POST",
                      url: "{{route('heykama.addaddress')}}",
                      data : JSON.stringify(datapost),
                      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                      dataType: 'json',
                      contentType: 'application/json; charset=utf-8',
                      success: function(response) {
                        if (response.success == true) {

                            swal({
                                  title: 'Success!',
                                  text: response.message,
                                  type: 'success',
                                  showCancelButton: false,
                                  confirmButtonText: 'Ok'
                              }).then(function () {
                                 window.location.href = "{{route('heykama.cart')}}";
                              })



                        } else{
                          swal("Failed!", response.message, "error");
                        }
                      }
                    });
              }else{
                  swal("Failed!", "Nama Belum Terisi!", "error");
              }

    }
</script>
<!--===============================================================================================-->
<script src="{{ asset('landingPageHeyKama/js/main.js')}}"></script>
@endsection
