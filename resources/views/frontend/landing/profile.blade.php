@extends('frontend.layouts.landing')

@section('title', 'HeyKama - Home')

@section('content')
<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url({{ asset('landingPageHeyKama/images/HKM08708.jpg')}}); background-size:cover; padding-top: 200px">
    <h2 class="l-text2 t-center">
        Profile
    </h2>
</section>
<section class="cart bgwhite p-t-70 p-b-100">
    <div class="container">
        <div class="row">
            <div class="col-md-4 p-b-30">
                <div class="hov-img-zoom">
                    <img src="{{ asset('landingPageHeyKama/images/HKM02603.jpg') }}" alt="IMG-ABOUT">
                </div>
            </div>

            <div class="col-md-8 p-b-30">
                <div class="mb-10 fv-row">
                    <!--begin::Label-->
                    <label class="required form-label mb-3">Name</label>
                    <!--end::Label-->
                    <input type="text" class="form-control form-control-lg " id="category_name" name="category_name" placeholder="Name" value="{{@$detail_user->name}}" />
                </div>
                <div class="mb-10 fv-row">
                    <!--begin::Label-->
                    <label class="required form-label mb-3">Phone Number</label>
                    <!--end::Label-->
                    <input type="text" class="form-control form-control-lg " id="phone_number" name="phone_number" placeholder="Phone Number" value="{{@$detail_user->phone_number}}" />
                </div>
                <div class="mb-10 fv-row">
                    <!--begin::Label-->
                    <label class="required form-label mb-3">Email</label>
                    <!--end::Label-->
                    <input type="text" class="form-control form-control-lg " id="email" name="email" placeholder="Email" value="{{@$detail_user->email}}" />
                </div>
                <div class="mb-10 fv-row">
                    <!--begin::Label-->
                    <label class="form-label mb-3">Gender</label>
                    <!--end::Label-->
                    <select class="form-select form-control form-control-lg" data-control="select2" id="parent_id" name="parent_id" data-placeholder="Gender">
                        <option value="Pria" {{@$detail_user->gender == 'Pria' ? 'selected' : ''}}>Pria</option>
                        <option value="Wanita" {{@$detail_user->gender == 'Wanita' ? 'selected' : ''}}>Wanita</option>
                    </select>
                </div>
                <div class="mb-10 fv-row">
                    <!--begin::Label-->
                    <label class="required form-label mb-3">Address</label>
                    <!--end::Label-->
                    <textarea class="form-control form-control-lg " id="address" name="address"  data-kt-autosize="true"> {{@$detail_user->address}}</textarea>
                </div>

            </div>
        </div>
    </div>
</section>


@section('onpage-js')
<script type="text/javascript">

</script>
<!--===============================================================================================-->
<script src="{{ asset('landingPageHeyKama/js/main.js')}}"></script>
@endsection
