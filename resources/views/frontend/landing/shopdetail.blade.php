@extends('frontend.layouts.landing')

@section('title', 'HeyKama - '.@$product->name)

@section('content')

<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/main-moscot.css')}}">
    <style>
    .modal .modal-dialog {
        width: 100%;
        max-width: none;
        height: 100%;
        margin: 0;
    }
    .modal .modal-content {
        height: 100%;
        border: 0;
        border-radius: 0;
    }
    .modal .modal-body {
        overflow-y: auto;
        width: 100%;
        max-width: none;
        height: 100%;
    }

    .lense_checkbox_label {
        border: 1px solid #000000;
        display: block;
        padding: 21px 21px 21px 40px;
        cursor: pointer;
        text-align: left;
        width: 100%;
        max-width: none;
        border-radius: 5px;
    }

    </style>
	<!-- breadcrumb -->
	<div class="bread-crumb bgwhite flex-w p-l-52 p-r-15 p-t-30 p-l-15-sm">
		<a href="index.html" class="s-text16">
			Shop
			<i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
		</a>

		<a href="product.html" class="s-text16">
			New Arival
			<i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
		</a>

		<a href="#" class="s-text16">
			Glasses
			<i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
		</a>

		<span class="s-text17">
			{{@$product->name}}
		</span>
	</div>

	<!-- Product Detail -->
	<div class="container bgwhite p-t-35 p-b-80">
		<div class="flex-w flex-sb">
			<div class="w-size13 p-t-30 respon5">
				<div class="wrap-slick3 flex-sb flex-w">
					<div class="wrap-slick3-dots"></div>

					<div class="slick3">
                        @foreach ($detailproduct->image_variant as $image_variant)
                            <div class="item-slick3" data-thumb="{{ asset($image_variant->url) }}">
                                <div class="wrap-pic-w">
                                    <img src="{{ asset($image_variant->url) }}" alt="IMG-PRODUCT">
                                </div>
                            </div>
                        @endforeach
					</div>
				</div>
			</div>

			<div class="w-size14 p-t-30 respon5">
				<h4 class="product-detail-name m-text16 p-b-13">
					{{@$product->name}} | {{@$detailproduct->name}}
				</h4>

				<span class="m-text17">
					Rp {{@$detailproduct->price}}
				</span>

				<p class="s-text8 p-t-10">
					{{@$product->tagline}}
				</p>

				<!--  -->
				<div class="p-t-33 p-b-60">
					<div class="flex-m flex-w">
						<div class="s-text15 w-size15 t-center">
							Varian
						</div>

                        <select class="selection-2" name="color"  >
                            @foreach ($listvatiant as $variant)
                                <option value='{{$variant->id}}' {{ $detailproduct->id == $variant->id ? 'selected': ''}}>{{$variant->name}}</option>
                            @endforeach
                        </select>
					</div>

					<div class="flex-r-m flex-w p-t-10">
						<div class="w-size16 flex-m flex-w">
							<div class="flex-w bo5 of-hidden m-r-22 m-t-10 m-b-10">
								<button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
									<i class="fs-12 fa fa-minus" aria-hidden="true"></i>
								</button>

								<input class="size8 m-text18 t-center num-product" type="number" name="num-product" value="1">

								<button class="btn-num-product-up color1 flex-c-m size7 bg8 eff2">
									<i class="fs-12 fa fa-plus" aria-hidden="true"></i>
								</button>
							</div>

							<div class="btn-add cart-product-detail size9 trans-0-4 m-t-10 m-b-10">
								<button  class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
									Add to Cart
								</button>
							</div>
						</div>
					</div>
				</div>

				<div class="p-b-45">
					<span class="s-text8 m-r-35">SKU: {{@$product->sku}}</span>
					<span class="s-text8">Categories: Glasses</span>
				</div>

				<!--  -->
				<div class="wrap-dropdown-content bo6 p-t-15 p-b-14 active-dropdown-content">
					<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
						Description
						<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
						<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
					</h5>

					<div class="dropdown-content dis-none p-t-15 p-b-23">
						<p class="s-text8">
							{{@$product->description}}
						</p>
					</div>
				</div>

				<div class="wrap-dropdown-content bo7 p-t-15 p-b-14">
					<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
						Additional information
						<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
						<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
					</h5>

					<div class="dropdown-content dis-none p-t-15 p-b-23">
						<p class="s-text8">
                            {{@$product->additional_information}}
                        </p>
					</div>
				</div>

				<div class="wrap-dropdown-content bo7 p-t-15 p-b-14">
					<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
						Reviews (0)
						<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
						<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
					</h5>

					<div class="dropdown-content dis-none p-t-15 p-b-23">
						<p class="s-text8">
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Relate Product -->
	<section class="relateproduct bgwhite p-t-45 p-b-138">
		<div class="container">
			<div class="sec-title p-b-60">
				<h3 class="m-text5 t-center">
					Related Products
				</h3>
			</div>

			<!-- Slide2 -->
			<div class="wrap-slick2">
				<div class="slick2">
                    @foreach ($listproduct as $product)
                        <div class="item-slick2 p-l-15 p-r-15">
                            <!-- Block2 -->
                            <div class="block2">
                                <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                                    <img src="{{ asset($product->url_thumnail) }}" alt="IMG-PRODUCT">

                                    <div class="block2-overlay trans-0-4">
                                        <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                            <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                            <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                        </a>

                                        <div class="block2-btn-addcart w-size1 trans-0-4">
                                            <!-- Button -->
                                            <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
                                                Add to Cart
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="block2-txt p-t-20">
                                    <a href="product-detail.html" class="block2-name dis-block s-text3 p-b-5">
                                        {{@$product->name}}
                                    </a>

                                    <span class="block2-price m-text6 p-r-5">
                                        Rp. {{@$product->selling_price}}
                                    </span>
                                </div>
                            </div>
                        </div>
                    @endforeach


				</div>
			</div>

		</div>



    <div class="modal fade" id="modal_add_glasses" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-lg p-9" >
            <!--begin::Modal content-->
            <div class="modal-content modal-rounded" >
                <!--begin::Modal header-->

                <!--begin::Modal header-->
                <!--begin::Modal body-->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">

                            <h4 class="product-detail-name m-text16 p-b-13" style="position: absolute !important; margin: 15px">
                                {{@$product->name}} | {{@$detailproduct->name}}
                            </h4>
                            <img src="{{ asset('glasses/Yumika BG_Cover.JPG') }}" style="width: 96%; height:auto" />
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="modal-header py-7 d-flex justify-content-between">
                                <!--begin::Modal title-->
                                <h4>CHOOSE LENS TYPE <span id="tipe_lensa"></span></h4>
                                <!--end::Modal title-->
                                <!--begin::Close-->
                                <button class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                                    <i class="fa fa-times "></i>
                                </button>
                                <!--end::Close-->
                            </div>

                            <div class="choose_lense_drawer_options" id="basic" style="display: none" >
                                <div class="choose_lense_options_main">
                                    <div class="choose_lense_options_box">
                                        <div class="choose_lense_options_form">
                                            @foreach ($listlens as $product_lens)
                                                @if($product_lens->product_type_id == 3)
                                                {{@$product_lens->name}}
                                                @foreach ($product_lens->variant as $variant)

                                                <div class="choose_lense_first_level_options lens_radio_button">
                                                    <input class="lense_radio" type="radio" name="variant_name" data-value="{{$variant->variant_id}}" value="{{$variant->variant_id}}" id="variant_name_{{$variant->variant_id}}"  data-price="{{$variant->selling_price}}">
                                                    <label for="variant_name_{{$variant->variant_id}}" class="lense_checkbox_label">
                                                      <div class="label_text_box">
                                                        <div class="lense_checkbox_label_title">
                                                        <img src="{{ asset($variant->url_thumnail) }}" alt="IMG-PRODUCT" style="width: 100px; height: auto">
                                                          <span class="lens-label">{{$variant->variant_name}}</span>
                                                          <span class="lens-price">+Rp {{$variant->selling_price}}</span>
                                                        </div>
                                                      </div>
                                                    </label>

                                                  </div>
                                                @endforeach
                                                @endif
                                            @endforeach
                                            <div class="mb-10 fv-row">
                                                <!--begin::Label-->
                                                <label class="required form-label">Note Minus And Silindris</label>
                                                <!--end::Label-->
                                                <!--begin::Input-->
                                                <textarea class="form-control mb-2" id="product_description" name="product_description" placeholder="Note Your Minus and Silindris"  required> </textarea>
                                                <!--end::Input-->
                                            </div>
                                            <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="background-color: #c70303;" onClick="backSelectLens()">
                                                Back
                                            </button>

                                            <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4"  onClick="saveToChartLens()">
                                                Add To Chart
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="choose_lense_drawer_options" id="hi-index" style="display: none" >
                                <div class="choose_lense_options_main">
                                    <div class="choose_lense_options_box">
                                        <div class="choose_lense_options_form">
                                            HI-INDEX
                                            @foreach ($listlens as $product_lens)
                                            @if($product_lens->product_type_id == 4)
                                            {{@$product_lens->name}}
                                            @foreach ($product_lens->variant as $variant)

                                            <div class="choose_lense_first_level_options lens_radio_button">
                                                <input class="lense_radio" type="radio" name="variant_name" data-value="{{$variant->variant_id}}" value="{{$variant->variant_id}}" id="variant_name_{{$variant->variant_id}}"  data-price="{{$variant->selling_price}}">
                                                <label for="variant_name_{{$variant->variant_id}}" class="lense_checkbox_label">
                                                  <div class="label_text_box">
                                                    <div class="lense_checkbox_label_title">
                                                    <img src="{{ asset($variant->url_thumnail) }}" alt="IMG-PRODUCT" style="width: 100px; height: auto">
                                                      <span class="lens-label">{{$variant->variant_name}}</span>
                                                      <span class="lens-price">+Rp {{$variant->selling_price}}</span>
                                                    </div>
                                                  </div>
                                                </label>

                                              </div>
                                            @endforeach
                                            @endif
                                        @endforeach


                                        <div class="mb-10 fv-row">
                                            <!--begin::Label-->
                                            <label class="required form-label">Note Minus And Silindris</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <textarea class="form-control mb-2" id="product_description" name="product_description" placeholder="Note Your Minus and Silindris"  required> </textarea>
                                            <!--end::Input-->
                                        </div>
                                            <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="background-color: #c70303;" onClick="backSelectLens()">
                                                Back
                                            </button>

                                            <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4"  onClick="saveToChartLens()">
                                                Add To Chart
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="choose_lense_drawer_options" id="premium" style="display: none" >
                                <div class="choose_lense_options_main">
                                    <div class="choose_lense_options_box">
                                        <div class="choose_lense_options_form">
                                            @foreach ($listlens as $product_lens)
                                            @if($product_lens->product_type_id == 5)
                                            {{@$product_lens->name}}
                                            @foreach ($product_lens->variant as $variant)

                                            <div class="choose_lense_first_level_options lens_radio_button">
                                                <input class="lense_radio" type="radio" name="variant_name" data-value="{{$variant->variant_id}}" value="{{$variant->variant_id}}" id="variant_name_{{$variant->variant_id}}"  data-price="{{$variant->selling_price}}">
                                                <label for="variant_name_{{$variant->variant_id}}" class="lense_checkbox_label">
                                                  <div class="label_text_box">
                                                    <div class="lense_checkbox_label_title">
                                                    <img src="{{ asset($variant->url_thumnail) }}" alt="IMG-PRODUCT" style="width: 100px; height: auto">
                                                      <span class="lens-label">{{$variant->variant_name}}</span>
                                                      <span class="lens-price">+Rp {{$variant->selling_price}}</span>
                                                    </div>
                                                  </div>
                                                </label>

                                              </div>
                                            @endforeach
                                            @endif
                                        @endforeach


                                        <div class="mb-10 fv-row">
                                            <!--begin::Label-->
                                            <label class="required form-label">Note Minus And Silindris</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <textarea class="form-control mb-2" id="product_description" name="product_description" placeholder="Note Your Minus and Silindris"  required> </textarea>
                                            <!--end::Input-->
                                        </div>
                                            <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" style="background-color: #c70303;" onClick="backSelectLens()">
                                                Back
                                            </button>

                                            <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4"  onClick="saveToChartLens()">
                                                Add To Chart
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="choose_lense_drawer_options" id="progresif" style="display: none" >
                                <div class="choose_lense_options_main">
                                    <div class="choose_lense_options_box">
                                        <div class="choose_lense_options_form">
                                            @foreach ($listlens as $product_lens)
                                            @if($product_lens->product_type_id == 11)
                                            {{@$product_lens->name}}
                                            @foreach ($product_lens->variant as $variant)

                                            <div class="choose_lense_first_level_options lens_radio_button">
                                                <input class="lense_radio" type="radio" name="variant_name" data-value="{{$variant->variant_id}}" value="{{$variant->variant_id}}" id="variant_name_{{$variant->variant_id}}"  data-price="{{$variant->selling_price}}">
                                                <label for="variant_name_{{$variant->variant_id}}" class="lense_checkbox_label">
                                                  <div class="label_text_box">
                                                    <div class="lense_checkbox_label_title">
                                                    <img src="{{ asset($variant->url_thumnail) }}" alt="IMG-PRODUCT" style="width: 100px; height: auto">
                                                      <span class="lens-label">{{$variant->variant_name}}</span>
                                                      <span class="lens-price">+Rp {{$variant->selling_price}}</span>
                                                    </div>
                                                  </div>
                                                </label>

                                              </div>
                                            @endforeach
                                            @endif
                                        @endforeach

                                        <div class="mb-10 fv-row">
                                            <!--begin::Label-->
                                            <label class="required form-label">Note Minus And Silindris</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <textarea class="form-control mb-2" id="product_description" name="product_description" placeholder="Note Your Minus and Silindris"  required> </textarea>
                                            <!--end::Input-->
                                        </div>

                                            <button class="flex-c-m size1 bo-rad-23 hov1 s-text1 trans-0-4" style="background-color: #c70303;" onClick="backSelectLens()">
                                                Back
                                            </button>

                                            <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4"  onClick="saveToChartLens()">
                                                Add To Chart
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="choose_lense_drawer_options" id="lens-chooise">
                                <div class="choose_lense_options_main">
                                  <div class="choose_lense_options_box">
                                    <div class="choose_lense_options_form">

                                      <div class="choose_lense_first_level_options lens_radio_button">
                                        <input class="lense_radio" type="radio" name="lens_type" data-value="sv" value="basic" id="lens_type_1" data-price="0">
                                        <label for="lens_type_1" class="lense_checkbox_label">
                                          <div class="label_text_box">
                                            <div class="lense_checkbox_label_title">
                                              <span class="lens-label">Basic</span>
                                              <!--span class="lens-price">+Rp 0,00</span-->
                                            </div>
                                            <div class="lense_checkbox_label_text">For distance or reading.</div>
                                          </div>
                                        </label>
                                      </div>
                                      <div class="choose_lense_first_level_options lens_radio_button">
                                        <input class="lense_radio" type="radio" name="lens_type" data-value="prg" value="hi-index" id="lens_type_2" data-price="0">
                                        <label for="lens_type_2" class="lense_checkbox_label">
                                          <div class="label_text_box">
                                            <div class="lense_checkbox_label_title">
                                              <span class="lens-label">Hi-Index</span>
                                              <!--span class="lens-price">+Rp 0,00</span-->
                                            </div>
                                            <div class="lense_checkbox_label_text">For distance, reading, and everything in-between.</div>
                                          </div>
                                        </label>

                                      </div>


                                       <div class="choose_lense_first_level_options lens_radio_button">
                                        <input class="lense_radio" type="radio" name="lens_type" data-value="rr" value="premium" id="lens_type_3" data-price="87500000">
                                        <label for="lens_type_3" class="lense_checkbox_label">
                                          <div class="label_text_box">
                                            <div class="lense_checkbox_label_title">
                                              <span class="lens-label">Premium</span>
                                              <!--span class="lens-price price">+Rp 875.000,00</span-->
                                            </div>
                                            <div class="lense_checkbox_label_text">Magnifies text using the same power in each lens.</div>
                                          </div>
                                        </label>
                                      </div>

                                      <div class="choose_lense_first_level_options lens_radio_button">
                                        <input class="lense_radio" type="radio" name="lens_type" data-value="np" value="progresif" id="lens_type_4" data-price="87500000">
                                        <label for="lens_type_4" class="lense_checkbox_label">
                                          <div class="label_text_box">
                                            <div class="lense_checkbox_label_title">
                                              <span class="lens-label">Progresif</span>
                                              <!--span class="lens-price">+Rp 875.000,00</span-->
                                            </div>
                                            <div class="lense_checkbox_label_text">Clear lens with an anti-reflective coating.</div>
                                          </div>
                                        </label>
                                      </div>

                                      <div class="choose_lense_first_level_options lens_radio_button">
                                        <input class="lense_radio" type="radio" name="lens_type" value="frame-only" id="lens_type_5" data-price="0">
                                        <label for="lens_type_5" class="lense_checkbox_label">
                                          <div class="label_text_box">
                                            <div class="lense_checkbox_label_title">
                                              <span class="lens-label">Frame only</span>
                                              <!--span class="lens-price">+Rp 0,00</span-->
                                            </div>
                                            <div class="lense_checkbox_label_text">Clear temporary lenses not for daily use.</div>
                                          </div>
                                        </label>
                                      </div>


                                        <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" id="btn-choose-lens" onclick="selectLens()">
                                            Choose Lens
                                        </button>

                                        <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" id="btn-checkout" style="display: none" onClick="saveToChart({{$product->id}})">
                                            Add To Chart
                                        </button>

                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--begin::Modal body-->
            </div>
        </div>
    </div>

	</section>


@section('onpage-js')

<script>
    $(document).ready(function(){
        $('#modal_add_glasses').modal('show')


    });
    document.getElementById('lens-chooise').style.display = "block";
    $('input[type=radio][name=lens_type]').change(function() {
    if (this.value == 'frame-only') {
        document.getElementById('btn-choose-lens').style.display = "none";
        document.getElementById('btn-checkout').style.display = "block";
    }
    else {

        document.getElementById('btn-choose-lens').style.display = "block";
        document.getElementById('btn-checkout').style.display = "none";
    }
});

function selectLens(){
    var val = $('input[name=lens_type]:checked').val();
    if(val == 'basic'){
        $('#tipe_lensa').html(': BASIC');
        document.getElementById('lens-chooise').style.display = "none";
        document.getElementById('progresif').style.display = "none";
        document.getElementById('basic').style.display = "block";
        document.getElementById('premium').style.display = "none";
        document.getElementById('hi-index').style.display = "none";
    }else if(val == 'progresif'){

        $('#tipe_lensa').html(': PROGRESIF');
        document.getElementById('lens-chooise').style.display = "none";
        document.getElementById('progresif').style.display = "block";
        document.getElementById('basic').style.display = "none";
        document.getElementById('premium').style.display = "none";
        document.getElementById('hi-index').style.display = "none";
    }else if(val == 'premium'){

        $('#tipe_lensa').html(': PREMIUM');
        document.getElementById('lens-chooise').style.display = "none";
        document.getElementById('progresif').style.display = "none";
        document.getElementById('basic').style.display = "none";
        document.getElementById('premium').style.display = "block";
        document.getElementById('hi-index').style.display = "none";
    }else if(val == 'hi-index'){

        $('#tipe_lensa').html(': HI-INDEX');
        document.getElementById('lens-chooise').style.display = "none";
        document.getElementById('progresif').style.display = "none";
        document.getElementById('basic').style.display = "none";
        document.getElementById('premium').style.display = "none";
        document.getElementById('hi-index').style.display = "block";
    }
}
function backSelectLens(){
    $('#tipe_lensa').html('');
    document.getElementById('lens-chooise').style.display = "block";
    document.getElementById('progresif').style.display = "none";
    document.getElementById('basic').style.display = "none";
    document.getElementById('premium').style.display = "none";
    document.getElementById('hi-index').style.display = "none";
    $('#product_description').val('');
}
</script>

<script src="{{ asset('landingPageHeyKama/js/main.js')}}"></script>
@endsection
