@extends('frontend.layouts.landing')

@section('title', 'HeyKama - Home')

@section('content')

<link href="assetsfront/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
<link href="../../unicons.iconscout.com/release/v4.0.0/css/line.css" rel="stylesheet">
<link href="assetsfront/css/twentytwenty.css" rel="stylesheet" type="text/css" />
<!-- Main css File -->
<link href="assetsfront/css/style.css" rel="stylesheet" type="text/css" />
<link href="{{asset('demo-style.css')}}" type="text/css" media="screen" rel="stylesheet" />
<link href="{{asset('hint.min.css')}}" type="text/css" media="screen" rel="stylesheet" />

	<!-- Product Detail -->
	<div class="container bgwhite p-t-35 p-b-80">
		<div class="flex-w flex-sb">

			<div class="w-size14 p-t-30 respon5">
                <h4> Checkout Success</h4>
                <p>Payment Transfer to : 123672136778 a/n TESTER BCA</p>
                <div class="w-size2 p-t-20">
                    <a href="{{ route('heykama.order') }}" class="flex-c-m size2 bg4 bo-rad-23 hov1 m-text3 trans-0-4">View In Order</a>
                </div>
			</div>
		</div>
	</div>

@endsection


@section('customjs')
