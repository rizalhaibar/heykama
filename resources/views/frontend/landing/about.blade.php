@extends('frontend.layouts.landing')

@section('title', 'HeyKama - About')

@section('content')


	<!-- Title Page -->
	<!--section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url({{ asset('landingPageHeyKama/images/HKM08516.jpg') }}); background-size:cover; padding-top: 200px">
		<h2 class="l-text2 t-center">
			About
		</h2>
	</section-->

	<!-- content page -->
	<section class="bgwhite p-t-66 p-b-38" style="margin-top: 100px">
		<div class="container">
			<!--div class="row">
				<div class="col-md-4 p-b-30">
					<div class="hov-img-zoom">
						<img src="{{ asset('landingPageHeyKama/images/HKM02603.jpg') }}" alt="IMG-ABOUT">
					</div>
				</div>

				<div class="col-md-8 p-b-30">
					<h3 class="m-text26 p-t-15 p-b-16">
						Our story
					</h3>

					<p class="p-b-28">
                        Tampil bergaya dengan frame kacamata yang bikin kamu semakin menarik. Juga mata yang lebih terlindungi dari berbagai sinar bahaya. Semua bahkan bisa didapatkan tanpa harus keluar rumah!!

					</p>

					<div class="bo13 p-l-29 m-l-9 p-b-10">
						<p class="p-b-11">

                        HeyKama adalah sebuah online store kacamata terpercaya yang membuat kamu bisa dapatkan frame-frame yang super trendy dan up to date untuk bikin penampilan kamu semakin menarik. Kacamata Korea, Kacamata Cewek, Kacamata Cowok, Juga lensa-lensa fungsional yang bisa bikin mata kamu semakin terlindungi dari sinar-sinar berbahaya.

                        Batang kokoh dengan build quality terbaik bisa dengan mudah didapatkan oleh Teman Kama.

                        Mulai dari lensa anti radiasi, lensa photochromic, lensa blueray, sampai lensa bluechromic bisa dengan mudah Teman Kama dapatkan mulai sekarang.	</p>

						<span class="s-text7">
							- HeyKama
						</span>
					</div>
				</div>
			</div-->
		</div>
	</section>


@section('customjs')
<script type="text/javascript">
    $(".selection-1").select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect1')
    });

    $(".selection-2").select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect2')
    });
</script>
<!--===============================================================================================-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKFWBqlKAGCeS1rMVoaNlwyayu0e0YRes"></script>

<script src="{{ asset('landingPageHeyKama/js/map-custom.js')}}"></script>
<!--===============================================================================================-->
<script src="{{ asset('landingPageHeyKama/js/main.js')}}"></script>
@endsection
