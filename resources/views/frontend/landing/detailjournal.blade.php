@extends('frontend.layouts.landing')

@section('title', 'HeyKama - '.$journaldetail->title)

@section('content')


	<!-- content page -->
	<section class="bgwhite p-t-60 p-b-25" >
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-12 p-b-80">
					<div class="p-r-50 p-r-0-lg">
						<div class="p-b-40">
							<div class="blog-detail-img wrap-pic-w">
								<img src="{{asset($journaldetail->url)}}" alt="IMG-BLOG">
							</div>

							<div class="blog-detail-txt p-t-33">
								<h4 class="p-b-11 m-text24">
									{{$journaldetail->title}}
								</h4>

								<div class="s-text8 flex-w flex-m p-b-21">
									<span>
										By Admin
										<span class="m-l-3 m-r-6">|</span>
									</span>

									<span>
										{{date_format(date_create($journaldetail->created_at),"d M, Y ")}}
									</span>
								</div>

								<p class="p-b-25">
									{{strip_tags($journaldetail->body)}}
								</p>

							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>


@endsection

@section('customjs')
@endsection
