@extends('frontend.layouts.landing')

@section('title', 'HeyKama - Home')

@section('content')
<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url({{ asset('landingPageHeyKama/images/HKM08708.jpg')}}); background-size:cover; padding-top: 200px">
    <h2 class="l-text2 t-center">
        Orders
    </h2>
</section>
<!-- Cart -->
<section class="cart bgwhite p-t-70 p-b-100">
    <div class="container">
        <!-- Cart item -->
        <div class="container-table-cart pos-relative">
            <div class="wrap-table-shopping-cart bgwhite">
                <table class="table-shopping-cart">
                    <tr class="table-head">
                        <th class="column-1">INVOICE ID</th>
                        <th class="column-2">Address</th>
                        <th class="column-3">Quantity</th>
                        <th class="column-4">Total</th>
                        <th class="column-5">Status</th>
                        <th class="column-6">Action</th>
                    </tr>

                    @foreach($listorder as $cart)
                    <tr class="table-row">
                        <td class="column-1">
                            {{ $cart->invoice_id}}
                        </td>
                        <td class="column-2">{{$cart->shipping_address}}</td>
                        <td class="column-3 p-l-70">{{$cart->qty}}</td>
                        <td class="column-4">{{$cart->grand_total}}</td>
                        <td class="column-5">{{$cart->status}}</td>
                        <td class="column-6" style="padding-right: 50px">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Detail Order</a>
                                <a class="dropdown-item" href="#">Upload Evidence</a>
                                <a class="dropdown-item" href="#">Cancel Order</a>
                            </div>
                        </td>
                   </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</section>


@section('onpage-js')
<script type="text/javascript">
</script>
<!--===============================================================================================-->
<script src="{{ asset('landingPageHeyKama/js/main.js')}}"></script>
@endsection
