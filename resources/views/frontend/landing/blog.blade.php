@extends('frontend.layouts.landing')

@section('title', 'HeyKama - Journal')

@section('content')


	<!-- Title Page
	<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url({{ asset('landingPageHeyKama/images/heading-pages-01.jpg') }}); background-size:cover; padding-top: 200px">
		<h2 class="l-text2 t-center">

		</h2>
	</section> -->

	<!-- content page -->
	<section class="bgwhite " style="padding-bottom: 500px">
		<div class="container">
			<div class="row" style="margin-top: 100px">
				<div class="col-md-12 col-lg-12 p-b-75">
                    <img src="{{asset($banner->url)}}" style="width:100%; height:auto" alt="IMG-BLOG" />

                </div>
                @foreach ($listjournal as $journal )
                    <div class="col-md-12 col-lg-6 p-b-75" style="margin-top: -40px">
                        <div class=" p-r-0-lg">
                            <!-- item blog -->
                            <div class="item-blog p-b-80">
                                <a href="{{ route('heykama.detailjournal', ['id' =>$journal->id]) }}" class="item-blog-img pos-relative dis-block hov-img-zoom">
                                    <img src="{{asset($journal->url)}}" alt="IMG-BLOG">

                                    <span class="item-blog-date dis-block flex-c-m pos1 size17 bg4 s-text1">
                                        {{date_format(date_create($journal->created_at),"d M, Y ")}}
                                    </span>
                                </a>

                                <div class="item-blog-txt p-t-33">
                                    <h4 class="p-b-11">
                                        <a href="blog-detail.html" class="m-text24">
                                            {{$journal->title}}
                                        </a>
                                    </h4>
                                </div>
                                <hr style='border-top: 1px solid #e2dfdf; margin-top:-10px'/>
                            </div>
                        </div>
                    </div>
                @endforeach
				<div class="col-md-12 col-lg-6 p-b-75" style="margin-top: -40px">
					<div class="p-r-0-lg">
						<!-- item blog -->
						<div class="item-blog p-b-80">
							<a href="#" class="item-blog-img pos-relative dis-block hov-img-zoom">
								<img src="{{asset('landingPageHeyKama/images/blog-04.jpg')}}" alt="IMG-BLOG">

								<span class="item-blog-date dis-block flex-c-m pos1 size17 bg4 s-text1">
									28 Dec, 2018
								</span>
							</a>

							<div class="item-blog-txt p-t-33">
								<h4 class="p-b-11">
									<a href="blog-detail.html" class="m-text24">
										Black Friday Guide: Best Sales & Discount Codes
									</a>
								</h4>
							</div>

                            <hr style='border-top: 1px solid #e2dfdf; margin-top:-10px'/>
						</div>


					</div>
				</div>

				<div class="col-md-12 col-lg-12 p-b-75" style="margin-top: -40px">

                </div>

			</div>
		</div>
	</section>

@endsection
@section('customjs')
@endsection
