@extends('frontend.layouts.landing')

@section('title', 'HeyKama - Shop')

@section('content')



	<!-- Content page -->
	<section class="bgwhite p-t-55 p-b-65">
		<div class="container">
			<div class="row">
                <div class="col-md-12 col-lg-12 p-b-75">
                    <img src="{{asset($banner->url)}}" style="width:100%; height:auto" alt="IMG-BLOG" />

                </div>
				<div class="col-sm-12 col-md-12 col-lg-12 p-b-50">
					<!--  -->
					<div class="flex-sb-m flex-w p-b-35">
						<div class="flex-w">
							<select class="selection-2" name="sorting">
                                <option>Default Sorting</option>
                                <option>Popularity</option>
                                <option>Price: low to high</option>
                                <option>Price: high to low</option>
                            </select>
                            &nbsp;
							<select class="selection-2" name="sorting">
                                <option>Price</option>
                                <option>Rp.50.000 - Rp.100.000</option>
                                <option>Rp.100.000 - Rp.150.000</option>
                                <option><Rp class="20"></Rp>0.000</option>

                            </select>
						</div>

						<span class="s-text8 p-t-5 p-b-5">
                            @php($total = count($listproduct))
							Showing {{$total}}–{{$total}} of {{$total}} results
						</span>
					</div>

					<!-- Product -->
					<div class="row">
                        @foreach($listproduct as $product)
                            <div class="col-sm-12 col-md-6 col-lg-3 p-b-50">
                                <!-- Block2 -->
                                <div class="block2">
                                    <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                                        <img src="{{ asset($product->url_thumnail) }}" alt="IMG-PRODUCT" >

                                        <div class="block2-overlay trans-0-4">
                                            <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                                <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                                <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                            </a>

                                            <div class="block2-btn-addcart w-size1 trans-0-4">
                                                <!-- Button -->
                                                <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" onClick="saveToChart({{$product->id}})">
                                                    Add to Cart
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="block2-txt p-t-20">
                                        <a href="{{ route('heykama.shopdetail', ['id' =>$product->id]) }}" class="block2-name dis-block s-text3 p-b-5">
                                            {{$product->name}}
                                        </a>

                                        <span class="block2-price m-text6 p-r-5">
                                            {{$product->selling_price}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
					</div>

					<!-- Pagination -->
					<div class="pagination flex-m flex-w p-t-26">
						<a href="#" class="item-pagination flex-c-m trans-0-4 active-pagination">1</a>
					</div>
				</div>
			</div>
		</div>
	</section>


@endsection

@section('onpage-js')
