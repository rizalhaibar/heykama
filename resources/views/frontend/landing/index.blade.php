@extends('frontend.layouts.landing')

@section('title', 'HeyKama - Home')

@section('content')

<?php
function rupiah($angka){

    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
    return $hasil_rupiah;

};
?>
<!-- Banner2 -->
<section class="banner2 bg5 p-t-55 p-b-55" style="background: #fff !important; margin-top: -100px; width: 100%; ">
    <div class="row" style="border-bottom: 1px solid #aaa !important;">
        <div class="col-sm-10 col-md-8 col-lg-6 m-l-r-auto p-t-15 p-b-15" style="padding: 0px !important">
            <div class="hov-img-zoom pos-relative">
                <img src="{{ asset('landingPageHeyKama/images/HKM08559.jpg') }}" alt="IMG-BANNER">

                <!--div class="ab-t-l sizefull flex-col-c-m p-l-15 p-r-15">
                    <span class="m-text9 p-t-45 fs-20-sm">
                        The Beauty
                    </span>

                    <h3 class="l-text1 fs-35-sm">
                        Lookbook
                    </h3>

                    <a href="#" class="s-text4 hov2 p-t-20 ">
                        View Collection
                    </a>
                </div-->
            </div>
        </div>

        <div class="col-sm-10 col-md-8 col-lg-6 m-l-r-auto p-t-15 p-b-15" style="padding: 0px !important">
            <div class="bgwhite hov-img-zoom pos-relative p-b-20per-ssm">
                <img src="{{ asset('landingPageHeyKama/images/HKM08543.jpg') }}" alt="IMG-BANNER">

                <div class="ab-t-l sizefull flex-col-c-b p-l-15 p-r-15 p-b-20">
                    <div class="t-center">
                        <a href="product-detail.html" class="dis-block s-text3 p-b-5">
                            Gafas sol Hawkers one
                        </a>

                        <span class="block2-oldprice m-text7 p-r-5">
                            $29.50
                        </span>

                        <span class="block2-newprice m-text8">
                            $15.90
                        </span>
                    </div>

                    <div class="flex-c-m p-t-44 p-t-30-xl">
                        <div class="flex-col-c-m size3 bo1 m-l-5 m-r-5">
                            <span class="m-text10 p-b-1 days">
                                69
                            </span>

                            <span class="s-text5">
                                days
                            </span>
                        </div>

                        <div class="flex-col-c-m size3 bo1 m-l-5 m-r-5">
                            <span class="m-text10 p-b-1 hours">
                                04
                            </span>

                            <span class="s-text5">
                                hrs
                            </span>
                        </div>

                        <div class="flex-col-c-m size3 bo1 m-l-5 m-r-5">
                            <span class="m-text10 p-b-1 minutes">
                                32
                            </span>

                            <span class="s-text5">
                                mins
                            </span>
                        </div>

                        <div class="flex-col-c-m size3 bo1 m-l-5 m-r-5">
                            <span class="m-text10 p-b-1 seconds">
                                05
                            </span>

                            <span class="s-text5">
                                secs
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <!--div class="row" style="margin-top: 20px;">
                <div class="col-lg-6">
                    <h6 class="">
                        HeyKama Summer
                    </h6>

                </div>
                <div class="col-lg-6">
                    <h4 class="m-text4 ">
                        Gaya Baru Harus Mulai Dari Diri Sendiri
                    </h4>

                </div>
            </div-->
        </div>
    </div>
</section>

<!-- Banner2
<section class="banner2 bg5 p-t-55 p-b-55" style="background: #fff !important; margin-top: -100px; width: 100%;">
    <div class="row" style="border-bottom: 1px solid #aaa !important;">
        <div class="col-sm-10 col-md-8 col-lg-6 m-l-r-auto p-t-15 p-b-15">
            <div class="hov-img-zoom pos-relative">
                <video id="background-video" autoplay loop muted poster="https://assets.codepen.io/6093409/river.jpg">
                    <source src="{{ asset('video/NEWSUNGLASSES.mp4') }}" type="video/mp4">
                  </video>

            </div>
        </div>

        <div class="col-sm-10 col-md-8 col-lg-6 m-l-r-auto p-t-15 p-b-15">
            <div class="bgwhite hov-img-zoom pos-relative p-b-20per-ssm">
                <video id="background-video" autoplay loop muted poster="https://assets.codepen.io/6093409/river.jpg">
                    <source src="{{ asset('video/NEWSUNGLASSES_REELS.mp4') }}" type="video/mp4">
                </video>
            </div>

            <div class="row" style="margin-top: 20px;">
                <div class="col-lg-6">
                    <h6 class="">
                        HeyKama Summer
                    </h6>

                </div>
                <div class="col-lg-6">
                    <h4 class="m-text4 ">
                        Gaya Baru Harus Mulai Dari Diri Sendiri
                    </h4>

                </div>
            </div>
        </div>
    </div>
</section>
-->


<!-- Slide1 -->
<section class="slide1" style="width: 100%;">
    <div class="wrap-slick1">
        <div class="slick1">
            <div class="item-slick1 item1-slick1" style="background-image: url({{ asset('landingPageHeyKama/images/testi-1.png')}});">
                <div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">

                    <div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="zoomIn" style="margin-top:300px !important">
                        <!-- Button -->
                        <a href="product.html" class="flex-c-m size2 bo-rad-23 s-text2 bgwhite hov1 trans-0-4">
                            Shop Now
                        </a>
                    </div>
                </div>
            </div>

            <div class="item-slick1 item2-slick1" style="background-image: url({{ asset('landingPageHeyKama/images/terbaik-dari.psd.png')}});">
                <div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">

                    <div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="slideInUp">
                        <!-- Button -->
                        <a href="product.html" class="flex-c-m size2 bo-rad-23 s-text2 bgwhite hov1 trans-0-4">
                            Shop Now
                        </a>
                    </div>
                </div>
            </div>

            <div class="item-slick1 item3-slick1" style="background-image: url({{ asset('landingPageHeyKama/images/master-slide-04.jpg')}});">
                <div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">

                    <div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="rotateIn">
                        <!-- Button -->
                        <a href="product.html" class="flex-c-m size2 bo-rad-23 s-text2 bgwhite hov1 trans-0-4">
                            Shop Now
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>



<!-- Banner2 -->
<section class="banner2 bg5 p-t-55 p-b-55" style="background: #fff !important; width: 99%;">
    <div class="row" style="border-bottom: 1px solid #aaa !important;">
        <div class="col-sm-10 col-md-8 col-lg-4 m-l-r-auto p-t-15 p-b-15 ">
            <div class="col-lg-6 corpo">

                <h4 class="m-text4  w-size3 p-b-8 ">
                    Four new sun silhouettes cutting through the light’s arrival.
                </h4>
                <h6>
                    Shop Glasses
                </h6>

            </div>
        </div>

        <div class="col-sm-10 col-md-8 col-lg-8 m-l-r-auto p-t-15 p-b-15">
            <div class="row">
                @foreach ($homeproduct as $product )
                    <div class="col-sm-6 col-md-6 col-lg-6 m-l-r-auto">
                        <a href="{{ route('heykama.shopdetail', ['id' =>$product->id]) }}" >
                        <!-- block1 -->
                        <div class="block1 hov-img-zoom pos-relative m-b-30">
                            <img src="{{ asset($product->url_thumnail) }}" alt="IMG-BENNER">

                        </div>
                        <div class="block2-txt">




                            <span class="block2-name dis-block s-text3 p-b-5">
                                {{$product->name}}
                            </span>
                            <span class="block2-price m-text6 p-r-5">
                                {{rupiah($product->selling_price)}}
                            </span>
                        </div>

                    </a>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
</section>



<!-- Banner2 -->
<section class="banner2 bg5 p-t-55 p-b-55" style="background: #fff !important; width: 99%;">
    <div class="row" style="border-bottom: 1px solid #aaa !important;">

        <div class="col-sm-10 col-md-8 col-lg-8 m-l-r-auto p-t-15 p-b-15">
            <div class="hov-img-zoom pos-relative">
                <img src="{{ asset('landingPageHeyKama/images/HKM02603.jpg') }}" alt="IMG-BANNER">

                <div class="ab-t-l sizefull flex-col-c-m p-l-15 p-r-15">
                    <span class="m-text9 p-t-45 fs-20-sm">
                        The Beauty
                    </span>

                    <h3 class="l-text1 fs-35-sm">
                        Lookbook
                    </h3>

                    <a href="#" class="s-text4 hov2 p-t-20 ">
                        View Collection
                    </a>
                </div>
            </div>

        </div>

        <div class="col-sm-10 col-md-8 col-lg-4 m-l-r-auto p-t-15 p-b-15 ">
            <div class="col-lg-6 corpo">

                <h4 class="m-text4  w-size3 p-b-8 ">
                    Four new sun silhouettes cutting through the light’s arrival.
                </h4>
                <h6 class="">
                    Shop Glasses
                </h6>

            </div>
        </div>
    </div>

</section>



	<!-- New Product -->
<section class="newproduct bgwhite p-t-45 p-b-105" style="width: 99%;">
    <div class="sec-title p-b-60">
        <h3 class="m-text5 t-center">
            Featured Products
        </h3>
    </div>

    <!-- Slide2 -->
    <div class="wrap-slick2" style="margin-left: 60px; width:95%">
        <div class="slick2">
            @foreach ($featuredproduct as $featured )

            <div class="item-slick2 p-l-15 p-r-15">
                <!-- Block2 -->
                <a href="{{ route('heykama.shopdetail', ['id' =>$featured->id]) }}" >
                    <div class="block2">
                        <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                            <img src="{{ asset($featured->url_thumnail) }}" alt="IMG-PRODUCT">

                            <div class="block2-overlay trans-0-4">
                                <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                    <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                    <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                </a>

                                <div class="block2-btn-addcart w-size1 trans-0-4">
                                    <!-- Button -->
                                    <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" onClick="saveToChart({{$featured->id}})">
                                        Add to Cart
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="block2-txt p-t-20">
                            <span class="block2-name dis-block s-text3 p-b-5">
                                {{$featured->name}}
                            </span>

                            <span class="block2-price m-text6 p-r-5">
                                {{rupiah($featured->selling_price)}}
                            </span>
                        </div>
                    </div>

                </a>
            </div>
        @endforeach
        </div>
    </div>

</section>




	<!-- Blog -->
<section class="blog bgwhite p-t-94 p-b-65" style="margin-top: -250px !important; width: 99%; margin-left: 10px;">
    <div class="sec-title p-b-52">
        <h3 class="m-text5 t-center">
            Our Journal
        </h3>
    </div>

    <div class="row">
        @foreach ($listjournal as $journal )
            <div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
                <!-- Block3 -->
                <div class="block3">
                    <a href="blog-detail.html" class="block3-img dis-block hov-img-zoom">
                        <img src="{{asset($journal->url)}}" alt="IMG-BLOG">
                    </a>

                    <div class="block3-txt p-t-14">
                        <h4 class="p-b-7">
                            <a href="blog-detail.html" class="m-text11">
                                {{$journal->title}}
                            </a>
                        </h4>

                        <span class="s-text6">By</span> <span class="s-text7">Admin</span>
                        <span class="s-text6">on</span> <span class="s-text7">{{date_format(date_create($journal->created_at),"d M, Y ")}}</span>

                    </div>
                </div>
            </div>
        @endforeach
    </div>
</section>


<div class="modal" tabindex="-1" role="dialog" id="modalSubcribe" style="margin-top:150px">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="background-image: url({{ asset('landingPageHeyKama/images/subscribe.png') }}); background-size:cover; ">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div style="color: rgb(255, 255, 255); text-align: center; padding-top: 150px">
                    <center>
                       <h4> Join Heykama </h4>

                       <h1> Don't Miss Out</h1>

                       <h4>  Enjoy 10% off your next order </h4>
                    </center>
                </div>
                <div class="row" style="color: rgb(255, 255, 255); text-align: center; padding-top: 100px">
                    <div class="col-md-6">
                        <p class="form-row form-row-wide mailchimp-newsletter" style="margin: 20px">
                            <label for="mailchimp_woocommerce_newsletter" class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
                                <input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="mailchimp_woocommerce_newsletter" type="checkbox" name="mailchimp_woocommerce_newsletter" value="1" checked="checked">
                                <span  style="color: rgb(255, 255, 255)">SUBSCRIBE TO THE NEWSLETTER</span>
                            </label>
                        </p>
                    </div>
                    <div class="col-md-12">
                        <p class="woocommerce-form-row form-row">
                            <input type="hidden" id="woocommerce-register-nonce" name="woocommerce-register-nonce" value="57fcb5284d">
                            <input type="hidden" name="_wp_http_referer" value="/shop/">
                            <a href="{{route('register')}}"class="flex-c-m size2 bg4 bo-rad-23 hov1 m-text3 trans-0-4" style="margin-top: 10px">
                                Subscribe
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('customjs')
@endsection
