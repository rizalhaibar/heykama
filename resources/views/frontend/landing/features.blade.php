@extends('frontend.layouts.landing')

@section('title', 'HeyKama - Home')

@section('content')


	<!-- Title Page -->
	<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url({{ asset('landingPageHeyKama/images/HKM08516.jpg') }}); background-size:cover; padding-top: 200px">
		<h2 class="l-text2 t-center">
			Features
		</h2>
	</section>

	<!-- content page -->
	<section class="bgwhite p-t-66 p-b-38">
		<div class="container">
			<div class="row">

				</div>
			</div>
		</div>
	</section>


@section('customjs')
<script type="text/javascript">
    $(".selection-1").select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect1')
    });

    $(".selection-2").select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect2')
    });
</script>
<!--===============================================================================================-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKFWBqlKAGCeS1rMVoaNlwyayu0e0YRes"></script>

<script src="{{ asset('landingPageHeyKama/js/map-custom.js')}}"></script>
<!--===============================================================================================-->
<script src="{{ asset('landingPageHeyKama/js/main.js')}}"></script>
@endsection
