<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('heykama.index');
    // return view('welcome');
});


Route::group(['as' => 'heykama.'], function (){

	Route::get('index',  [App\Http\Controllers\Frontend\HeykamaController::class, 'index'])->name('index');
	Route::get('profile',  [App\Http\Controllers\Frontend\HeykamaController::class, 'profile'])->name('profile');
	Route::get('about', [App\Http\Controllers\Frontend\HeykamaController::class, 'about'])->name('about');
	Route::get('contact', [App\Http\Controllers\Frontend\HeykamaController::class, 'contact'])->name('contact');
	Route::get('blog', [App\Http\Controllers\Frontend\HeykamaController::class, 'blog'])->name('blog');
	Route::get('detailjournal/{id}', [App\Http\Controllers\Frontend\HeykamaController::class, 'detailjournal'])->name('detailjournal');
	Route::get('shop', [App\Http\Controllers\Frontend\HeykamaController::class, 'shop'])->name('shop');
	Route::get('shopdetail/{id}', [App\Http\Controllers\Frontend\HeykamaController::class, 'shopdetail'])->name('shopdetail');
	Route::get('promo', [App\Http\Controllers\Frontend\HeykamaController::class, 'promo'])->name('promo');
	Route::get('features', [App\Http\Controllers\Frontend\HeykamaController::class, 'features'])->name('features');
	Route::get('cart', [App\Http\Controllers\Frontend\HeykamaController::class, 'cart'])->name('cart');
	Route::get('order', [App\Http\Controllers\Frontend\HeykamaController::class, 'order'])->name('order');
	Route::get('trfpage/{id}', [App\Http\Controllers\Frontend\HeykamaController::class, 'trfpage'])->name('trfpage');
	Route::post('addaddress', [App\Http\Controllers\Frontend\HeykamaController::class, 'addaddress'])->name('addaddress');



	Route::post('checkout', [App\Http\Controllers\Frontend\HeykamaController::class, 'checkout'])->name('checkout');
	Route::get('dashboard', [App\Http\Controllers\Backend\DashboardController::class, 'index'])->name('dashboard');
	Route::get('product', [App\Http\Controllers\Backend\ProductController::class, 'index'])->name('product');


	Route::get('category', [App\Http\Controllers\Backend\CategoryController::class, 'index'])->name('category');
	Route::post('addcategory', [App\Http\Controllers\Backend\CategoryController::class, 'create'])->name('addcategory');
	Route::post('editviewcategory', [App\Http\Controllers\Backend\CategoryController::class, 'create'])->name('editviewcategory');

	Route::get('uom', [App\Http\Controllers\Backend\UomController::class, 'index'])->name('uom');
	Route::post('adduom', [App\Http\Controllers\Backend\UomController::class, 'create'])->name('adduom');

	Route::get('producttype', [App\Http\Controllers\Backend\ProductTypeController::class, 'index'])->name('producttype');
	Route::post('addproducttype', [App\Http\Controllers\Backend\ProductTypeController::class, 'create'])->name('addproducttype');

	Route::get('supplier', [App\Http\Controllers\Backend\SupplierController::class, 'index'])->name('supplier');
	Route::post('addsupplier', [App\Http\Controllers\Backend\SupplierController::class, 'create'])->name('addsupplier');

	Route::get('customer', [App\Http\Controllers\Backend\CustomerController::class, 'index'])->name('customer');
	Route::post('addcustomer', [App\Http\Controllers\Backend\CustomerController::class, 'create'])->name('addcustomer');




	Route::post('addtocart', [App\Http\Controllers\Frontend\HeykamaController::class, 'addtocart'])->name('addtocart');
});




Auth::routes();


Route::group([
	'namespace' => 'Backend',
	'prefix' => 'backend',
	'as' => 'backend.',
], function () {


    Route::group(['as' => 'product.'], function (){
        Route::get('product', [App\Http\Controllers\Backend\ProductController::class, 'index'])->name('index');
        Route::get('product/addproduct', [App\Http\Controllers\Backend\ProductController::class, 'addproduct'])->name('addproduct');
        Route::get('product/addvarian/{id}', [App\Http\Controllers\Backend\ProductController::class, 'addvarian'])->name('addvarian');
        Route::get('product/addprice/{id}', [App\Http\Controllers\Backend\ProductController::class, 'addprice'])->name('addprice');
        Route::get('product/detailproduct/{id}', [App\Http\Controllers\Backend\ProductController::class, 'detailproduct'])->name('detailproduct');
        Route::get('product/editproduct/{id}', [App\Http\Controllers\Backend\ProductController::class, 'editproduct'])->name('editproduct');
        Route::get('product/editvariant/{id}', [App\Http\Controllers\Backend\ProductController::class, 'editvariant'])->name('editvariant');
        Route::get('product/modaleditvariant/{id}', [App\Http\Controllers\Backend\ProductController::class, 'modaleditvariant'])->name('modaleditvariant');

        Route::post('product/save', [App\Http\Controllers\Backend\ProductController::class, 'save'])->name('save');
        Route::post('product/edit', [App\Http\Controllers\Backend\ProductController::class, 'edit'])->name('edit');
        Route::post('product/delele', [App\Http\Controllers\Backend\ProductController::class, 'delele'])->name('delele');
        Route::post('product/delelevariant', [App\Http\Controllers\Backend\ProductController::class, 'delelevariant'])->name('delelevariant');
        Route::post('product/savevariant', [App\Http\Controllers\Backend\ProductController::class, 'savevariant'])->name('savevariant');
        Route::post('product/updatevariant', [App\Http\Controllers\Backend\ProductController::class, 'updatevariant'])->name('updatevariant');
        Route::post('product/variantupdate', [App\Http\Controllers\Backend\ProductController::class, 'variantupdate'])->name('variantupdate');
        Route::post('product/saveprice', [App\Http\Controllers\Backend\ProductController::class, 'saveprice'])->name('saveprice');
        Route::post('product/setproducttype', [App\Http\Controllers\Backend\ProductController::class, 'setproducttype'])->name('setproducttype');
    });

    Route::group(['as' => 'order.'], function (){
        Route::get('order', [App\Http\Controllers\Backend\OrderController::class, 'index'])->name('index');
        Route::get('order/detail/{id}', [App\Http\Controllers\Backend\OrderController::class, 'detail'])->name('detail');
    });

    Route::group(['as' => 'setting.'], function (){
        Route::get('setting', [App\Http\Controllers\Backend\SettingController::class, 'index'])->name('index');
        Route::post('setting/create', [App\Http\Controllers\Backend\SettingController::class, 'create'])->name('create');
        Route::get('setting/detail/{id}', [App\Http\Controllers\Backend\SettingController::class, 'detail'])->name('detail');
    });

    Route::group(['as' => 'promotion.'], function (){
        Route::get('promotion', [App\Http\Controllers\Backend\PromotionController::class, 'index'])->name('index');
        Route::get('promotion/listdatatable', [App\Http\Controllers\Backend\PromotionController::class, 'listdatatable'])->name('listdatatable');
    });

    Route::group(['as' => 'journal.'], function (){
        Route::get('journal', [App\Http\Controllers\Backend\JournalController::class, 'index'])->name('index');
        Route::post('journal/create', [App\Http\Controllers\Backend\JournalController::class, 'create'])->name('create');
        Route::get('journal/detail/{id}', [App\Http\Controllers\Backend\JournalController::class, 'detail'])->name('detail');
    });
    Route::group(['as' => 'about.'], function (){
        Route::get('about', [App\Http\Controllers\Backend\AboutController::class, 'index'])->name('index');
        Route::post('about/create', [App\Http\Controllers\Backend\AboutController::class, 'create'])->name('create');
        Route::get('about/detail/{id}', [App\Http\Controllers\Backend\AboutController::class, 'detail'])->name('detail');
    });
    Route::group(['as' => 'newslatter.'], function (){
        Route::get('newslatter', [App\Http\Controllers\Backend\NewlatterController::class, 'index'])->name('index');
        Route::post('newslatter/create', [App\Http\Controllers\Backend\NewlatterController::class, 'create'])->name('create');
        Route::get('newslatter/detail/{id}', [App\Http\Controllers\Backend\NewlatterController::class, 'detail'])->name('detail');
    });
    Route::group(['as' => 'user.'], function (){
        Route::get('user', [App\Http\Controllers\Backend\UserController::class, 'index'])->name('index');
        Route::get('user/add', [App\Http\Controllers\Backend\UserController::class, 'add'])->name('add');
        Route::get('user/edit/{id}', [App\Http\Controllers\Backend\UserController::class, 'edit'])->name('edit');
        Route::post('user/create', [App\Http\Controllers\Backend\UserController::class, 'create'])->name('create');
        Route::post('user/update', [App\Http\Controllers\Backend\UserController::class, 'update'])->name('update');
        Route::get('user/detail/{id}', [App\Http\Controllers\Backend\UserController::class, 'detail'])->name('detail');
    });

});
